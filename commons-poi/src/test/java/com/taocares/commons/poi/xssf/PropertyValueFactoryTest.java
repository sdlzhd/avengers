package com.taocares.commons.poi.xssf;

import com.taocares.commons.poi.demo.data.DemoData;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.testng.Assert.assertEquals;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/11/23
 */
public class PropertyValueFactoryTest {

    @Test
    public void testCall() {
        List<DemoData> data = new ArrayList<>();
        data.add(new DemoData(1L, "张三", 18, 95, 80, 99, new Date()));
        data.add(new DemoData(2L, "李四", 19, 89, 100, 92, new Date()));
        ValueFactory valueFactory = new PropertyValueFactory("name", "age");
        List<Object[]> call = valueFactory.call(data);
        assertEquals(2, call.size());
        assertEquals("张三", call.get(0)[0]);
        assertEquals(18, call.get(0)[1]);
    }
}