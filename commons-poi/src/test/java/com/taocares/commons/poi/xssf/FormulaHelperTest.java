package com.taocares.commons.poi.xssf;

import com.taocares.commons.poi.xssf.formula.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/11/21
 */
public class FormulaHelperTest {

    @Test
    public void testSum() {
        assertEquals("SUM(A1:D1)", new SumBuilder()
                .range(new CellRangeAddress(0, 0, 0, 3))
                .build());
        assertEquals("SUM(A1, D1, 3.0)", new SumBuilder()
                .cell(new CellReference(0, 0))
                .number(3)
                .cell(new CellReference(0, 3))
                .build());
    }

    @Test
    public void testSumIf() {
        CellRangeAddress range = new CellRangeAddress(0, 99, 0, 0);
        CellReference criteria = new CellReference(0, 1);
        CellRangeAddress sumRange = new CellRangeAddress(0, 99, 2, 2);
        assertEquals("SUMIF($A$1:$A$100, B1, $C$1:$C$100)", new SumIfBuilder()
                .range(range).criteria(criteria).sumRange(sumRange).build());
        assertEquals("SUMIF($A$1:$A$100, \">2\", $C$1:$C$100)", new SumIfBuilder()
                .range(range).criteria(">2").sumRange(sumRange).build());
        assertEquals("SUMIF($A$1:$A$100, 2)", new SumIfBuilder()
                .range(range).criteria(2).build());
    }

    @Test
    public void testCriteria() {
        CellReference a1 = new CellReference(0, 0);
        CellReference b1 = new CellReference(0, 1);
        Criteria criteria = Criteria.of(Criteria.Operator.LE, a1, 3);
        assertEquals("A1<=3", criteria.toString());
        String and = criteria.and(Criteria.of(Criteria.Operator.GT, a1, b1));
        assertEquals("AND(A1<=3, A1>B1)", and);
    }

    @Test
    public void testIf() {
        CellReference cellReference = new CellReference(0, 0);
        String formula = new IfBuilder()
                .criteria(Criteria.of(Criteria.Operator.GE, cellReference, 90))
                .valueIfTrue("优秀")
                .valueIfFalse("不优秀")
                .build();
        assertEquals("IF(A1>=90, \"优秀\", \"不优秀\")", formula);

//        if (score >= 90) {
//            result = "优秀";
//        } else if (score >= 75) {
//            result = "良好";
//        } else if (score >= 60) {
//            result = "及格";
//        } else {
//            result = "不及格";
//        }
        formula = new SwitchBuilder().cellReference(cellReference)
                .condition(Criteria.Operator.GE, 90, "优秀")
                .condition(Criteria.Operator.GE, 75, "良好")
                .condition(Criteria.Operator.GE, 60, "及格")
                .defaultValue("不及格").build();
        System.err.println(formula);
    }
}