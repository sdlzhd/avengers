package com.taocares.commons.poi.xslf.chart.option;

import lombok.Builder;
import lombok.Data;
import org.openxmlformats.schemas.drawingml.x2006.chart.STRadarStyle;

/**
 * 雷达图参数
 *
 * @author Ankang
 * @date 2018/11/14
 */
@Data
@Builder
public class RadarChartOptions implements ChartOptions {
    /**
     * 图表样式
     */
    @Builder.Default
    private STRadarStyle.Enum style = STRadarStyle.STANDARD;
}
