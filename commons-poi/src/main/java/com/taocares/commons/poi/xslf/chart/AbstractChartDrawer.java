package com.taocares.commons.poi.xslf.chart;

import com.taocares.commons.poi.Constants;
import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.xslf.ChartUtils;
import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import com.taocares.commons.poi.xslf.chart.option.CommonChartOptions;
import com.taocares.commons.poi.xssf.SheetUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xssf.usermodel.*;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumn;

import java.util.List;
import java.util.Map;

/**
 * 图表绘制基类
 *
 * @author Ankang
 * @date 2018/11/9
 */
public abstract class AbstractChartDrawer {

    /**
     * 针对图表应用定制化的参数，应该在绘制之后执行
     *
     * @param chart   图表
     * @param options 参数
     */
    public void applyOptions(XSLFChart chart, ChartOptions options) {
        if (options instanceof CommonChartOptions) {
            CommonChartOptions commonChartOptions = (CommonChartOptions) options;
            LegendPosition legendPosition = commonChartOptions.getLegendPosition();
            if (legendPosition == null) {
                chart.deleteLegend();
            } else {
                chart.getOrAddLegend().setPosition(legendPosition);
            }
            String numberFormat = commonChartOptions.getNumberFormat();
            if (numberFormat != null) {
                // update sheet
                XSSFSheet sheet = ChartUtils.getSheet(chart);
                XSSFDataFormat dataFormat = sheet.getWorkbook().createDataFormat();
                short format = dataFormat.getFormat(numberFormat);
                XSSFCellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setDataFormat(format);
                for (Row row : sheet) {
                    for (Cell cell : row) {
                        if (cell.getCellType() == CellType.NUMERIC) {
                            cell.setCellStyle(cellStyle);
                        }
                    }
                }
                // update axis
                updateSerNum(chart, numberFormat);
                for (XDDFChartAxis chartAxis : chart.getAxes()) {
                    chartAxis.setNumberFormat(numberFormat);
                }
            }
        } else {
            applyCustomizeOptions(chart, options);
        }
    }

    /**
     * 针对图表应用定制化的参数，应该在绘制之后执行
     *
     * @param chart   图表
     * @param options 参数
     */
    protected abstract void applyCustomizeOptions(XSLFChart chart, ChartOptions options);

    /**
     * 根据传入数据绘制图表
     *
     * @param chart        图表
     * @param categoryData 类别
     * @param valuesData   数据（key：系列，value：数据）
     * @throws POIException 绘制过程发生异常
     */
    public <T extends Number> void drawChart(XSLFChart chart, List<?> categoryData, Map<String, List<T>> valuesData) {
        int categoryCount = categoryData.size();
        XSSFSheet sheet = ChartUtils.getSheet(chart);
        CategoryDataType categoryDataType = getCategoryDataType(categoryData.get(0));
        checkDataSize(categoryData, valuesData);
        checkDataTable(sheet, categoryCount, valuesData.size());
        CellRangeAddress cellRangeAddress = new CellRangeAddress(1, categoryCount, 0, 0);
        String range = chart.formatRange(cellRangeAddress);
        XDDFDataSource category = null;
        switch (categoryDataType) {
            case NUMERIC:
                category = XDDFDataSourcesFactory.fromArray(categoryData.toArray(new Number[categoryCount]), range);
                break;
            case STRING:
                category = XDDFDataSourcesFactory.fromArray(categoryData.toArray(new String[categoryCount]), range);
                break;
            default:
        }
        XDDFChartData chartData = getOrAddChartData(chart, categoryDataType);
        List<XDDFChartData.Series> seriesList = chartData.getSeries();
        int remaining = seriesList.size();
        int colIndex = 1;
        for (Map.Entry<String, List<T>> data : valuesData.entrySet()) {
            cellRangeAddress = new CellRangeAddress(1, categoryCount, colIndex, colIndex);
            range = chart.formatRange(cellRangeAddress);
            XDDFNumericalDataSource<Number> values = XDDFDataSourcesFactory.fromArray(data.getValue().toArray(new Number[categoryCount]), range, colIndex);
            XDDFChartData.Series series;
            if (remaining > 0) {
                series = seriesList.get(colIndex - 1);
                series.replaceData(category, values);
                remaining--;
            } else {
                series = chartData.addSeries(category, values);
            }
            setSerTitle(chart, data.getKey(), colIndex - 1, 0, colIndex);
            colIndex++;
        }
        // remove unused series from template
        for (int i = colIndex; i < colIndex + remaining; i++) {
            removeSer(chart, i - 1);
            chartData.getSeries().remove(i - 1);
        }

        chart.plot(chartData);
        // 对第一列重新赋值正确的格式
        fillCategories(sheet, categoryData, categoryDataType);
    }

    /**
     * 根据表格中指定的数据绘制图表
     *
     * @param chart         图表
     * @param categoryRange 类别的区域
     * @param valuesRange   数据的区域
     */
    public void drawChart(XSLFChart chart, CellRangeAddress categoryRange, CellRangeAddress valuesRange) {
        drawChart(chart, categoryRange, valuesRange, CategoryDataType.STRING);
    }

    /**
     * 根据表格中指定的数据绘制图表
     *
     * @param chart            图表
     * @param categoryRange    类别的区域
     * @param valuesRange      数据的区域
     * @param categoryDataType 分类数据的类型
     */
    public void drawChart(XSLFChart chart, CellRangeAddress categoryRange, CellRangeAddress valuesRange, CategoryDataType categoryDataType) {
        XSSFSheet sheet = ChartUtils.getSheet(chart);
        XDDFDataSource category = null;
        switch (categoryDataType) {
            case NUMERIC:
                category = XDDFDataSourcesFactory.fromNumericCellRange(sheet, categoryRange);
                break;
            case STRING:
                category = XDDFDataSourcesFactory.fromStringCellRange(sheet, categoryRange);
                break;
            default:
        }
        XDDFChartData chartData = getOrAddChartData(chart, categoryDataType);
        List<XDDFChartData.Series> seriesList = chartData.getSeries();
        int remaining = seriesList.size();
        int serIdx = 0;
        for (int colIndex = valuesRange.getFirstColumn(); colIndex <= valuesRange.getLastColumn(); colIndex++) {
            CellRangeAddress range = new CellRangeAddress(valuesRange.getFirstRow(), valuesRange.getLastRow(), colIndex, colIndex);
            XDDFNumericalDataSource<Double> values = XDDFDataSourcesFactory.fromNumericCellRange(sheet, range);
            XDDFChartData.Series series;
            if (remaining > 0) {
                series = seriesList.get(serIdx);
                series.replaceData(category, values);
                remaining--;
            } else {
                series = chartData.addSeries(category, values);
            }
            setSerTitle(chart, serIdx, valuesRange.getFirstRow() - 1, colIndex);
            serIdx++;
        }
        // remove unused series from template
        for (int i = valuesRange.getLastColumn(); i < valuesRange.getLastColumn() + remaining; i++) {
            removeSer(chart, i - 1);
            chartData.getSeries().remove(i - 1);
        }

        chart.plot(chartData);
    }

    /**
     * 检查数据长度是否正确
     *
     * @param categoryData 分类数据
     * @param valuesData   数值数据
     * @param <T>          数值数据类型
     * @throws POIException 数据数量不正确
     */
    private <T extends Number> void checkDataSize(List<?> categoryData, Map<String, List<T>> valuesData) {
        int size = categoryData.size();
        for (List<T> data : valuesData.values()) {
            if (data.size() != size) {
                throw new POIException("数据的数量(" + data.size() + ")和分类的数量(" + size + ")不一致！");
            }
        }
    }

    private void checkDataTable(XSSFSheet sheet, int rowCount, int colCount) {
        CellReference firstCell = new CellReference(0, 0);
        CellReference lastCell = new CellReference(rowCount, colCount);
        AreaReference area = new AreaReference(firstCell, lastCell, null);
        XSSFTable table;
        if (sheet.getTables().isEmpty()) {
            table = sheet.createTable(area);
            String tableName = Constants.TABLE_NAME + sheet.getTables().size();
            table.setName(tableName);
            table.setDisplayName(tableName);
            sheet.getCTWorksheet().getTableParts().setCount(1);
            SheetUtils.getCell(sheet, 0, 0).setCellValue(" ");
        } else {
            table = sheet.getTables().get(0);
            // remove unused data from template
            int prevRowCount = table.getRowCount();
            int prevColCount = table.getColumnCount();
            for (int i = prevRowCount - 1; i > rowCount; i--) {
                sheet.removeRow(sheet.getRow(i));
            }
            for (int i = prevColCount - 1; i > colCount; i--) {
                for (Row row : sheet) {
                    // 单元格不一定存在，需要判断
                    if (row.getCell(i) != null) {
                        row.removeCell(row.getCell(i));
                    }
                }
            }
            table.setArea(area);
        }
        List<XSSFTableColumn> columns = table.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            columns.get(i).setId(i + 1);
        }
    }

    /**
     * 将分类数据填充到数据页中
     *
     * @param sheet            数据页
     * @param categoryData     分类数据
     * @param categoryDataType 分类数据的类型
     */
    private void fillCategories(XSSFSheet sheet, List<?> categoryData, CategoryDataType categoryDataType) {
        int rowCount = categoryData.size();
        for (int i = 0; i < rowCount; i++) {
            XSSFCell cell = SheetUtils.getCell(sheet, i + 1, 0);
            if (categoryDataType == CategoryDataType.NUMERIC) {
                cell.setCellValue(((Number) categoryData.get(i)).doubleValue());
            } else if (categoryDataType == CategoryDataType.STRING) {
                cell.setCellValue(categoryData.get(i).toString());
            }
        }
    }

    /**
     * 获取或新增图表数据
     *
     * @param chart            图表
     * @param categoryDataType 分类的数据类型
     * @return 图表数据对象
     */
    private XDDFChartData getOrAddChartData(XSLFChart chart, CategoryDataType categoryDataType) {
        // POI bug，Category如果为数值类型，尝试获取数据会抛出NPE，需要清除
        // 副作用是部分样式会丢失
        if (categoryDataType == CategoryDataType.NUMERIC
                && !ChartUtils.getChartTypes(chart).isEmpty()) {
            clearSeries(chart);
        }
        for (XDDFChartData chartData : chart.getChartSeries()) {
            if (getChartDataClass() == chartData.getClass()) {
                return chartData;
            }
        }
        return addChartData(chart, categoryDataType);
    }

    /**
     * 新增图表数据
     *
     * @param chart            图表
     * @param categoryDataType 分类的数据类型
     * @return 图表数据对象
     */
    private XDDFChartData addChartData(XSLFChart chart, CategoryDataType categoryDataType) {
        XDDFChartData chartData;
        if (getChartType() == ChartTypes.PIE) {
            CTPieChart ctPieChart = chart.getCTChart().getPlotArea().addNewPieChart();
            chartData = new XDDFPieChartData(ctPieChart);
            customizeChart(chart, null, null);
        } else {
            XDDFChartAxis categoryAxis = getCategoryAxis(chart, categoryDataType);
            XDDFValueAxis valueAxis = getValueAxis(chart);
            chartData = chart.createData(getChartType(), categoryAxis, valueAxis);
            customizeChart(chart, categoryAxis, valueAxis);
        }
        return chartData;
    }

    /**
     * 图表初始化时应用基本的参数（设置坐标轴等）
     *
     * @param chart        图表
     * @param categoryAxis 类别（横）坐标轴
     * @param valueAxis    值（纵）坐标轴
     */
    protected abstract void customizeChart(XSLFChart chart, XDDFChartAxis categoryAxis, XDDFValueAxis valueAxis);

    private XDDFChartAxis getCategoryAxis(XSLFChart chart, CategoryDataType categoryDataType) {
        if (chart.getAxes().isEmpty()) {
            switch (categoryDataType) {
                case NUMERIC:
                    return chart.createValueAxis(AxisPosition.BOTTOM);
                case STRING:
                    return chart.createCategoryAxis(AxisPosition.BOTTOM);
                default:
                    return chart.createDateAxis(AxisPosition.BOTTOM);
            }
        } else {
            return chart.getAxes().get(0);
        }
    }

    private XDDFValueAxis getValueAxis(XDDFChart chart) {
        boolean first = true;
        for (XDDFChartAxis chartAxis : chart.getAxes()) {
            // 第一个坐标轴视为横轴
            if (first) {
                first = false;
            } else {
                if (chartAxis instanceof XDDFValueAxis) {
                    return (XDDFValueAxis) chartAxis;
                }
            }
        }
        XDDFValueAxis valueAxis = chart.createValueAxis(AxisPosition.LEFT);
        valueAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
        return valueAxis;
    }

    /**
     * 设置系列的标题，直接引用单元格的值
     *
     * @param chart  图表
     * @param serIdx 系列索引（从0开始）
     * @param rowIdx 行的序号（从1开始）
     * @param colIdx 列的序号（从1开始）
     */
    private void setSerTitle(XSLFChart chart, int serIdx, int rowIdx, int colIdx) {
        XSSFSheet sheet = ChartUtils.getSheet(chart);
        XSSFCell cell = SheetUtils.getCell(sheet, rowIdx, colIdx);
        setSerTitle(chart, cell.getStringCellValue(), serIdx, rowIdx, colIdx);
    }

    /**
     * 设置系列的标题，并更新单元格
     *
     * @param chart  图表
     * @param title  标题，如果为null，取列的第一行的值
     * @param serIdx 系列索引（从0开始）
     * @param rowIdx 行的序号（从1开始）
     * @param colIdx 列的序号（从1开始）
     */
    private void setSerTitle(XSLFChart chart, String title, int serIdx, int rowIdx, int colIdx) {
        CTSerTx ctSerTx = getSerTx(chart, serIdx);
        CTStrRef ctStrRef = ctSerTx.getStrRef();
        if (ctStrRef == null) {
            ctStrRef = ctSerTx.addNewStrRef();
        }
        XSSFSheet sheet = ChartUtils.getSheet(chart);
        ctStrRef.setF(SheetUtils.getCellReferenceString(sheet, rowIdx, colIdx));
        SheetUtils.getCell(sheet, rowIdx, colIdx).setCellValue(title);
        CTStrData ctStrData = ctStrRef.getStrCache();
        if (ctStrData == null) {
            ctStrData = ctStrRef.addNewStrCache();
        }
        CTUnsignedInt ptCount = ctStrData.getPtCount();
        if (ptCount == null) {
            ctStrData.addNewPtCount().setVal(1);
        }
        // Gitlab issue#13
        if (ctStrData.sizeOfPtArray() == 0) {
            CTStrVal ctStrVal = ctStrData.addNewPt();
            ctStrVal.setIdx(0);
            ctStrVal.setV(title);
        } else {
            ctStrData.getPtArray(0).setV(title);
        }
        // update related table
        CTTable ctTable = ChartUtils.getSheet(chart).getTables().get(0).getCTTable();
        CTTableColumn ctTableColumn = ctTable.getTableColumns().getTableColumnArray(colIdx);
        ctTableColumn.setName(title);
    }

    /**
     * 更新图表中系列的数字显示格式
     *
     * @param chart        图表
     * @param numberFormat 数字格式
     */
    private void updateSerNum(XSLFChart chart, String numberFormat) {
        for (CTAxDataSource catDS : getCatDS(chart)) {
            if (catDS.isSetNumLit()) {
                catDS.getNumLit().setFormatCode(numberFormat);
            }
            if (catDS.isSetNumRef()) {
                catDS.getNumRef().getNumCache().setFormatCode(numberFormat);
            }
        }
        for (CTNumDataSource valDS : getValDS(chart)) {
            if (valDS.isSetNumLit()) {
                valDS.getNumLit().setFormatCode(numberFormat);
            }
            if (valDS.isSetNumRef()) {
                valDS.getNumRef().getNumCache().setFormatCode(numberFormat);
            }
        }
    }

    /**
     * 清除图表中的所有数据
     *
     * @param chart 图表
     */
    protected abstract void clearSeries(XSLFChart chart);

    /**
     * 移除图表中的系列
     *
     * @param chart  图表
     * @param serIdx 系列的索引（从0开始）
     */
    protected abstract void removeSer(XSLFChart chart, int serIdx);

    /**
     * 图表中的系列的标题元素
     *
     * @param chart  图表
     * @param serIdx 系列的索引（从0开始）
     * @return 标题元素
     */
    protected abstract CTSerTx getSerTx(XSLFChart chart, int serIdx);

    /**
     * 从特定数值开始更新系列的序号并返回
     *
     * @param chart   图表
     * @param fromIdx 起始序号
     * @return 新的起始序号
     */
    public abstract long updateSerIdx(XSLFChart chart, long fromIdx);

    /**
     * 获取图表中所有分类的数据源
     *
     * @param chart 图表
     * @return 所有分类的数据源
     */
    protected abstract List<CTAxDataSource> getCatDS(XSLFChart chart);

    /**
     * 获取图表中所有数据的数据源
     *
     * @param chart 图表
     * @return 所有数据的数据源
     */
    protected abstract List<CTNumDataSource> getValDS(XSLFChart chart);

    /**
     * 获取图表类型
     */
    protected abstract ChartTypes getChartType();

    private Class<? extends XDDFChartData> getChartDataClass() {
        switch (getChartType()) {
            case BAR:
                return XDDFBarChartData.class;
            case LINE:
                return XDDFLineChartData.class;
            case PIE:
                return XDDFPieChartData.class;
            case RADAR:
                return XDDFRadarChartData.class;
            case SCATTER:
                return XDDFScatterChartData.class;
            default:
                return XDDFChartData.class;
        }
    }

    /**
     * 获取分类数据的类型
     *
     * @param categoryData 分类数据
     * @return 分类数据的类型
     */
    private CategoryDataType getCategoryDataType(Object categoryData) {
        if (categoryData instanceof Number) {
            return CategoryDataType.NUMERIC;
        } else if (categoryData instanceof String) {
            return CategoryDataType.STRING;
        } else {
            throw new POIException("不支持的数据类型！" + categoryData.getClass());
        }
    }

    /**
     * 分类数据类型
     */
    public enum CategoryDataType {
        STRING, NUMERIC
    }
}
