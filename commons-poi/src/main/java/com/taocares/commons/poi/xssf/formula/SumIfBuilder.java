package com.taocares.commons.poi.xssf.formula;

import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.xssf.FormulaHelper;
import org.apache.poi.ss.util.CellRangeAddress;

import static com.taocares.commons.poi.Constants.DELIMITER;

/**
 * SUMIF函数构建工具
 *
 * @author Ankang
 * @date 2018/11/22
 */
public class SumIfBuilder {

    private CellRangeAddress range;

    private Object criteria;

    private CellRangeAddress sumRange;

    /**
     * 设置待判定的数据范围
     *
     * @param cellRangeAddress 待判定的数据范围
     */
    public SumIfBuilder range(CellRangeAddress cellRangeAddress) {
        this.range = cellRangeAddress;
        return this;
    }

    /**
     * 设置判断条件，可以是某个单元格引用，也可以是数值、字符串形式的条件，<br/>
     * 如：A1, 2, ">1"
     *
     * @param criteria 判断条件
     */
    public SumIfBuilder criteria(Object criteria) {
        this.criteria = criteria;
        return this;
    }

    /**
     * 设置求和的数据范围，如果范围和待判定的范围一致，可以不设置
     *
     * @param cellRangeAddress 求和的数据范围
     */
    public SumIfBuilder sumRange(CellRangeAddress cellRangeAddress) {
        this.sumRange = cellRangeAddress;
        return this;
    }

    /**
     * 生成SUMIF公式
     *
     * @return 公式
     * @throws POIException 如果参数不完整
     */
    public String build() {
        if (range == null || criteria == null) {
            throw new POIException("公式参数不能为空！");
        }
        StringBuilder sb = new StringBuilder("SUMIF(");
        sb.append(range.formatAsString(null, true));
        sb.append(DELIMITER);
        sb.append(FormulaHelper.getFormulaString(criteria));
        if (sumRange != null) {
            sb.append(DELIMITER);
            sb.append(sumRange.formatAsString(null, true));
        }
        sb.append(")");
        return sb.toString();
    }
}
