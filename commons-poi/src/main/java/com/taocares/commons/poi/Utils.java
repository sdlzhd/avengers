package com.taocares.commons.poi;

import org.apache.poi.util.Units;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;

import java.awt.geom.Rectangle2D;

/**
 * 通用的工具类
 *
 * @author Ankang
 * @date 2018/11/8
 */
public class Utils {

    /**
     * 设置元素的几何属性（位置和宽高）
     *
     * @param ctGraphicalObjectFrame 页面元素
     * @param anchor                 位置信息
     */
    public static void setAnchor(CTGraphicalObjectFrame ctGraphicalObjectFrame, Rectangle2D anchor) {
        CTTransform2D xfrm = (ctGraphicalObjectFrame.getXfrm() != null) ? ctGraphicalObjectFrame.getXfrm() : ctGraphicalObjectFrame.addNewXfrm();
        CTPoint2D off = xfrm.isSetOff() ? xfrm.getOff() : xfrm.addNewOff();
        long x = Units.toEMU(anchor.getX());
        long y = Units.toEMU(anchor.getY());
        off.setX(x);
        off.setY(y);
        CTPositiveSize2D ext = xfrm.isSetExt() ? xfrm.getExt() : xfrm.addNewExt();
        long cx = Units.toEMU(anchor.getWidth());
        long cy = Units.toEMU(anchor.getHeight());
        ext.setCx(cx);
        ext.setCy(cy);
    }
}
