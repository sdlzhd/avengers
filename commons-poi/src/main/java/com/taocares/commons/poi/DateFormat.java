package com.taocares.commons.poi;

/**
 * 常用的日期格式
 *
 * @author Ankang
 * @date 2018/11/19
 */
public class DateFormat {

    public static final String DATE = "yyyy/mm/dd";

    public static final String TIME = "hh:mm:ss";

    public static final String DATE_TIME = "yyyy/mm/dd hh:mm:ss";
}
