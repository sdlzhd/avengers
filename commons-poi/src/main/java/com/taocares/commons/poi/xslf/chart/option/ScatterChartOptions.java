package com.taocares.commons.poi.xslf.chart.option;

import lombok.Builder;
import lombok.Data;

/**
 * 散点图参数
 *
 * @author Ankang
 * @date 2018/11/14
 */
@Data
@Builder
public class ScatterChartOptions implements ChartOptions {
    /**
     * 是否显示标记
     */
    @Builder.Default
    private boolean marker = true;
    /**
     * 是否显示连接线
     */
    @Builder.Default
    private boolean line = true;
    /**
     * 连接线是否平滑
     */
    @Builder.Default
    private boolean smooth = false;

}
