package com.taocares.commons.poi.xslf;

import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.xslf.chart.*;
import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.LegendPosition;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 图表工具<br/>
 *
 * @author Ankang
 * @date 2018/11/8
 */
public class ChartUtils {

    private static Map<ChartTypes, AbstractChartDrawer> drawerMap = new HashMap<>();

    static {
        drawerMap.put(ChartTypes.BAR, new BarChartDrawer());
        drawerMap.put(ChartTypes.LINE, new LineChartDrawer());
        drawerMap.put(ChartTypes.PIE, new PieChartDrawer());
        drawerMap.put(ChartTypes.RADAR, new RadarChartDrawer());
        drawerMap.put(ChartTypes.SCATTER, new ScatterChartDrawer());
    }

    /**
     * 根据传入的数据更新图表<br/>
     * 新建图表请使用{@link #drawChart(XSLFChart, ChartTypes, List, Map)}指定图表类型
     *
     * @param chart      图表
     * @param categories 分类名称列表
     * @param values     各个系列的数据，key为系列名，value为对应的数据
     * @param <T>        数据的类型
     */
    public static <T extends Number> void updateChart(XSLFChart chart, List<?> categories, Map<String, List<T>> values) {
        updateChart(chart, categories, values, null);
    }

    /**
     * 根据传入的数据更新图表，并应用参数<br/>
     * 新建图表请使用{@link #drawChart(XSLFChart, ChartTypes, List, Map, ChartOptions)}指定图表类型
     *
     * @param chart      图表
     * @param categories 分类名称列表
     * @param values     各个系列的数据，key为系列名，value为对应的数据
     * @param options    图表参数
     * @param <T>        数据的类型
     */
    public static <T extends Number> void updateChart(XSLFChart chart, List<?> categories, Map<String, List<T>> values, ChartOptions options) {
        for (ChartTypes chartType : getChartTypes(chart)) {
            drawChart(chart, chartType, categories, values, options);
        }
    }

    /**
     * 绘制复合图表
     *
     * @param chart      图表
     * @param chartTypes 图表类型列表
     * @param categories 分类名称列表
     * @param values     各个系列的数据，key为系列名，value为对应的数据
     * @param <T>        数据的类型
     */
    public static <T extends Number> void drawCompositeChart(XSLFChart chart, List<ChartTypes> chartTypes, List<?> categories, Map<String, List<T>> values) {
        drawCompositeChart(chart, chartTypes, categories, values, null);
    }

    /**
     * 绘制复合图表
     *
     * @param chart      图表
     * @param chartTypes 图表类型列表
     * @param categories 分类名称列表
     * @param values     各个系列的数据，key为系列名，value为对应的数据
     * @param options    图表参数
     * @param <T>        数据的类型
     */
    public static <T extends Number> void drawCompositeChart(XSLFChart chart, List<ChartTypes> chartTypes, List<?> categories, Map<String, List<T>> values, ChartOptions options) {
        boolean first = true;
        for (ChartTypes chartType : chartTypes) {
            if (first) {
                drawChart(chart, chartType, categories, values, options);
                first = false;
            } else {
                CellRangeAddress categoryRange = new CellRangeAddress(1, categories.size(), 0, 0);
                CellRangeAddress valuesRange = new CellRangeAddress(1, categories.size(), 1, values.size());
                drawChart(chart, chartType, categoryRange, valuesRange, options);
            }
        }
    }

    /**
     * 根据传入的数据绘制图表（更新或新增）
     *
     * @param chart      图表
     * @param chartType  图表类型
     * @param categories 分类名称列表
     * @param values     各个系列的数据，key为系列名，value为对应的数据
     * @param <T>        数据的类型
     */
    public static <T extends Number> void drawChart(XSLFChart chart, ChartTypes chartType, List<?> categories, Map<String, List<T>> values) {
        drawChart(chart, chartType, categories, values, null);
    }

    /**
     * 根据传入的数据绘制图表（更新或新增），并应用参数
     *
     * @param chart      图表
     * @param chartType  图表类型
     * @param categories 分类名称列表
     * @param values     各个系列的数据，key为系列名，value为对应的数据
     * @param options    图表参数
     * @param <T>        数据的类型
     */
    public static <T extends Number> void drawChart(XSLFChart chart, ChartTypes chartType, List<?> categories, Map<String, List<T>> values, ChartOptions options) {
        getChartDrawer(chartType).drawChart(chart, categories, values);
        chart.getOrAddLegend().setPosition(LegendPosition.BOTTOM);
        if (options != null) {
            applyOptions(chart, options);
        }
        updateSerIdx(chart);
    }

    /**
     * 根据表格中指定的数据绘制图表
     *
     * @param chart         图表
     * @param chartType     图表类型
     * @param categoryRange 类别的区域
     * @param valuesRange   数据的区域
     */
    public static void drawChart(XSLFChart chart, ChartTypes chartType, CellRangeAddress categoryRange, CellRangeAddress valuesRange) {
        drawChart(chart, chartType, categoryRange, valuesRange, null);
    }

    /**
     * 根据表格中指定的数据绘制图表
     *
     * @param chart         图表
     * @param chartType     图表类型
     * @param categoryRange 类别的区域
     * @param valuesRange   数据的区域
     * @param options       图表参数
     */
    public static void drawChart(XSLFChart chart, ChartTypes chartType, CellRangeAddress categoryRange, CellRangeAddress valuesRange, ChartOptions options) {
        getChartDrawer(chartType).drawChart(chart, categoryRange, valuesRange);
        if (options != null) {
            applyOptions(chart, options);
        }
        updateSerIdx(chart);
    }

    /**
     * 重新排列图表中的系列序号
     *
     * @param chart 图表
     */
    private static void updateSerIdx(XSLFChart chart) {
        long fromIdx = 0L;
        for (ChartTypes chartType : getChartTypes(chart)) {
            fromIdx = getChartDrawer(chartType).updateSerIdx(chart, fromIdx);
        }
    }

    /**
     * 对指定的图表应用个性化参数
     *
     * @param chart   图表
     * @param options 参数
     */
    public static void applyOptions(XSLFChart chart, ChartOptions options) {
        for (ChartTypes chartType : getChartTypes(chart)) {
            getChartDrawer(chartType).applyOptions(chart, options);
        }
    }

    /**
     * 为指定的图表设置标题
     *
     * @param chart 图表
     * @param title 标题
     */
    public static void setChartTitle(XSLFChart chart, String title) {
        CTChart ctChart = chart.getCTChart();
        CTTitle ctTitle = ctChart.getTitle() == null ? ctChart.addNewTitle() : ctChart.getTitle();
        CTTx ctTx = ctTitle.getTx() == null ? ctTitle.addNewTx() : ctTitle.getTx();
        CTTextBody ctTextBody = ctTx.getRich() == null ? ctTx.addNewRich() : ctTx.getRich();
        if (ctTextBody.getBodyPr() == null) {
            // body properties must exist, but can be empty
            ctTextBody.addNewBodyPr();
        }
        CTTextParagraph ctTextParagraph = ctTextBody.sizeOfPArray() == 0 ? ctTextBody.addNewP() : ctTextBody.getPArray(0);
        CTRegularTextRun ctRegularTextRun = ctTextParagraph.sizeOfRArray() == 0 ? ctTextParagraph.addNewR() : ctTextParagraph.getRArray(0);
        ctRegularTextRun.setT(title);

        CTBoolean overlay = ctTitle.getOverlay() == null ? ctTitle.addNewOverlay() : ctTitle.getOverlay();
        overlay.setVal(false);
    }

    /**
     * 获取图表数据对应的数据表格（如果不存在，会自动创建）
     *
     * @param chart 图表
     * @return 对应的数据表格
     */
    public static XSSFSheet getSheet(XSLFChart chart) {
        try {
            return chart.getWorkbook().getSheetAt(0);
        } catch (Exception e) {
            throw new POIException("获取图表数据出错！", e);
        }
    }

    private static AbstractChartDrawer getChartDrawer(ChartTypes chartType) {
        AbstractChartDrawer chartDrawer = drawerMap.get(chartType);
        if (chartDrawer == null) {
            throw new POIException("不支持的图表类型：" + chartType);
        }
        return chartDrawer;
    }

    /**
     * 获取图表中存在的图表类型
     *
     * @param chart 图表
     * @return 图表类型集合
     */
    public static List<ChartTypes> getChartTypes(XSLFChart chart) {
        CTPlotArea ctPlotArea = chart.getCTChart().getPlotArea();
        List<ChartTypes> chartTypes = new ArrayList<>();
        if (ctPlotArea.sizeOfBarChartArray() > 0) {
            chartTypes.add(ChartTypes.BAR);
        }
        if (ctPlotArea.sizeOfLineChartArray() > 0) {
            chartTypes.add(ChartTypes.LINE);
        }
        if (ctPlotArea.sizeOfPieChartArray() > 0) {
            chartTypes.add(ChartTypes.PIE);
        }
        if (ctPlotArea.sizeOfRadarChartArray() > 0) {
            chartTypes.add(ChartTypes.RADAR);
        }
        if (ctPlotArea.sizeOfScatterChartArray() > 0) {
            chartTypes.add(ChartTypes.SCATTER);
        }
        return chartTypes;
    }
}
