package com.taocares.commons.poi.xslf.chart;

import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import com.taocares.commons.poi.xslf.chart.option.ScatterChartOptions;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.XDDFChartAxis;
import org.apache.poi.xddf.usermodel.chart.XDDFValueAxis;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * 散点图绘制类
 *
 * @author Ankang
 * @date 2018/11/9
 */
public class ScatterChartDrawer extends AbstractChartDrawer {

    @Override
    protected CTSerTx getSerTx(XSLFChart chart, int serIdx) {
        CTScatterSer ser = getCtChart(chart).getSerArray(serIdx);
        CTSerTx ctSerTx = ser.getTx();
        if (ctSerTx == null) {
            ctSerTx = ser.addNewTx();
        }
        return ctSerTx;
    }

    @Override
    public long updateSerIdx(XSLFChart chart, long fromIdx) {
        for (CTScatterSer ser : getCtChart(chart).getSerList()) {
            ser.getIdx().setVal(fromIdx);
            ser.getOrder().setVal(fromIdx);
            fromIdx++;
        }
        return fromIdx;
    }

    @Override
    protected List<CTAxDataSource> getCatDS(XSLFChart chart) {
        List<CTAxDataSource> ctAxDataSources = new ArrayList<>();
        for (CTScatterSer ser : getCtChart(chart).getSerList()) {
            ctAxDataSources.add(ser.getXVal());
        }
        return ctAxDataSources;
    }

    @Override
    protected List<CTNumDataSource> getValDS(XSLFChart chart) {
        List<CTNumDataSource> ctNumDataSources = new ArrayList<>();
        for (CTScatterSer ser : getCtChart(chart).getSerList()) {
            ctNumDataSources.add(ser.getYVal());
        }
        return ctNumDataSources;
    }

    @Override
    public void applyCustomizeOptions(XSLFChart chart, ChartOptions options) {
        if (options instanceof ScatterChartOptions) {
            ScatterChartOptions scatterChartOptions = (ScatterChartOptions) options;
            CTScatterChart ctScatterChart = getCtChart(chart);

            for (int i = 0; i < ctScatterChart.sizeOfSerArray(); i++) {
                CTScatterSer ctScatterSer = ctScatterChart.getSerArray(i);

                CTBoolean smooth = ctScatterSer.isSetSmooth() ?
                        ctScatterSer.getSmooth() : ctScatterSer.addNewSmooth();
                smooth.setVal(scatterChartOptions.isSmooth());

                CTShapeProperties ctShapeProperties = ctScatterSer.isSetSpPr() ?
                        ctScatterSer.getSpPr() : ctScatterSer.addNewSpPr();
                CTLineProperties ctLineProperties = ctShapeProperties.isSetLn() ?
                        ctShapeProperties.getLn() : ctShapeProperties.addNewLn();
                if (scatterChartOptions.isLine()) {
                    if (!ctLineProperties.isSetSolidFill()) {
                        ctLineProperties.addNewSolidFill();
                    }
                    if (ctLineProperties.isSetNoFill()) {
                        ctLineProperties.unsetNoFill();
                    }
                } else {
                    if (!ctLineProperties.isSetNoFill()) {
                        ctLineProperties.addNewNoFill();
                    }
                    if (ctLineProperties.isSetSolidFill()) {
                        ctLineProperties.unsetSolidFill();
                    }
                }
            }
        }
    }

    @Override
    protected void customizeChart(XSLFChart chart, XDDFChartAxis categoryAxis, XDDFValueAxis valueAxis) {
        CTScatterChart ctScatterChart = getCtChart(chart);
        ctScatterChart.addNewAxId().setVal(categoryAxis.getId());
        ctScatterChart.addNewAxId().setVal(valueAxis.getId());
        ctScatterChart.addNewScatterStyle().setVal(STScatterStyle.LINE_MARKER);
        ctScatterChart.addNewVaryColors().setVal(false);
    }

    @Override
    protected void clearSeries(XSLFChart chart) {
        CTScatterChart ctScatterChart = getCtChart(chart);
        for (int i = ctScatterChart.sizeOfSerArray() - 1; i >= 0; i--) {
            ctScatterChart.removeSer(i);
        }
    }

    @Override
    protected void removeSer(XSLFChart chart, int serIdx) {
        getCtChart(chart).removeSer(serIdx);
    }

    @Override
    protected ChartTypes getChartType() {
        return ChartTypes.SCATTER;
    }

    private CTScatterChart getCtChart(XSLFChart chart) {
        return chart.getCTChart().getPlotArea().getScatterChartArray(0);
    }
}
