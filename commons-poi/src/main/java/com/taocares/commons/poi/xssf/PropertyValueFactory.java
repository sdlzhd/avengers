package com.taocares.commons.poi.xssf;

import com.taocares.commons.poi.xssf.model.ColumnData;
import com.taocares.commons.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 根据配置获取POJO中的属性并转换为Object[]<br/>
 * 注意：此类直接使用反射获取字段的值，不使用getter
 *
 * @author Ankang
 * @date 2018/11/23
 */
public class PropertyValueFactory implements ValueFactory {
    /**
     * 属性列表
     */
    private List<String> properties = new ArrayList<>();

    /**
     * 存储列的格式，key为属性名，value为格式
     */
    private Map<String, String> formatMap = new HashMap<>();

    /**
     * 创建工厂类
     *
     * @param properties 属性列表
     */
    public PropertyValueFactory(String... properties) {
        this.properties.addAll(Arrays.asList(properties));
    }

    /**
     * 创建工厂类
     *
     * @param properties 属性列表
     */
    public PropertyValueFactory(Collection<String> properties) {
        this.properties.addAll(properties);
    }

    /**
     * 为特定属性设置格式
     *
     * @param property 属性
     * @param format   格式
     */
    public void setFormat(String property, String format) {
        this.formatMap.put(property, format);
    }

    /**
     * 为属性设置格式
     *
     * @param formatMap 属性和格式
     */
    public void setFormat(Map<String, String> formatMap) {
        this.formatMap.putAll(formatMap);
    }

    /**
     * 根据已经设置的属性列表，获取POJO中的对应值并返回
     *
     * @param data POJO对象列表
     * @return Object[]列表
     */
    @Override
    public List<Object[]> call(List<?> data) {
        Map<String, Field> fieldMap = new HashMap<>();
        List<Object[]> result = new ArrayList<>();
        for (Object item : data) {
            if (item == null) continue;

            List<Object> temp = new ArrayList<>();
            for (String property : properties) {
                if (property == null) continue;

                try {
                    Field field = fieldMap.get(property);
                    if (field == null) {
                        field = ReflectionUtils.getField(item.getClass(), property);
                        fieldMap.put(property, field);
                    }
                    Object value = ReflectionUtils.getFieldValue(field, item);
                    if (formatMap.containsKey(property)) {
                        temp.add(new ColumnData(value, formatMap.get(property)));
                    } else {
                        temp.add(value);
                    }
                } catch (NoSuchFieldException e) {
                    // 忽略无法获取的属性
                }
            }
            result.add(temp.toArray());
        }
        return result;
    }

    @Override
    public String toString() {
        return "PropertyValueFactory{" +
                "properties=" + properties +
                '}';
    }
}
