package com.taocares.commons.poi.xssf;

import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.xssf.formula.*;
import com.taocares.commons.poi.xssf.model.ConditionData;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;

/**
 * 公式快捷插入工具，提供常用的公式使用方式<br/>
 * 此工具中未提供的特殊的公式需求，可以使用formula builder生成公式，<br/>
 * 使用{@link org.apache.poi.ss.usermodel.Cell#setCellFormula(String)}设置公式
 *
 * @author Ankang
 * @date 2018/11/21
 */
public class FormulaHelper {

    /**
     * 在指定行插入求和行（对数据区域的每列求和）<br/>
     * 注意：数据区域是指所有数据行和需要求和的数据列的范围，并非求和区域<br/>
     * 如：sheet中的数据范围A2:D11，需要在第12行计算第2-11行数据的和<br/>
     * <pre>
     * // 数据区域是A2:D11，行索引1-10共10行，列索引0-3共4列
     * CellRangeAddress range = new CellRangeAddress(1, 10, 0, 3);
     * FormulaHelper.addSumRow(sheet, 11, range);
     * </pre>
     *
     * @param sheet      数据表格
     * @param formulaRow 求和列索引（从0开始）
     * @param range      数据区域（不是指“求和区域”）
     */
    public static void addSumRow(XSSFSheet sheet, int formulaRow, CellRangeAddress range) {
        for (int colIdx = range.getFirstColumn(); colIdx <= range.getLastColumn(); colIdx++) {
            String formula = new SumBuilder()
                    .range(new CellRangeAddress(range.getFirstRow(), range.getLastRow(), colIdx, colIdx))
                    .build();
            SheetUtils.getCell(sheet, formulaRow, colIdx).setCellFormula(formula);
        }
    }

    /**
     * 在指定列插入求和列（对数据区域的每行求和），<br/>
     * 注意：数据区域是指所有数据行和特定数据列的范围，并非求和区域<br/>
     * 如：sheet中的数据范围A2:D11，需要在第5列计算对第2/3/4列的和<br/>
     * <pre>
     * // 数据区域是A3:D11，行索引1-10共10行，列索引1-3共3列
     * CellRangeAddress range = new CellRangeAddress(1, 10, 1, 3);
     * FormulaHelper.addSumColumn(sheet, 4, range);
     * </pre>
     *
     * @param sheet      数据表格
     * @param formulaCol 求和列索引（从0开始）
     * @param range      数据区域（不是指“求和区域”）
     */
    public static void addSumColumn(XSSFSheet sheet, int formulaCol, CellRangeAddress range) {
        for (int rowIdx = range.getFirstRow(); rowIdx <= range.getLastRow(); rowIdx++) {
            String formula = new SumBuilder()
                    .range(new CellRangeAddress(rowIdx, rowIdx, range.getFirstColumn(), range.getLastColumn()))
                    .build();
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(formula);
        }
    }

    /**
     * 在指定列插入求和列，并指定参与求和的列的索引
     *
     * @param sheet      数据表格
     * @param formulaCol 求和列索引（从0开始）
     * @param firstRow   需要插入公式的首行索引（从0开始）
     * @param lastRow    需要插入公式的末行索引（从0开始）
     * @param sumCols    参与求和的列的索引
     */
    public static void addSumColumn(XSSFSheet sheet, int formulaCol, int firstRow, int lastRow, int... sumCols) {
        for (int rowIdx = firstRow; rowIdx <= lastRow; rowIdx++) {
            SumBuilder sb = new SumBuilder();
            for (int colIdx : sumCols) {
                sb.cell(new CellReference(rowIdx, colIdx));
            }
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(sb.build());
        }
    }

    /**
     * 在指定行插入求平均数行（对数据区域的每列求平均数）<br/>
     * 注意：数据区域是指所有数据行和需要求平均数的数据列的范围，并非求平均数区域<br/>
     * 如：sheet中的数据范围A2:D11，需要在第12行计算第2-11行数据的平均数<br/>
     * <pre>
     * // 数据区域是A2:D11，行索引1-10共10行，列索引0-3共4列
     * CellRangeAddress range = new CellRangeAddress(1, 10, 0, 3);
     * FormulaHelper.addSumRow(sheet, 11, range);
     * </pre>
     *
     * @param sheet      数据表格
     * @param formulaRow 求平均数列索引（从0开始）
     * @param range      数据区域（不是指“求平均数区域”）
     */
    public static void addAverageRow(XSSFSheet sheet, int formulaRow, CellRangeAddress range) {
        for (int colIdx = range.getFirstColumn(); colIdx <= range.getLastColumn(); colIdx++) {
            String formula = new AverageBuilder()
                    .range(new CellRangeAddress(range.getFirstRow(), range.getLastRow(), colIdx, colIdx))
                    .build();
            SheetUtils.getCell(sheet, formulaRow, colIdx).setCellFormula(formula);
        }
    }

    /**
     * 在指定列插入求平均数列（对数据区域的每行求平均数），<br/>
     * 注意：数据区域是指所有数据行和特定数据列的范围，并非求平均数区域<br/>
     * 如：sheet中的数据范围A2:D11，需要在第5列计算对第2/3/4列的平均数<br/>
     * <pre>
     * // 数据区域是A3:D11，行索引1-10共10行，列索引1-3共3列
     * CellRangeAddress range = new CellRangeAddress(1, 10, 1, 3);
     * FormulaHelper.addAverageColumn(sheet, 4, range);
     * </pre>
     *
     * @param sheet      数据表格
     * @param formulaCol 求平均数列索引（从0开始）
     * @param range      数据区域（不是指“求平均数区域”）
     */
    public static void addAverageColumn(XSSFSheet sheet, int formulaCol, CellRangeAddress range) {
        for (int rowIdx = range.getFirstRow(); rowIdx <= range.getLastRow(); rowIdx++) {
            String formula = new AverageBuilder()
                    .range(new CellRangeAddress(rowIdx, rowIdx, range.getFirstColumn(), range.getLastColumn()))
                    .build();
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(formula);
        }
    }

    /**
     * 在指定列插入求平均数列，并指定参与求平均数的列的索引
     *
     * @param sheet      数据表格
     * @param formulaCol 求平均数列索引（从0开始）
     * @param firstRow   需要插入公式的首行索引（从0开始）
     * @param lastRow    需要插入公式的末行索引（从0开始）
     * @param avgCols    参与求平均数的列的索引
     */
    public static void addAverageColumn(XSSFSheet sheet, int formulaCol, int firstRow, int lastRow, int... avgCols) {
        for (int rowIdx = firstRow; rowIdx <= lastRow; rowIdx++) {
            AverageBuilder sb = new AverageBuilder();
            for (int colIdx : avgCols) {
                sb.cell(new CellReference(rowIdx, colIdx));
            }
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(sb.build());
        }
    }

    /**
     * 对表格中参考列相同的数据的指定（求和）列进行求和，并将求和结果进行合并
     *
     * @param sheet      　表格
     * @param formulaCol 公式列索引（从0开始）
     * @param firstRow   数据首行索引（从0开始）
     * @param lastRow    数据末行索引（从0开始）
     * @param refCol     参考列
     * @param sumCol     求和列
     */
    public static void addSumByRefColumn(XSSFSheet sheet, int formulaCol, int firstRow, int lastRow, int refCol, int sumCol) {
        CellRangeAddress range = new CellRangeAddress(firstRow, lastRow, refCol, refCol);
        CellRangeAddress sumRange = new CellRangeAddress(firstRow, lastRow, sumCol, sumCol);
        for (int rowIdx = firstRow; rowIdx <= lastRow; rowIdx++) {
            String formula = new SumIfBuilder()
                    .range(range)
                    .criteria(new CellReference(rowIdx, refCol))
                    .sumRange(sumRange)
                    .build();
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(formula);
        }
        SheetUtils.groupData(sheet, formulaCol, refCol);
    }

    /**
     * 向指定的列添加IF函数
     *
     * @param sheet        表格
     * @param formulaCol   公式列索引（从0开始）
     * @param firstRow     数据首行索引（从0开始）
     * @param lastRow      数据末行索引（从0开始）
     * @param testCol      判断条件列索引（从0开始）
     * @param operator     比较操作符
     * @param testValue    比较的值
     * @param valueIfTrue  如果为真的结果
     * @param valueIfFalse 如果为假的结果
     */
    public static void addIfColumn(XSSFSheet sheet, int formulaCol, int firstRow, int lastRow,
                                   int testCol, Criteria.Operator operator, Object testValue,
                                   Object valueIfTrue, Object valueIfFalse) {
        for (int rowIdx = firstRow; rowIdx <= lastRow; rowIdx++) {
            CellReference cellReference = new CellReference(rowIdx, testCol);
            String formula = new IfBuilder()
                    .criteria(Criteria.of(operator, cellReference, testValue))
                    .valueIfTrue(valueIfTrue)
                    .valueIfFalse(valueIfFalse)
                    .build();
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(formula);
        }
    }

    /**
     * 向指定的列添加多重IF函数
     *
     * @param sheet        表格
     * @param formulaCol   公式列索引（从0开始）
     * @param firstRow     数据首行索引（从0开始）
     * @param lastRow      数据末行索引（从0开始）
     * @param testCol      判断条件列索引（从0开始）
     * @param conditions   判断条件数据列表
     * @param defaultValue 默认值（所有条件均不满足的值）
     */
    public static void addIfColumn(XSSFSheet sheet, int formulaCol, int firstRow, int lastRow,
                                   int testCol, List<ConditionData> conditions, Object defaultValue) {
        for (int rowIdx = firstRow; rowIdx <= lastRow; rowIdx++) {
            CellReference cellReference = new CellReference(rowIdx, testCol);
            SwitchBuilder sb = new SwitchBuilder();
            sb.cellReference(cellReference);
            for (ConditionData condition : conditions) {
                sb.condition(condition.getOperator(), condition.getTestValue(), condition.getValueIfTrue());
            }
            sb.defaultValue(defaultValue);
            SheetUtils.getCell(sheet, rowIdx, formulaCol).setCellFormula(sb.build());
        }
    }

    /**
     * 获取对象在公式中的字符串
     *
     * @param object 对象
     * @return 在公式中的字符串
     * @throws POIException 如果参数为null
     */
    public static String getFormulaString(Object object) {
        if (object == null) {
            throw new POIException("公式的参数不能为null！");
        }
        if (object instanceof CellReference) {
            return ((CellReference) object).formatAsString();
        } else if (object instanceof String) {
            return "\"" + object + "\"";
        } else {
            return object.toString();
        }
    }
}
