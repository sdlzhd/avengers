package com.taocares.commons.poi.demo;

import com.taocares.commons.poi.DateFormat;
import com.taocares.commons.poi.demo.data.DemoData;
import com.taocares.commons.poi.xssf.FormulaHelper;
import com.taocares.commons.poi.xssf.PropertyValueFactory;
import com.taocares.commons.poi.xssf.SheetUtils;
import com.taocares.commons.poi.xssf.formula.Criteria;
import com.taocares.commons.poi.xssf.model.ConditionData;
import com.taocares.commons.poi.xssf.model.TableHeader;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Excel创建演示
 *
 * @author Ankang
 * @date 2018/11/19
 */
public class ExcelCreateDemo {

    public static void main(String[] args) {
//        // 表头示例
//        complexHeaderDemo();
//        // 数据分组示例
//        groupDataDemo();
//        // 求和、求平均值示例
//        calcDataDemo();
//        // 按照参考列进行分组求和示例
//        sumDataByRefColumnDemo();
//        // 条件和组合条件示例
//        conditionDataDemo();
        // 使用值工厂的完整的示例
        excelCreateDemo();
    }

    private static void excelCreateDemo() {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        // 绘制表头
        TableHeader root = new TableHeader("");
        root.getChildren().add(new TableHeader("姓名", 2));
        root.getChildren().add(new TableHeader("年龄", 2));
        root.getChildren().add(new TableHeader("出生日期", 2));
        TableHeader scoreHeader = new TableHeader("成绩");
        scoreHeader.getChildren().add(new TableHeader("语文"));
        scoreHeader.getChildren().add(new TableHeader("数学"));
        scoreHeader.getChildren().add(new TableHeader("英语"));
        root.getChildren().add(scoreHeader);
        root.getChildren().add(new TableHeader("平均分", 2));
        root.getChildren().add(new TableHeader("级别", 2));
        SheetUtils.setHeader(sheet, root, false);
        // 填充数据
        List<DemoData> data = new ArrayList<>();
        data.add(new DemoData(2L, "张三", 19, 90, 100, 92, new Date()));
        data.add(new DemoData(1L, "李四", 18, 95, 80, 92, new Date()));
        data.add(new DemoData(6L, "王老五", 19, 75, 88, 77, new Date()));
        data.add(new DemoData(5L, "孙六", 18, 72, 48, 93, new Date()));
        data.add(new DemoData(4L, "郑七", 19, 66, 78, 51, new Date()));
        data.add(new DemoData(3L, "王八", 18, 59, 31, 87, new Date()));
        PropertyValueFactory valueFactory = new PropertyValueFactory("name", "age", "birthday", "score1", "score2", "score3");
        // 设置生日列的格式，需要在call方法调用之前
        valueFactory.setFormat("birthday", DateFormat.DATE);
        List<Object[]> sheetData = valueFactory.call(data);
        SheetUtils.setData(sheet, sheetData, 2, 0);
        // 成绩求平均
        FormulaHelper.addAverageColumn(sheet, 6, new CellRangeAddress(2, 7, 3, 5));
        // 成绩等级
        List<ConditionData> conditions = new ArrayList<>();
        conditions.add(new ConditionData(Criteria.Operator.GE, 90, "优秀"));
        conditions.add(new ConditionData(Criteria.Operator.GE, 75, "良好"));
        conditions.add(new ConditionData(Criteria.Operator.GE, 60, "及格"));
        FormulaHelper.addIfColumn(sheet, 7, 2, 7, 6, conditions, "不及格");
        // 相同等级合并
        SheetUtils.groupData(sheet, 7);

        try (OutputStream out = new FileOutputStream("sheet-create-demo-output.xlsx")) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 演示表头的创建
     */
    private static void complexHeaderDemo() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        TableHeader root = new TableHeader("表格Root", 2);
        root.getChildren().add(new TableHeader("第一列", 2));
        root.getChildren().add(new TableHeader("第二列", 3));
        TableHeader grouped = new TableHeader("第三列", 2);
        grouped.getChildren().add(new TableHeader("3-1"));
        grouped.getChildren().add(new TableHeader("3-2"));
        grouped.getChildren().add(new TableHeader("3-3"));
        root.getChildren().add(grouped);
        TableHeader lvl2 = new TableHeader("第四列");
        TableHeader lvl3 = new TableHeader("4-1");
        TableHeader lvl4 = new TableHeader("4-1-1");
        root.getChildren().add(lvl2);
        lvl2.getChildren().add(lvl3);
        lvl3.getChildren().add(lvl4);
//        for (TableHeader header : root.getChildren()) {
//            System.err.println(header.getName()
//                    + ": row span " + header.getRowSpan()
//                    + "; column span " + header.getColSpan());
//        }
//        System.err.println(root);

        // 不显示根节点，从表格左上角开始绘制表头
        SheetUtils.setHeader(sheet, root, false);
        // 显示根节点，从第6行第7列开始绘制表头
        SheetUtils.setHeader(sheet, root, 5, 6, true);

        try (OutputStream out = new FileOutputStream("sheet-header-demo-output.xlsx")) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 演示数据的创建和相同数据合并
     */
    private static void groupDataDemo() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        TableHeader root = new TableHeader("");
        root.getChildren().add(new TableHeader("第一列"));
        root.getChildren().add(new TableHeader("第二列"));
        root.getChildren().add(new TableHeader("第三列"));
        root.getChildren().add(new TableHeader("第四列"));
        SheetUtils.setHeader(sheet, root, false);

        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{1, 2, 3, 4});
        data.add(new Object[]{1, 2, 3, 5});
        data.add(new Object[]{1, 3, 3, 4});
        data.add(new Object[]{2, 3, "3", 5});
        data.add(new Object[]{2, 3, 3, 5});
        SheetUtils.setData(sheet, data, 1, 0);
        SheetUtils.groupData(sheet, 0);
//        SheetUtils.groupData(sheet, 1);
        SheetUtils.groupData(sheet, 2);
//        SheetUtils.groupData(sheet, 3);

        try (OutputStream out = new FileOutputStream("sheet-data-demo-output.xlsx")) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 演示SUM公式的使用
     */
    private static void calcDataDemo() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        TableHeader root = new TableHeader("");
        root.getChildren().add(new TableHeader("分组"));
        root.getChildren().add(new TableHeader("第一列"));
        root.getChildren().add(new TableHeader("第二列"));
        root.getChildren().add(new TableHeader("第三列"));
        root.getChildren().add(new TableHeader("第四列"));
        root.getChildren().add(new TableHeader("合计1"));
        root.getChildren().add(new TableHeader("合计2"));
        root.getChildren().add(new TableHeader("平均"));
        SheetUtils.setHeader(sheet, root, false);

        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{"第一行", 1, 2, 3, 4});
        data.add(new Object[]{"第二行", 1, 2, null, 5});
        data.add(new Object[]{"第三行", 1, 3, 3, 4});
        data.add(new Object[]{"第四行", 2, 3, 3, 5});
        data.add(new Object[]{"第五行", 2, 3, 3, 5});
        data.add(new Object[]{"汇总"});
        SheetUtils.setData(sheet, data, 1, 0);
        // 指定求和区域（第1-4列）
        FormulaHelper.addSumColumn(sheet, 5, new CellRangeAddress(1, 5, 1, 4));
        // 指定求和列（第2/5列）
        FormulaHelper.addSumColumn(sheet, 6, 1, 5, 2, 4);
        // 添加汇总行
        FormulaHelper.addSumRow(sheet, 6, new CellRangeAddress(1, 5, 1, 6));
        // 在第8列求第1-4列的平均数
        FormulaHelper.addAverageColumn(sheet, 7, new CellRangeAddress(1, 5, 1, 4));

        try (OutputStream out = new FileOutputStream("sheet-calc-demo-output.xlsx")) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 演示SUMIF公式的使用
     */
    private static void sumDataByRefColumnDemo() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        TableHeader root = new TableHeader("");
        root.getChildren().add(new TableHeader("航司"));
        root.getChildren().add(new TableHeader("机型"));
        root.getChildren().add(new TableHeader("数量"));
        root.getChildren().add(new TableHeader("总数"));
        SheetUtils.setHeader(sheet, root, false);

        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{"东航", "B737", 12});
        data.add(new Object[]{"东航", "B738", 33});
        data.add(new Object[]{"东航", "A320", 10});
        data.add(new Object[]{"山航", "B738", 30});
        data.add(new Object[]{"山航", "A320", 25});
        SheetUtils.setData(sheet, data, 1, 0);
        // 参考第1列的值，在第4列对第3列求和，并合并第4列的结果
        FormulaHelper.addSumByRefColumn(sheet, 3, 1, 5, 0, 2);

        try (OutputStream out = new FileOutputStream("sheet-sum-by-ref-demo-output.xlsx")) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 演示IF公式的使用
     */
    private static void conditionDataDemo() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        TableHeader root = new TableHeader("");
        root.getChildren().add(new TableHeader("姓名"));
        root.getChildren().add(new TableHeader("性别"));
        root.getChildren().add(new TableHeader("分数"));
        root.getChildren().add(new TableHeader("性别（英文）"));
        root.getChildren().add(new TableHeader("级别"));
        SheetUtils.setHeader(sheet, root, false);

        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{"张三", "男", 92});
        data.add(new Object[]{"李四", "女", 85});
        data.add(new Object[]{"王五", "男", 83});
        data.add(new Object[]{"孙六", "女", 73});
        data.add(new Object[]{"郑七", "男", 50});
        SheetUtils.setData(sheet, data, 1, 0);
        // 在第4列，将第2列的性别转换为英文
        FormulaHelper.addIfColumn(sheet, 3, 1, 5, 1, Criteria.Operator.EQ, "男", "MALE", "FEMALE");

//        if (score >= 90) {
//            result = "优秀";
//        } else if (score >= 75) {
//            result = "良好";
//        } else if (score >= 60) {
//            result = "及格";
//        } else {
//            result = "不及格";
//        }
        // 条件支持Builder和Constructor两种创建方式
        List<ConditionData> conditions = new ArrayList<>();
        conditions.add(ConditionData.builder()
                .operator(Criteria.Operator.GE)
                .testValue(90)
                .valueIfTrue("优秀").build());
        conditions.add(new ConditionData(Criteria.Operator.GE, 75, "良好"));
        conditions.add(new ConditionData(Criteria.Operator.GE, 60, "及格"));
        // 在第5列，判断第3列的分数，转换为对应的成绩等级
        FormulaHelper.addIfColumn(sheet, 4, 1, 5, 2, conditions, "不及格");
        // 对成绩等级进行分组
        SheetUtils.groupData(sheet, 4);

        try (OutputStream out = new FileOutputStream("sheet-condition-demo-output.xlsx")) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
