package com.taocares.commons.poi.xssf.model;

import java.util.List;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/11/20
 */
public interface RecursiveData<T> {

    List<T> getChildren();
}
