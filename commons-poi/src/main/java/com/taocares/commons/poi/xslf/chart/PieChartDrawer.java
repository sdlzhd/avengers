package com.taocares.commons.poi.xslf.chart;

import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import com.taocares.commons.poi.xslf.chart.option.PieChartOptions;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.XDDFChartAxis;
import org.apache.poi.xddf.usermodel.chart.XDDFValueAxis;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 饼形图绘制类
 *
 * @author Ankang
 * @date 2018/11/9
 */
public class PieChartDrawer extends AbstractChartDrawer {

    @Override
    protected CTSerTx getSerTx(XSLFChart chart, int serIdx) {
        CTPieSer ser = getCtChart(chart).getSerArray(serIdx);
        CTSerTx ctSerTx = ser.getTx();
        if (ctSerTx == null) {
            ctSerTx = ser.addNewTx();
        }
        return ctSerTx;
    }

    @Override
    public long updateSerIdx(XSLFChart chart, long fromIdx) {
        for (CTPieSer ser : getCtChart(chart).getSerList()) {
            ser.getIdx().setVal(fromIdx);
            ser.getOrder().setVal(fromIdx);
            fromIdx++;
        }
        return fromIdx;
    }

    @Override
    protected List<CTAxDataSource> getCatDS(XSLFChart chart) {
        List<CTAxDataSource> ctAxDataSources = new ArrayList<>();
        for (CTPieSer ser : getCtChart(chart).getSerList()) {
            ctAxDataSources.add(ser.getCat());
        }
        return ctAxDataSources;
    }

    @Override
    protected List<CTNumDataSource> getValDS(XSLFChart chart) {
        List<CTNumDataSource> ctNumDataSources = new ArrayList<>();
        for (CTPieSer ser : getCtChart(chart).getSerList()) {
            ctNumDataSources.add(ser.getVal());
        }
        return ctNumDataSources;
    }

    @Override
    public void applyCustomizeOptions(XSLFChart chart, ChartOptions options) {
        if (options instanceof PieChartOptions) {
            PieChartOptions pieChartOptions = (PieChartOptions) options;
            CTPieChart ctPieChart = getCtChart(chart);
        }
    }

    @Override
    protected void customizeChart(XSLFChart chart, XDDFChartAxis categoryAxis, XDDFValueAxis valueAxis) {
        CTPieChart ctPieChart = getCtChart(chart);
    }

    @Override
    protected void clearSeries(XSLFChart chart) {
        CTPieChart ctPieChart = getCtChart(chart);
        for (int i = ctPieChart.sizeOfSerArray() - 1; i >= 0; i--) {
            ctPieChart.removeSer(i);
        }
    }

    @Override
    protected void removeSer(XSLFChart chart, int serIdx) {
        getCtChart(chart).removeSer(serIdx);
    }

    @Override
    protected ChartTypes getChartType() {
        return ChartTypes.PIE;
    }

    private CTPieChart getCtChart(XSLFChart chart) {
        return chart.getCTChart().getPlotArea().getPieChartArray(0);
    }
}
