package com.taocares.commons.poi.xssf.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 表格列头，树状结构
 *
 * @author Ankang
 * @date 2018/11/20
 */
public class TableHeader implements RecursiveData<TableHeader> {

    /**
     * 列名
     */
    private String name;

    /**
     * 占多少行的空间
     */
    private int rowSpan;

    /**
     * 下级列头
     */
    private List<TableHeader> children;

    /**
     * 创建列头，默认占一行
     *
     * @param name 列名
     */
    public TableHeader(String name) {
        this(name, 1);
    }

    /**
     * 创建列头，并指定所占的行数
     *
     * @param name    列名
     * @param rowSpan 所占行数
     */
    public TableHeader(String name, int rowSpan) {
        this.name = name;
        this.rowSpan = rowSpan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(int rowSpan) {
        this.rowSpan = rowSpan;
    }

    @Override
    public List<TableHeader> getChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return children;
    }

    public void setChildren(List<TableHeader> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "TableHeader{" +
                "name='" + name + '\'' +
                ", rowSpan=" + rowSpan +
                ", children=" + children +
                '}';
    }

    /**
     * 获取所占的列数，通过计算子节点得到
     *
     * @return 所占列数
     */
    public int getColSpan() {
        if (getChildren().isEmpty()) {
            return 1;
        } else {
            int span = 0;
            for (TableHeader child : getChildren()) {
                span += child.getColSpan();
            }
            return span;
        }
    }
}
