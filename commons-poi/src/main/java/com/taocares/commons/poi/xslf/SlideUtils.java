package com.taocares.commons.poi.xslf;

import com.taocares.commons.poi.Constants;
import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.Utils;
import com.taocares.commons.poi.XmlUtils;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTChart;
import org.openxmlformats.schemas.drawingml.x2006.diagram.CTDataModel;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObjectData;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrameNonVisual;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlide;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;
import java.util.stream.Collectors;

import static com.taocares.commons.poi.Constants.CT_DIAGRAM_DATA;

/**
 * 幻灯片工具
 *
 * @author Ankang
 * @date 2018/11/16
 */
public class SlideUtils {

    /**
     * 替换幻灯片模板中的文字
     *
     * @param slide     幻灯片
     * @param variables 变量表
     */
    public static void replaceVariables(XSLFSlide slide, Map<String, String> variables) {
        CTSlide ctSlide = slide.getXmlObject();
        XmlUtils.replaceVariables(ctSlide, variables);
        for (POIXMLDocumentPart part : slide.getRelations()) {
            if (part instanceof XSLFChart) {
                CTChart ctChart = ((XSLFChart) part).getCTChart();
                XmlUtils.replaceVariables(ctChart, variables);
            } else {
                PackagePart packagePart = part.getPackagePart();
                if (CT_DIAGRAM_DATA.equals(packagePart.getContentType())) {
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(packagePart.getInputStream()));
                         OutputStream out = packagePart.getOutputStream()) {
                        String result = br.lines().collect(Collectors.joining("\n"));
                        CTDataModel ctDataModel = CTDataModel.Factory.parse(result);
                        XmlUtils.replaceVariables(ctDataModel, variables);
                        ctDataModel.save(out);
                    } catch (Exception e) {
                        // skip if failed
                    }
                } else {
                    // TODO 开发使用，需要移除
                    System.err.println(part.getClass() + " | " + part.toString());
                }
            }
        }
    }

    /**
     * 从幻灯片中定位到第一个图表
     *
     * @param slide 幻灯片
     * @return 图表
     * @throws POIException 找不到图表
     */
    public static XSLFChart findChart(XSLFSlide slide) {
        return findChart(slide, 0);
    }

    /**
     * 从幻灯片中定位到指定的图表
     *
     * @param slide 幻灯片
     * @param index 图表的索引位置（从0开始）
     * @return 图表
     * @throws POIException 找不到指定索引位置的图表
     */
    public static XSLFChart findChart(XSLFSlide slide, int index) {
        String error = "找不到索引为" + index + "的图表！";
        for (POIXMLDocumentPart part : slide.getRelations()) {
            if (part instanceof XSLFChart) {
                if (index > 0) {
                    index--;
                } else {
                    return (XSLFChart) part;
                }
            }
        }
        throw new POIException(error);
    }

    /**
     * 将图表添加到幻灯片中，默认为单个表格居中（注意：已经存在的图表不要重复添加）<br/>
     * 如果需要指定位置，请使用{@link #addChart(XSLFSlide, XSLFChart, Rectangle2D)}
     *
     * @param slide 幻灯片
     * @param chart 图表
     */
    public static void addChart(XSLFSlide slide, XSLFChart chart) {
        addChart(slide, chart, 1, 1, 1, 1, null);
    }

    /**
     * 将图表添加到幻灯片中，默认为单个表格居中（注意：已经存在的图表不要重复添加）<br/>
     * 可以指定预留空间<br/>
     * 如果需要指定位置，请使用{@link #addChart(XSLFSlide, XSLFChart, Rectangle2D)}
     *
     * @param slide  幻灯片
     * @param chart  图表
     * @param insets 上下左右预留的空间
     */
    public static void addChart(XSLFSlide slide, XSLFChart chart, Insets insets) {
        addChart(slide, chart, 1, 1, 1, 1, insets);
    }

    /**
     * 将图表添加到幻灯片中，栅格布局（注意：已经存在的图表不要重复添加）<br/>
     *
     * @param slide 幻灯片
     * @param chart 图表
     * @param row   所在的行（从1开始）
     * @param col   所在的列（从1开始）
     * @param rows  总行数
     * @param cols  总列数
     */
    public static void addChart(XSLFSlide slide, XSLFChart chart, int row, int col, int rows, int cols) {
        addChart(slide, chart, row, col, rows, cols, null);
    }

    /**
     * 将图表添加到幻灯片中，栅格布局（注意：已经存在的图表不要重复添加）<br/>
     * 可以指定预留空间<br/>
     *
     * @param slide  幻灯片
     * @param chart  图表
     * @param row    所在的行（从1开始）
     * @param col    所在的列（从1开始）
     * @param rows   总行数
     * @param cols   总列数
     * @param insets 上下左右预留的空间
     */
    public static void addChart(XSLFSlide slide, XSLFChart chart, int row, int col, int rows, int cols, Insets insets) {
        Dimension pageSize = slide.getSlideShow().getPageSize();
        insets = insets == null ? new Insets(0, 0, 0, 0) : insets;
        double totalWidth = pageSize.width - insets.left - insets.right;
        double totalHeight = pageSize.height - insets.top - insets.bottom;
        double ratio = 0.8;
        double w = totalWidth * ratio / cols;
        double h = totalHeight * ratio / rows;
        double x = w * (col - 1) + totalWidth * (1 - ratio) * col / (cols + 1) + insets.left;
        double y = h * (row - 1) + totalHeight * (1 - ratio) * row / (rows + 1) + insets.top;
        addChart(slide, chart, new Rectangle2D.Double(x, y, w, h));
    }

    /**
     * 将图表添加到幻灯片中，并指定位置和大小（注意：已经存在的图表不要重复添加）
     *
     * @param slide     幻灯片
     * @param chart     图表
     * @param rectangle 位置信息
     */
    public static void addChart(XSLFSlide slide, XSLFChart chart, Rectangle2D rectangle) {
        long cNvPrId = 1;
        int cNvPrNameCount = 1;
        for (CTGraphicalObjectFrame currGraphicalObjectFrame : slide.getXmlObject().getCSld().getSpTree().getGraphicFrameList()) {
            if (currGraphicalObjectFrame.getNvGraphicFramePr() != null) {
                if (currGraphicalObjectFrame.getNvGraphicFramePr().getCNvPr() != null) {
                    cNvPrId++;
                    if (currGraphicalObjectFrame.getNvGraphicFramePr().getCNvPr().getName().startsWith(Constants.CHART_NAME)) {
                        cNvPrNameCount++;
                    }
                }
            }
        }
        CTGraphicalObjectFrame ctGraphicalObjectFrame = slide.getXmlObject().getCSld().getSpTree().addNewGraphicFrame();
        CTGraphicalObjectFrameNonVisual ctGraphicalObjectFrameNonVisual = ctGraphicalObjectFrame.addNewNvGraphicFramePr();
        CTNonVisualDrawingProps ctNonVisualDrawingProps = ctGraphicalObjectFrameNonVisual.addNewCNvPr();
        ctNonVisualDrawingProps.setId(cNvPrId);
        ctNonVisualDrawingProps.setName(Constants.CHART_NAME + cNvPrNameCount);
        ctGraphicalObjectFrameNonVisual.addNewCNvGraphicFramePr();
        ctGraphicalObjectFrameNonVisual.addNewNvPr();

        CTGraphicalObjectData graphicalObjectData = null;
        try {
            graphicalObjectData = CTGraphicalObjectData.Factory.parse(
                    "<c:chart xmlns:c=\"http://schemas.openxmlformats.org/drawingml/2006/chart\" "
                            + "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" "
                            + "r:id=\"" + slide.getRelationId(chart) + "\"/>"
            );
            graphicalObjectData.setUri("http://schemas.openxmlformats.org/drawingml/2006/chart");
        } catch (XmlException e) {
            throw new POIException("无法解析内容！", e);
        }
        CTGraphicalObject ctGraphicalObject = ctGraphicalObjectFrame.addNewGraphic();
        ctGraphicalObject.setGraphicData(graphicalObjectData);

        Utils.setAnchor(ctGraphicalObjectFrame, rectangle);
    }
}
