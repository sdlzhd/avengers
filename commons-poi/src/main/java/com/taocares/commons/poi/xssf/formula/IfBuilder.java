package com.taocares.commons.poi.xssf.formula;

import com.taocares.commons.poi.xssf.FormulaHelper;

import static com.taocares.commons.poi.Constants.DELIMITER;

/**
 * IF函数构建工具
 *
 * @author Ankang
 * @date 2018/11/22
 */
public class IfBuilder {
    private Criteria criteria;
    private Object valueIfTrue;
    private Object valueIfFalse;

    /**
     * 设置判断条件
     *
     * @param criteria 判断条件对象
     */
    public IfBuilder criteria(Criteria criteria) {
        this.criteria = criteria;
        return this;
    }

    /**
     * 设置条件为真的结果
     *
     * @param valueIfTrue 条件为真的结果
     */
    public IfBuilder valueIfTrue(Object valueIfTrue) {
        this.valueIfTrue = valueIfTrue;
        return this;
    }

    /**
     * 设置条件为假的结果
     *
     * @param valueIfFalse 条件为假的结果
     */
    public IfBuilder valueIfFalse(Object valueIfFalse) {
        this.valueIfFalse = valueIfFalse;
        return this;
    }

    /**
     * 生成IF公式
     *
     * @return 公式
     */
    public String build() {
        return "IF(" + criteria.toString() + DELIMITER +
                FormulaHelper.getFormulaString(valueIfTrue) + DELIMITER +
                FormulaHelper.getFormulaString(valueIfFalse) + ")";
    }
}
