package com.taocares.commons.poi.xssf;

import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.xssf.model.ColumnData;
import com.taocares.commons.poi.xssf.model.TableHeader;
import com.taocares.commons.util.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.*;

import java.util.*;

/**
 * 表格操作工具
 *
 * @author Ankang
 * @date 2018/11/12
 */
public class SheetUtils {

    /**
     * @param sheet    表格
     * @param root     列头根节点
     * @param showRoot 是否显示根节点
     */
    public static void setHeader(XSSFSheet sheet, TableHeader root, boolean showRoot) {
        setHeader(sheet, root, 0, 0, showRoot);
    }

    public static void setHeader(XSSFSheet sheet, TableHeader root, int rowIdx, int colIdx, boolean showRoot) {
        if (showRoot) {
            getCell(sheet, rowIdx, colIdx).setCellValue(root.getName());
            int rowSpan = root.getRowSpan();
            int colSpan = root.getColSpan();
            if (rowSpan > 1 || colSpan > 1) {
                sheet.addMergedRegion(new CellRangeAddress(rowIdx, rowIdx + rowSpan - 1, colIdx, colIdx + colSpan - 1));
            }
            rowIdx += rowSpan;
        }
        setHeader(sheet, root.getChildren(), rowIdx, colIdx);
    }

    private static void setHeader(XSSFSheet sheet, List<TableHeader> headers, final int startRow, final int startCol) {
        int colIdx = startCol;
        for (TableHeader header : headers) {
            getCell(sheet, startRow, colIdx).setCellValue(header.getName());
            setHeader(sheet, header.getChildren(), startRow + header.getRowSpan(), colIdx);
            colIdx += header.getColSpan();
        }
        colIdx = startCol;
        for (TableHeader header : headers) {
            int rowSpan = header.getRowSpan();
            int colSpan = header.getColSpan();
            if (rowSpan > 1 || colSpan > 1) {
                sheet.addMergedRegion(new CellRangeAddress(startRow, startRow + rowSpan - 1, colIdx, colIdx + colSpan - 1));
            }
            colIdx += colSpan;
        }
    }

    /**
     * 向表格中插入数据（从第一行第一列开始）
     *
     * @param sheet 表格
     * @param data  数据
     */
    public static void setData(XSSFSheet sheet, List<Object[]> data) {
        setData(sheet, data, 0, 0);
    }

    /**
     * 向表格中指定位置插入数据
     *
     * @param sheet    表格
     * @param data     数据
     * @param startRow 首行索引（从0开始）
     * @param startCol 首列索引（从0开始）
     */
    public static void setData(XSSFSheet sheet, List<Object[]> data, int startRow, int startCol) {
        Map<Integer, XSSFCellStyle> styleMap = new HashMap<>();
        int rowIdx = startRow;
        for (Object[] objects : data) {
            int colIdx = startCol;
            for (Object object : objects) {
                XSSFCell cell = getCell(sheet, rowIdx, colIdx);
                if (object instanceof ColumnData) {
                    ColumnData columnData = (ColumnData) object;
                    setCellValue(cell, columnData.getData());
                    if (StringUtils.isNotEmpty(columnData.getFormat())) {
                        XSSFCellStyle cellStyle = styleMap.get(colIdx);
                        if (cellStyle == null) {
                            cellStyle = sheet.getWorkbook().createCellStyle();
                            XSSFDataFormat dataFormat = sheet.getWorkbook().createDataFormat();
                            cellStyle.setDataFormat(dataFormat.getFormat(columnData.getFormat()));
                            styleMap.put(colIdx, cellStyle);
                        }
                        cell.setCellStyle(cellStyle);
                    }
                } else {
                    setCellValue(cell, object);
                }
                colIdx++;
            }
            rowIdx++;
        }
    }

    /**
     * 对表格中指定的列的相同数据进行合并单元格操作
     *
     * @param sheet  数据表格
     * @param colIdx 列索引（从0开始）
     */
    public static void groupData(XSSFSheet sheet, final int colIdx) {
        groupData(sheet, colIdx, -1);
    }

    /**
     * 对表格中指定的列的相同数据进行合并单元格操作<br/>
     * 如果参考列的值发生变化，即使相同数据也不会合并
     *
     * @param sheet  数据表格
     * @param colIdx 列索引（从0开始）
     * @param refCol 参考列索引（负数表示无参考）
     */
    public static void groupData(XSSFSheet sheet, final int colIdx, final int refCol) {
        // 需要先计算所有公式的结果
        XSSFFormulaEvaluator.evaluateAllFormulaCells(sheet.getWorkbook());
        Object lastValue = null;
        Object lastRefValue = null;
        int beginRowIdx = sheet.getFirstRowNum();
        int endRowIdx = sheet.getFirstRowNum();
        for (int rowIdx = sheet.getFirstRowNum(); rowIdx <= sheet.getLastRowNum(); rowIdx++) {
            Object value = getCellValue(getCell(sheet, rowIdx, colIdx));
            Object refValue = refCol < 0 ? null : getCellValue(getCell(sheet, rowIdx, refCol));
            if (Objects.equals(value, lastValue) && Objects.equals(refValue, lastRefValue)) {
                endRowIdx++;
            } else {
                if (endRowIdx > beginRowIdx) {
                    sheet.addMergedRegion(new CellRangeAddress(beginRowIdx, endRowIdx, colIdx, colIdx));
                }
                beginRowIdx = rowIdx;
                endRowIdx = rowIdx;
            }
            lastValue = value;
            lastRefValue = refValue;
        }
        if (endRowIdx > beginRowIdx) {
            sheet.addMergedRegion(new CellRangeAddress(beginRowIdx, endRowIdx, colIdx, colIdx));
        }
    }

    /**
     * 清空表格中的内容
     *
     * @param sheet 表格
     */
    public static void clearSheet(XSSFSheet sheet) {
        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }

    /**
     * 获取字符串形式的单元格引用，如果单元格不存在，会自动创建<br/>
     * 如果表格不为null，返回的是绝对引用；否则返回相对引用
     *
     * @param sheet  表格
     * @param rowIdx 行索引（从0开始）
     * @param colIdx 列索引（从0开始）
     * @return 字符串形式的单元格引用
     */
    public static String getCellReferenceString(XSSFSheet sheet, int rowIdx, int colIdx) {
        return getCellReference(sheet, rowIdx, colIdx).formatAsString();
    }

    /**
     * 获取单元格引用，如果单元格不存在，会自动创建<br/>
     * 如果表格不为null，返回的是绝对引用；否则返回相对引用
     *
     * @param sheet  表格
     * @param rowIdx 行索引（从0开始）
     * @param colIdx 列索引（从0开始）
     * @return 单元格引用
     */
    public static CellReference getCellReference(XSSFSheet sheet, int rowIdx, int colIdx) {
        if (sheet != null) {
            ensureCellExists(sheet, rowIdx, colIdx);
            return new CellReference(sheet.getSheetName(), rowIdx, colIdx, true, true);
        } else {
            return new CellReference(rowIdx, colIdx);
        }
    }

    /**
     * 获取单元格，如果不存在则自动创建
     *
     * @param sheet  表格
     * @param rowIdx 行索引（从0开始）
     * @param colIdx 列索引（从0开始）
     * @return 单元格
     */
    public static XSSFCell getCell(XSSFSheet sheet, int rowIdx, int colIdx) {
        ensureCellExists(sheet, rowIdx, colIdx);
        return sheet.getRow(rowIdx).getCell(colIdx);
    }

    /**
     * 获取单元格的值，或者公式的运算结果（需要确保在此之前执行过公式计算）
     *
     * @param cell 单元格
     * @return 值
     */
    public static Object getCellValue(XSSFCell cell) {
        CellType cellType = cell.getCellType() == CellType.FORMULA ?
                cell.getCachedFormulaResultType() : cell.getCellType();
        switch (cellType) {
            case NUMERIC:
                return cell.getNumericCellValue();
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case ERROR:
                return cell.getErrorCellString();
            case BLANK:
            default:
                return null;
        }
    }

    /**
     * 设置单元格的值
     *
     * @param cell  单元格
     * @param value 值
     */
    public static void setCellValue(XSSFCell cell, Object value) {
        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((boolean) value);
        } else if (value instanceof Number) {
            cell.setCellValue(((Number) value).doubleValue());
        } else {
            throw new POIException("不支持的数据！" + value);
        }
    }

    private static void ensureCellExists(XSSFSheet sheet, int rowIdx, int colIdx) {
        XSSFRow row = sheet.getRow(rowIdx);
        if (row == null) {
            row = sheet.createRow(rowIdx);
        }
        XSSFCell cell = row.getCell(colIdx);
        if (cell == null) {
            cell = row.createCell(colIdx);
        }
    }
}
