package com.taocares.commons.poi.xssf.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 列的数据封装
 *
 * @author Ankang
 * @date 2018/12/4
 */
@Data
@AllArgsConstructor
public class ColumnData {
    /**
     * 数据
     */
    private Object data;
    /**
     * 格式，一般用于日期等，可为空
     */
    private String format;
}
