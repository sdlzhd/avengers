package com.taocares.commons.poi.xslf.chart.option;

import lombok.Builder;
import lombok.Data;
import org.openxmlformats.schemas.drawingml.x2006.chart.STGrouping;

/**
 * 折线图参数
 *
 * @author Ankang
 * @date 2018/11/14
 */
@Data
@Builder
public class LineChartOptions implements ChartOptions {
    /**
     * 聚合方式
     */
    @Builder.Default
    private STGrouping.Enum grouping = STGrouping.STANDARD;
    /**
     * 是否显示标记
     */
    @Builder.Default
    private boolean marker = true;
    /**
     * 连接线是否平滑
     */
    @Builder.Default
    private boolean smooth = true;
}
