package com.taocares.commons.poi.xslf.chart.option;

import lombok.Builder;
import lombok.Data;
import org.apache.poi.xddf.usermodel.chart.LegendPosition;

/**
 * 通用的参数
 *
 * @author Ankang
 * @date 2018/11/15
 */
@Data
@Builder
public class CommonChartOptions implements ChartOptions {

    /**
     * 图例的位置，默认为下方，可为null（不显示图例）
     */
    @Builder.Default
    private LegendPosition legendPosition = LegendPosition.BOTTOM;

    /**
     * 数字格式，{@link com.taocares.commons.poi.NumberFormat}提供了常用的格式<br/>
     * 也可以参照office标准自定义格式
     */
    private String numberFormat;
}
