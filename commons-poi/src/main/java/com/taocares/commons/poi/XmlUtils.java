package com.taocares.commons.poi;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTTitle;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTTx;
import org.openxmlformats.schemas.drawingml.x2006.diagram.CTDataModel;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharacterProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlide;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Open Office XML工具
 *
 * @author Ankang
 * @date 2018/11/20
 */
public class XmlUtils {

    /**
     * 替换幻灯片模板中的变量
     *
     * @param ctSlide   幻灯片
     * @param variables 变量表
     * @throws POIException 替换的内容无法解析
     */
    public static void replaceVariables(CTSlide ctSlide, Map<String, String> variables) {
        joinTextRuns(ctSlide);
        String xml = replaceVariables(ctSlide.toString(), variables);
        try {
            ctSlide.set(CTSlide.Factory.parse(xml));
        } catch (XmlException e) {
            throw new POIException("无法解析内容！", e);
        }
    }

    /**
     * 替换图表模板中的变量
     *
     * @param ctChart   图表
     * @param variables 变量表
     * @throws POIException 替换的内容无法解析
     */
    public static void replaceVariables(CTChart ctChart, Map<String, String> variables) {
        joinTextRuns(ctChart);
        String xml = replaceVariables(ctChart.toString(), variables);
        try {
            ctChart.set(CTChart.Factory.parse(xml));
        } catch (XmlException e) {
            throw new POIException("无法解析内容！", e);
        }
    }

    /**
     * 替换SmartArt模板中的变量
     *
     * @param ctDataModel SmartArt数据
     * @param variables   变量表
     * @throws POIException 替换的内容无法解析
     */
    public static void replaceVariables(CTDataModel ctDataModel, Map<String, String> variables) {
//        joinTextRuns(ctDataModel);
        String xml = replaceVariables(ctDataModel.toString(), variables);
        try {
            ctDataModel.set(CTDataModel.Factory.parse(xml));
        } catch (XmlException e) {
            throw new POIException("无法解析内容！", e);
        }
    }

    /**
     * 替换模板中的文字
     *
     * @param template  模板
     * @param variables 变量表
     * @return 替换后的结果
     */
    public static String replaceVariables(String template, Map<String, String> variables) {
        for (Map.Entry<String, String> entry : variables.entrySet()) {
            template = template.replaceAll(String.format("\\$\\{%s}", entry.getKey()), entry.getValue());
        }
        return template;
    }

    /**
     * 将幻灯片中格式相同的文本进行合并
     *
     * @param ctSlide 幻灯片
     */
    private static void joinTextRuns(CTSlide ctSlide) {
        List<CTGroupShape> allGroupShapes = new ArrayList<>();
        getAllGroupShapes(ctSlide.getCSld().getSpTree(), allGroupShapes);
        for (CTGroupShape gsp : allGroupShapes) {
            for (CTShape sp : gsp.getSpList()) {
                for (CTTextParagraph p : sp.getTxBody().getPList()) {
                    joinTextRuns(p);
                }
            }
        }
    }

    /**
     * 遍历获取所有GroupShape
     *
     * @param root   根节点
     * @param result 结果集合
     */
    private static void getAllGroupShapes(CTGroupShape root, List<CTGroupShape> result) {
        result.add(root);
        for (CTGroupShape gsp : root.getGrpSpList()) {
            getAllGroupShapes(gsp, result);
        }
    }

    /**
     * 将图表中格式相同的文本进行合并
     *
     * @param ctChart 图表
     */
    private static void joinTextRuns(CTChart ctChart) {
        if (ctChart.isSetTitle()) {
            CTTitle ctTitle = ctChart.getTitle();
            if (ctTitle.isSetTx()) {
                CTTx ctTx = ctTitle.getTx();
                if (ctTx.isSetRich()) {
                    CTTextBody ctTextBody = ctTx.getRich();
                    for (CTTextParagraph p : ctTextBody.getPList()) {
                        joinTextRuns(p);
                    }

                }
            }
        }
    }

    /**
     * 将段落中格式相同的文本进行合并
     *
     * @param ctTextParagraph 段落
     */
    private static void joinTextRuns(CTTextParagraph ctTextParagraph) {
        CTRegularTextRun pr = null;
        for (CTRegularTextRun r : ctTextParagraph.getRList()) {
            if (pr != null) {
                if (equals(r.getRPr(), pr.getRPr())) {
                    r.setT(pr.getT() + r.getT());
                    pr.setT("");
                }
            }
            pr = r;
        }
    }

    /**
     * 判断两个文本格式是否相同
     *
     * @param p1 文本属性
     * @param p2 文本属性
     * @return true-相同 false-不同
     */
    public static boolean equals(CTTextCharacterProperties p1, CTTextCharacterProperties p2) {
        if (p1 == p2) {
            return true;
        } else if (p1 == null || p2 == null) {
            return false;
        } else {
            return Objects.equals(p1.getLang(), p2.getLang()) // 语言
                    && equals(p1.getLatin(), p2.getLatin()) // 西文字体
                    && equals(p1.getEa(), p2.getEa()) // 中文字体
                    && p1.getB() == p2.getB() // 字体样式-加粗
                    && p1.getI() == p2.getI() // 字体样式-斜体
                    && p1.getSz() == p2.getSz() // 大小
                    && equals(p1.getSolidFill(), p2.getSolidFill()) // 字体颜色
                    && p1.getU() == p2.getU() // 下划线线型
                    && equals(p1.getUFill(), p2.getUFill()) // 下划线颜色
                    && p1.getStrike() == p2.getStrike() // 删除线
                    && p1.getBaseline() == p2.getBaseline() // 上标/下标
                    && p1.getNormalizeH() == p2.getNormalizeH() // 等高字符
                    && p1.getSpc() == p2.getSpc() // 字符间距
                    && p1.getCap() == p2.getCap() // 大小写
                    && equals(p1.getNoFill(), p2.getNoFill()) // 无填充
                    && equals(p1.getGradFill(), p2.getGradFill()) // 渐变填充
                    && equals(p1.getBlipFill(), p2.getBlipFill()) // 纹理填充
                    && equals(p1.getPattFill(), p2.getPattFill()) // 图案填充
                    && equals(p1.getLn(), p2.getLn()) // 文本轮廓
                    && equals(p1.getEffectLst(), p2.getEffectLst()) // 文字效果
                    && equals(p1.getCs(), p2.getCs()) // 字体？
                    && equals(p1.getSym(), p2.getSym()) // 字体？
                    && equals(p1.getHighlight(), p2.getHighlight()) // 高亮？
                    ;
        }
    }

    /**
     * 判断两个XmlObject是否相同
     *
     * @param o1 XmlObject
     * @param o2 XmlObject
     * @return true-相同 false-不同
     */
    public static boolean equals(XmlObject o1, XmlObject o2) {
        if (o1 == o2) {
            return true;
        } else if (o1 == null || o2 == null) {
            return false;
        } else {
            return Objects.equals(o1.toString(), o2.toString());
        }
    }
}
