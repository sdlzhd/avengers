package com.taocares.commons.poi.demo;

import com.taocares.commons.poi.NumberFormat;
import com.taocares.commons.poi.xslf.ChartUtils;
import com.taocares.commons.poi.xslf.SlideUtils;
import com.taocares.commons.poi.xslf.chart.option.*;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.openxmlformats.schemas.drawingml.x2006.chart.STBarDir;
import org.openxmlformats.schemas.drawingml.x2006.chart.STBarGrouping;
import org.openxmlformats.schemas.drawingml.x2006.chart.STGrouping;
import org.openxmlformats.schemas.drawingml.x2006.chart.STRadarStyle;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.List;

/**
 * 图表创建Demo
 *
 * @author Ankang
 * @date 2018/11/5
 */
public class ChartCreateDemo {


    public static void main(String[] args) {
        XMLSlideShow pptx = new XMLSlideShow();
        // 绘制普通表格
        drawChart(pptx, pptx.createSlide());
        // 绘制复合表格
        drawCompositeChart(pptx, pptx.createSlide());

        try (OutputStream out = new FileOutputStream("chart-create-demo-output.pptx")) {
            pptx.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建新图表的步骤：<br/>
     * <ol>
     *     <li>依次创建SlideShow、Slide、Chart对象</li>
     *     <li>调用ChartUtils.drawChart()方法更新Chart对象</li>
     *     <li>调用SlideUtils.addChart()方法将图表添加到幻灯片指定位置</li>
     *     <li>将SlideShow输出到输出流</li>
     * </ol>
     */
    private static void drawChart(XMLSlideShow pptx, XSLFSlide slide) {
        int rows = 1;
        int cols = 2;
        for (int r = 1; r <= rows; r++) {
            for (int c = 1; c <= cols; c++) {
                XSLFChart chart = pptx.createChart(slide);
                drawChart(chart, getRandomChartType());
                SlideUtils.addChart(slide, chart, r, c, rows, cols, new Insets(100, 0, 0, 0));
            }
        }
    }

    private static void drawCompositeChart(XMLSlideShow pptx, XSLFSlide slide) {
        XSLFChart chart = pptx.createChart(slide);
        SlideUtils.addChart(slide, chart);

        List<String> categories = Arrays.asList("类别 1", "类别 2", "类别 3", "类别 4");
        Map<String, List<Integer>> values = new LinkedHashMap<>();
        values.put("系列 1", Arrays.asList(2, 6, 9, 13));
        values.put("系列 2", Arrays.asList(8, 3, 8, 4));

        List<ChartTypes> chartTypes = Arrays.asList(ChartTypes.BAR, ChartTypes.LINE);
        ChartUtils.drawCompositeChart(chart, chartTypes, categories, values);
    }

    /**
     * 本方法是演示所用，实际使用时只需要调用
     * {@link ChartUtils#drawChart(XSLFChart, ChartTypes, List, Map)}（默认参数）
     * 或者{@link ChartUtils#drawChart(XSLFChart, ChartTypes, List, Map, ChartOptions)}（指定参数）
     * 方法即可
     *
     * @param chart     图表
     * @param chartType 图表类型
     */
    private static void drawChart(XSLFChart chart, ChartTypes chartType) {

        List<String> categories = Arrays.asList("类别 1", "类别 2", "类别 3", "类别 4");
        Map<String, List<Integer>> values = new LinkedHashMap<>();
        values.put("系列 1", Arrays.asList(2, 6, 9, 13));
        values.put("系列 2", Arrays.asList(6, 6, 6, 6));
        values.put("系列 3", Arrays.asList(8, 0, 8, 0));
        values.put("系列 4", Arrays.asList(1, 1, 1, 1));
        values.put("系列 5", Arrays.asList(1, 8, 3, 1));
        switch (chartType) {
            case BAR:
                BarChartOptions barChartOptions = BarChartOptions.builder()
                        .direction(STBarDir.BAR)
                        .grouping(STBarGrouping.CLUSTERED)
                        .overlap((byte) 0)
                        .build();
                ChartUtils.drawChart(chart, ChartTypes.BAR, categories, values);
                ChartUtils.applyOptions(chart, barChartOptions);
                break;
            case LINE:
                LineChartOptions lineChartOptions = LineChartOptions.builder()
                        .grouping(STGrouping.STANDARD)
                        .marker(true)
                        .smooth(false)
                        .build();
                ChartUtils.drawChart(chart, ChartTypes.LINE, categories, values, lineChartOptions);
                break;
            case PIE:
                ChartUtils.drawChart(chart, ChartTypes.PIE, categories, values);
                break;
            case RADAR:
                RadarChartOptions radarChartOptions = RadarChartOptions.builder()
                        .style(STRadarStyle.MARKER)
                        .build();
                ChartUtils.drawChart(chart, ChartTypes.RADAR, categories, values, radarChartOptions);
                break;
            case SCATTER:
                ScatterChartOptions scatterChartOptions = ScatterChartOptions.builder()
                        .line(false)
                        .marker(false)
                        .smooth(false)
                        .build();
                List<Double> x = Arrays.asList(-0.2, 0.0, 0.3, 0.4, 0.88);
                Map<String, List<Double>> y = new LinkedHashMap<>();
                y.put("满意度", Arrays.asList(0.85, 0.83, 0.9, 0.88, 0.6));
                y.put("投诉率", Arrays.asList(0.6, 0.45, 0.7, 0.9, 0.7));
                CommonChartOptions commonChartOptions = CommonChartOptions.builder()
                        .numberFormat(NumberFormat.PERCENT).build();
                ChartUtils.drawChart(chart, ChartTypes.SCATTER, x, y, commonChartOptions);
                ChartUtils.applyOptions(chart, scatterChartOptions);
                break;
        }
    }

    private static ChartTypes getRandomChartType() {
        int i = new Random().nextInt(ChartTypes.values().length);
        return ChartTypes.values()[i];
    }
}
