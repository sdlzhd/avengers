package com.taocares.commons.poi.xssf;

import java.util.List;

/**
 * Excel数据加工工厂
 *
 * @author Ankang
 * @date 2018/11/23
 */
public interface ValueFactory {

    /**
     * 将普通数据转换为Excel使用的数据
     *
     * @param data POJO对象列表
     * @return Object[]列表
     */
    List<Object[]> call(List<?> data);
}
