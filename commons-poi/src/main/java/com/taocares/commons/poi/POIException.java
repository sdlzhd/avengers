package com.taocares.commons.poi;

/**
 * POI相关的异常
 *
 * @author Ankang
 * @date 2018/11/20
 */
public class POIException extends RuntimeException {
    public POIException() {
        super();
    }

    public POIException(String message) {
        super(message);
    }

    public POIException(String message, Throwable cause) {
        super(message, cause);
    }

    public POIException(Throwable cause) {
        super(cause);
    }
}
