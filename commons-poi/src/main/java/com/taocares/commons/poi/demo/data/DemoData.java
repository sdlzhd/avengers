package com.taocares.commons.poi.demo.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * 测试数据
 *
 * @author Ankang
 * @date 2018/11/23
 */
@Data
@AllArgsConstructor
public class DemoData {
    private Long id;
    private String name;
    private int age;
    private int score1;
    private int score2;
    private int score3;
    private Date birthday;
}
