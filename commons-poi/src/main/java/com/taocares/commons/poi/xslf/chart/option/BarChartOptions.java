package com.taocares.commons.poi.xslf.chart.option;

import lombok.Builder;
import lombok.Data;
import org.openxmlformats.schemas.drawingml.x2006.chart.STBarDir;
import org.openxmlformats.schemas.drawingml.x2006.chart.STBarGrouping;

/**
 * 柱形图参数
 *
 * @author Ankang
 * @date 2018/11/14
 */
@Data
@Builder
public class BarChartOptions implements ChartOptions {
    /**
     * 方向（条形图或柱状图）
     */
    @Builder.Default
    private STBarDir.Enum direction = STBarDir.COL;
    /**
     * 聚合方式
     */
    @Builder.Default
    private STBarGrouping.Enum grouping = STBarGrouping.STANDARD;
    /**
     * 垂直方向重叠比例，范围-100~100，负数表示远离，正数表示靠近<br/>
     * 堆积图一般来说应该为100（完全重合）
     */
    @Builder.Default
    private byte overlap = 0;
}
