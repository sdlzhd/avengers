package com.taocares.commons.poi.xslf.chart;

import com.taocares.commons.poi.xslf.chart.option.BarChartOptions;
import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.XDDFChartAxis;
import org.apache.poi.xddf.usermodel.chart.XDDFValueAxis;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 柱形图绘制类
 *
 * @author Ankang
 * @date 2018/11/9
 */
public class BarChartDrawer extends AbstractChartDrawer {

    @Override
    protected CTSerTx getSerTx(XSLFChart chart, int serIdx) {
        CTBarSer ser = getCtChart(chart).getSerArray(serIdx);
        CTSerTx ctSerTx = ser.getTx();
        if (ctSerTx == null) {
            ctSerTx = ser.addNewTx();
        }
        return ctSerTx;
    }

    @Override
    public long updateSerIdx(XSLFChart chart, long fromIdx) {
        for (CTBarSer ser : getCtChart(chart).getSerList()) {
            ser.getIdx().setVal(fromIdx);
            ser.getOrder().setVal(fromIdx);
            fromIdx++;
        }
        return fromIdx;
    }

    @Override
    protected List<CTAxDataSource> getCatDS(XSLFChart chart) {
        List<CTAxDataSource> ctAxDataSources = new ArrayList<>();
        for (CTBarSer ser : getCtChart(chart).getSerList()) {
            ctAxDataSources.add(ser.getCat());
        }
        return ctAxDataSources;
    }

    @Override
    protected List<CTNumDataSource> getValDS(XSLFChart chart) {
        List<CTNumDataSource> ctNumDataSources = new ArrayList<>();
        for (CTBarSer ser : getCtChart(chart).getSerList()) {
            ctNumDataSources.add(ser.getVal());
        }
        return ctNumDataSources;
    }

    @Override
    public void applyCustomizeOptions(XSLFChart chart, ChartOptions options) {
        if (options instanceof BarChartOptions) {
            BarChartOptions barChartOptions = (BarChartOptions) options;
            CTBarChart ctBarChart = getCtChart(chart);

            CTBarDir ctBarDir = ctBarChart.getBarDir();
            if (ctBarDir == null) {
                ctBarDir = ctBarChart.addNewBarDir();
            }
            ctBarDir.setVal(barChartOptions.getDirection());

            CTBarGrouping ctBarGrouping = ctBarChart.isSetGrouping() ?
                    ctBarChart.getGrouping() : ctBarChart.addNewGrouping();
            ctBarGrouping.setVal(barChartOptions.getGrouping());

            CTOverlap ctOverlap = ctBarChart.isSetOverlap() ?
                    ctBarChart.getOverlap() : ctBarChart.addNewOverlap();
            ctOverlap.setVal(barChartOptions.getOverlap());
        }
    }

    @Override
    protected void customizeChart(XSLFChart chart, XDDFChartAxis categoryAxis, XDDFValueAxis valueAxis) {
        CTBarChart ctBarChart = getCtChart(chart);
        ctBarChart.addNewAxId().setVal(categoryAxis.getId());
        ctBarChart.addNewAxId().setVal(valueAxis.getId());
        ctBarChart.addNewBarDir().setVal(STBarDir.COL);
    }

    @Override
    protected void clearSeries(XSLFChart chart) {
        CTBarChart ctBarChart = getCtChart(chart);
        for (int i = ctBarChart.sizeOfSerArray() - 1; i >= 0; i--) {
            ctBarChart.removeSer(i);
        }
    }

    @Override
    protected void removeSer(XSLFChart chart, int serIdx) {
        getCtChart(chart).removeSer(serIdx);
    }

    @Override
    protected ChartTypes getChartType() {
        return ChartTypes.BAR;
    }

    private CTBarChart getCtChart(XSLFChart chart) {
        return chart.getCTChart().getPlotArea().getBarChartArray(0);
    }
}
