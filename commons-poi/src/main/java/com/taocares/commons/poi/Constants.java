package com.taocares.commons.poi;

/**
 * 常量类
 *
 * @author Ankang
 * @date 2018/11/14
 */
public class Constants {
    // SmartArt
    public static final String CT_DIAGRAM_DATA = "application/vnd.openxmlformats-officedocument.drawingml.diagramData+xml";

    public static final String CHART_NAME = "图表";
    public static final String TABLE_NAME = "表";

    public static final String DELIMITER = ", ";
}
