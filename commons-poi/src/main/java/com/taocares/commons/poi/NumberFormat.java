package com.taocares.commons.poi;

/**
 * 常用的数字格式
 *
 * @author Ankang
 * @date 2018/11/19
 */
public class NumberFormat {

    public static final String INTEGER = "#,##0";

    public static final String DECIMAL = "#,##0.00";

    public static final String PERCENT = "#,##0%";
}
