package com.taocares.commons.poi.demo;

import com.taocares.commons.poi.NumberFormat;
import com.taocares.commons.poi.xslf.ChartUtils;
import com.taocares.commons.poi.xslf.SlideUtils;
import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import com.taocares.commons.poi.xslf.chart.option.CommonChartOptions;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * 修改模板中图表的示例
 *
 * @author Ankang
 * @date 2018/11/16
 */
public class ChartModifyDemo {

    /**
     * 修改模板中的图表的步骤：<br/>
     * <ol>
     * <li>依次创建SlideShow、Slide、Chart对象，其中输入流是模板文件</li>
     * <li>调用SlideUtils.findChart()方法找到幻灯片中的Chart对象</li>
     * <li>调用ChartUtils.updateChart()方法更新Chart对象</li>
     * <li>将SlideShow输出到输出流</li>
     * </ol>
     *
     * @param args
     */
    public static void main(String[] args) {
        try (InputStream in = ChartModifyDemo.class.getResourceAsStream("/chart-modify-demo-input.pptx");
             OutputStream out = new FileOutputStream("chart-modify-demo-output.pptx")) {
            XMLSlideShow pptx = new XMLSlideShow(in);
            XSLFSlide slide = pptx.getSlides().get(0);
            XSLFChart chart = SlideUtils.findChart(slide);
            updateChart(chart);
            chart = SlideUtils.findChart(slide, 1);
            updateChart(chart);
            updateSlideText(pptx.getSlides().get(1));
            pptx.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 演示文本占位符的替换
     *
     * @param slide
     */
    private static void updateSlideText(XSLFSlide slide) {
        Map<String, String> map = new HashMap<>();
        map.put("year", "2018");
        map.put("month-of-year", "11");
        map.put("month", "11");
        map.put("total", "123");
        map.put("percent", "90%");
        SlideUtils.replaceVariables(slide, map);
    }

    /**
     * 本方法是演示所用，实际使用时只需要调用
     * {@link ChartUtils#updateChart(XSLFChart, List, Map)}（原始参数）
     * 或者{@link ChartUtils#updateChart(XSLFChart, List, Map, ChartOptions)}（指定参数）
     * 即可
     *
     * @param chart 图表
     */
    private static void updateChart(XSLFChart chart) {
        CommonChartOptions commonChartOptions = CommonChartOptions.builder()
                .numberFormat(NumberFormat.PERCENT)
                .build();
        if (ChartUtils.getChartTypes(chart).contains(ChartTypes.SCATTER)) {
            List<Double> x = Arrays.asList(-0.2, 0.0, 0.3, 0.4, 0.88);
            Map<String, List<Double>> y = new LinkedHashMap<>();
            y.put("满意度", Arrays.asList(0.85, 0.83, 0.9, 0.88, 0.6));
            y.put("投诉率", Arrays.asList(0.6, 0.45, 0.7, 0.9, 0.7));
            ChartUtils.updateChart(chart, x, y);
            ChartUtils.applyOptions(chart, commonChartOptions);
            ChartUtils.setChartTitle(chart, "修改的散点图");
        } else {
            List<String> categories = Arrays.asList("类别 1", "类别 2", "类别 3", "类别 4");
            Map<String, List<Integer>> values = new LinkedHashMap<>();
            values.put("系列 1", Arrays.asList(2, 6, 9, 13));
            values.put("系列 2", Arrays.asList(6, 6, 6, 6));
            values.put("系列 3", Arrays.asList(8, 0, 8, 0));
            values.put("系列 4", Arrays.asList(1, 1, 1, 1));
            values.put("系列 5", Arrays.asList(1, 8, 3, 1));
            ChartUtils.updateChart(chart, categories, values);
//            ChartUtils.applyOptions(chart, commonChartOptions);
        }
    }
}
