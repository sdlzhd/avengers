package com.taocares.commons.poi.xssf.formula;

import com.taocares.commons.poi.xssf.FormulaHelper;
import org.apache.poi.ss.util.CellReference;

import static com.taocares.commons.poi.Constants.DELIMITER;

/**
 * 比较条件
 *
 * @author Ankang
 * @date 2018/11/22
 */
public class Criteria {

    /**
     * 左值（单元格引用）
     */
    private CellReference left;
    /**
     * 右值（值或引用）
     */
    private Object right;
    /**
     * 操作符
     */
    private Operator operator;

    /**
     * 创建查询条件
     *
     * @param operator 操作符
     * @param left     左值
     * @param right    右值
     */
    public static Criteria of(Operator operator, CellReference left, Object right) {
        Criteria criteria = new Criteria();
        criteria.operator = operator;
        criteria.left = left;
        criteria.right = right;
        return criteria;
    }

    /**
     * 获取和另一个条件的“与”
     * @param that 另一个条件
     * @return 两个条件的“与”
     */
    public String and(Criteria that) {
        return "AND(" + this.toString() + DELIMITER + that.toString() + ")";
    }

    /**
     * 获取和另一个条件的“或”
     * @param that 另一个条件
     * @return 两个条件的“或”
     */
    public String or(Criteria that) {
        return "OR(" + this.toString() + DELIMITER + that.toString() + ")";
    }

    /**
     * 获取真实的表达式
     * @return 表达式
     */
    public String toString() {
        String leftValue = left.formatAsString();
        String rightValue = FormulaHelper.getFormulaString(right);
        return leftValue + operator.getOp() + rightValue;
    }

    public enum Operator {
        GT(">"), GE(">="), LT("<"), LE("<="), EQ("=");

        private final String op;

        Operator(String op) {
            this.op = op;
        }

        public String getOp() {
            return op;
        }
    }
}