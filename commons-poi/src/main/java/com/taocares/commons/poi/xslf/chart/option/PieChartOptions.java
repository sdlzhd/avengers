package com.taocares.commons.poi.xslf.chart.option;

import lombok.Builder;
import lombok.Data;

/**
 * 饼形图参数
 *
 * @author Ankang
 * @date 2018/11/14
 */
@Data
@Builder
public class PieChartOptions implements ChartOptions {
}
