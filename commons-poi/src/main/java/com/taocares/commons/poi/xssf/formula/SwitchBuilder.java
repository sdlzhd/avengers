package com.taocares.commons.poi.xssf.formula;

import com.taocares.commons.poi.POIException;
import com.taocares.commons.poi.xssf.FormulaHelper;
import org.apache.poi.ss.util.CellReference;

import java.util.ArrayList;
import java.util.List;

import static com.taocares.commons.poi.Constants.DELIMITER;

/**
 * 多重IF函数构建工具
 *
 * @author Ankang
 * @date 2018/11/23
 */
public class SwitchBuilder {
    private CellReference cellReference;
    private List<Condition> conditions = new ArrayList<>();
    private Object defaultValue;

    /**
     * 单元格引用
     *
     * @param cellReference 单元格引用
     */
    public SwitchBuilder cellReference(CellReference cellReference) {
        this.cellReference = cellReference;
        return this;
    }

    /**
     * 设置公式的条件
     *
     * @param operator    操作符
     * @param testValue   用于判断的单元格引用或具体的值
     * @param valueIfTrue 如果满足条件应该返回的结果
     * @throws POIException 如果尚未设置单元格引用
     */
    public SwitchBuilder condition(Criteria.Operator operator, Object testValue, Object valueIfTrue) {
        if (cellReference == null) {
            throw new POIException("需要先设置引用的单元格！");
        }
        Criteria criteria = Criteria.of(operator, cellReference, testValue);
        Condition condition = new Condition(criteria, valueIfTrue);
        this.conditions.add(condition);
        return this;
    }

    /**
     * 设置公式的默认值
     *
     * @param defaultValue 默认值
     */
    public SwitchBuilder defaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    /**
     * 生成嵌套的IF公式
     *
     * @return 公式
     * @throws POIException 如果默认值为空
     */
    public String build() {
        if (defaultValue == null) {
            throw new POIException("默认值不能为空！");
        }
        StringBuilder sb = new StringBuilder();
        for (Condition condition : conditions) {
            sb.append(condition.toString());
        }
        sb.append(FormulaHelper.getFormulaString(defaultValue));
        for (int i = 0; i < conditions.size(); i++) {
            sb.append(")");
        }
        return sb.toString();
    }

    class Condition {
        private Criteria criteria;
        private Object valueIfTrue;

        Condition(Criteria criteria, Object valueIfTrue) {
            this.criteria = criteria;
            this.valueIfTrue = valueIfTrue;
        }

        public String toString() {
            return "IF(" + criteria.toString() + DELIMITER +
                    FormulaHelper.getFormulaString(valueIfTrue) + DELIMITER;
        }
    }
}
