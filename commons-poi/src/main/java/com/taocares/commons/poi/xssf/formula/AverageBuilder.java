package com.taocares.commons.poi.xssf.formula;

import com.taocares.commons.poi.POIException;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;

import java.util.ArrayList;
import java.util.List;

import static com.taocares.commons.poi.Constants.DELIMITER;

/**
 * AVERAGE函数构建工具
 *
 * @author Ankang
 * @date 2018/11/22
 */
public class AverageBuilder {
    private List<CellRangeAddress> cellRangeAddresses = new ArrayList<>();

    private List<CellReference> cellReferences = new ArrayList<>();

    private List<Number> numbers = new ArrayList<>();

    /**
     * 增加求平均数的数据区域
     *
     * @param cellRangeAddress 求平均数的数据区域
     */
    public AverageBuilder range(CellRangeAddress cellRangeAddress) {
        this.cellRangeAddresses.add(cellRangeAddress);
        return this;
    }

    /**
     * 增加求平均数的单元格
     *
     * @param cellReference 单元格引用
     */
    public AverageBuilder cell(CellReference cellReference) {
        this.cellReferences.add(cellReference);
        return this;
    }

    /**
     * 求平均数的参数增加某个特定数值
     *
     * @param number 数值
     * @param <T>    数值类型
     */
    public <T extends Number> AverageBuilder number(T number) {
        this.numbers.add(number);
        return this;
    }

    /**
     * 生成AVERAGE公式
     *
     * @return 公式
     * @throws POIException 如果未设置任何参数
     */
    public String build() {
        StringBuilder sb = new StringBuilder("AVERAGE(");
        for (CellRangeAddress cellRangeAddress : cellRangeAddresses) {
            sb.append(cellRangeAddress.formatAsString());
            sb.append(DELIMITER);
        }
        for (CellReference cellReference : cellReferences) {
            sb.append(cellReference.formatAsString());
            sb.append(DELIMITER);
        }
        for (Number number : numbers) {
            sb.append(number.doubleValue());
            sb.append(DELIMITER);
        }
        int index = sb.lastIndexOf(DELIMITER);
        if (index > 0) {
            return sb.substring(0, index) + ")";
        } else {
            throw new POIException("公式中没有元素！");
        }
    }
}
