package com.taocares.commons.poi.xslf.chart;

import com.taocares.commons.poi.xslf.chart.option.ChartOptions;
import com.taocares.commons.poi.xslf.chart.option.LineChartOptions;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.XDDFChartAxis;
import org.apache.poi.xddf.usermodel.chart.XDDFValueAxis;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 折线图绘制类
 *
 * @author Ankang
 * @date 2018/11/9
 */
public class LineChartDrawer extends AbstractChartDrawer {

    @Override
    protected CTSerTx getSerTx(XSLFChart chart, int serIdx) {
        CTLineSer ser = getCtChart(chart).getSerArray(serIdx);
        CTSerTx ctSerTx = ser.getTx();
        if (ctSerTx == null) {
            ctSerTx = ser.addNewTx();
        }
        return ctSerTx;
    }

    @Override
    public long updateSerIdx(XSLFChart chart, long fromIdx) {
        for (CTLineSer ser : getCtChart(chart).getSerList()) {
            ser.getIdx().setVal(fromIdx);
            ser.getOrder().setVal(fromIdx);
            fromIdx++;
        }
        return fromIdx;
    }

    @Override
    protected List<CTAxDataSource> getCatDS(XSLFChart chart) {
        List<CTAxDataSource> ctAxDataSources = new ArrayList<>();
        for (CTLineSer ser : getCtChart(chart).getSerList()) {
            ctAxDataSources.add(ser.getCat());
        }
        return ctAxDataSources;
    }

    @Override
    protected List<CTNumDataSource> getValDS(XSLFChart chart) {
        List<CTNumDataSource> ctNumDataSources = new ArrayList<>();
        for (CTLineSer ser : getCtChart(chart).getSerList()) {
            ctNumDataSources.add(ser.getVal());
        }
        return ctNumDataSources;
    }

    @Override
    public void applyCustomizeOptions(XSLFChart chart, ChartOptions options) {
        if (options instanceof LineChartOptions) {
            LineChartOptions lineChartOptions = (LineChartOptions) options;
            CTLineChart ctLineChart = getCtChart(chart);

            CTGrouping ctGrouping = ctLineChart.getGrouping();
            if (ctGrouping == null) {
                ctGrouping = ctLineChart.addNewGrouping();
            }
            ctGrouping.setVal(lineChartOptions.getGrouping());

            for (int i = 0; i < ctLineChart.sizeOfSerArray(); i++) {
                CTLineSer ctLineSer = ctLineChart.getSerArray(i);

                CTMarker ctMarker = ctLineSer.isSetMarker() ?
                        ctLineSer.getMarker() : ctLineSer.addNewMarker();
                CTMarkerStyle ctMarkerStyle = ctMarker.isSetSymbol() ?
                        ctMarker.getSymbol() : ctMarker.addNewSymbol();
                if (lineChartOptions.isMarker()) {
                    int index = (i + 6) % 11 + 1; // start from 7
                    ctMarkerStyle.setVal(STMarkerStyle.Enum.forInt(index));
                } else {
                    ctMarkerStyle.setVal(STMarkerStyle.NONE);
                }
                CTBoolean smooth = ctLineSer.isSetSmooth() ?
                        ctLineSer.getSmooth() : ctLineSer.addNewSmooth();
                smooth.setVal(lineChartOptions.isSmooth());
            }
        }
    }

    @Override
    protected void customizeChart(XSLFChart chart, XDDFChartAxis categoryAxis, XDDFValueAxis valueAxis) {
        CTLineChart ctLineChart = getCtChart(chart);
        ctLineChart.addNewAxId().setVal(categoryAxis.getId());
        ctLineChart.addNewAxId().setVal(valueAxis.getId());
    }

    @Override
    protected void clearSeries(XSLFChart chart) {
        CTLineChart ctLineChart = getCtChart(chart);
        for (int i = ctLineChart.sizeOfSerArray() - 1; i >= 0; i--) {
            ctLineChart.removeSer(i);
        }
    }

    @Override
    protected void removeSer(XSLFChart chart, int serIdx) {
        getCtChart(chart).removeSer(serIdx);
    }

    @Override
    protected ChartTypes getChartType() {
        return ChartTypes.LINE;
    }

    private CTLineChart getCtChart(XSLFChart chart) {
        return chart.getCTChart().getPlotArea().getLineChartArray(0);
    }
}
