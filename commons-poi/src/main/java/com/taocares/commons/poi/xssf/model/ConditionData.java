package com.taocares.commons.poi.xssf.model;

import com.taocares.commons.poi.xssf.formula.Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * IF/多重IF判断需要的数据
 *
 * @author Ankang
 * @date 2018/11/23
 */
@Data
@Builder
@AllArgsConstructor
public class ConditionData {
    private Criteria.Operator operator;
    private Object testValue;
    private Object valueIfTrue;
}
