package com.taocares.commons.lang;

/**
 * 类型转换异常
 *
 * @author Ankang
 * @date 2018/9/20
 */
public class TypeConvertException extends RuntimeException {

    public TypeConvertException(String message) {
        super(message);
    }

    public TypeConvertException(Throwable cause) {
        super("类型转换异常", cause);
    }

    public TypeConvertException(String message, Throwable cause) {
        super(message, cause);
    }
}
