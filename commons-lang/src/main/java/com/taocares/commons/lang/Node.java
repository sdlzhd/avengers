package com.taocares.commons.lang;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 节点接口，欲转换的对象需实现该接口
 * @author: yunpeng liu
 * @create: 2019-01-18 13:56
 **/
public interface Node<ID> {
    /**
     * 获取父节点的ID
     * @return 父节点ID
     */
    ID getParent();

    /**
     * 获取该节点的ID
     * @return 该节点ID
     */
    ID getId();

    /**
     * 设置该节点的子节点列表
     * @param children 该节点的子节点列表
     */
    void setChildren(List<Node> children);

    List<Node> getChildren();

    default void addChildren(Node node) {
        List<Node> children = this.getChildren();
        if (children == null) {
            children = new ArrayList<>();
            this.setChildren(children);
        }
        children.add(node);
    }
}
