package com.taocares.commons.lang;

import java.lang.annotation.*;

/**
 * 表示方法参数、返回值、属性可以为空，仅供commons项目使用
 *
 * @author Ankang
 * @date 2018/9/14
 */
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Nullable {
}
