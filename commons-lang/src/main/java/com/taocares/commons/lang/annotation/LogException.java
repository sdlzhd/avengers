package com.taocares.commons.lang.annotation;

import java.lang.annotation.*;

/**
 * 需要记录异常日志的注解
 *
 * @author Ankang
 * @date 2019/1/14
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogException {
}
