package com.taocares.commons.lang;

/**
 * 方法传入了不支持类型的异常
 *
 * @author Ankang
 * @date 2018/9/17
 */
public class UnsupportedTypeException extends RuntimeException {

    public UnsupportedTypeException(String message) {
        super(message);
    }

    public UnsupportedTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
