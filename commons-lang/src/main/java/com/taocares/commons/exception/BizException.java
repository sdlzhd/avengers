package com.taocares.commons.exception;

/**
 * 业务类异常基类
 *
 * @author Ankang
 * @date 2018/10/23
 */
public class BizException extends RuntimeException {

    public BizException(String message) {
        super(message);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(Throwable cause) {
        super(cause);
    }
}
