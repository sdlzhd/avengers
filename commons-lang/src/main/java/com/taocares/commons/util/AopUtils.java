package com.taocares.commons.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.CodeSignature;

import java.lang.reflect.Method;

/**
 * AOP相关工具类，暂时放在commons-lang模块，后续根据需要可能调整
 *
 * @author Ankang
 * @date 2019/1/14
 */
public class AopUtils {

    /**
     * 从切入点获取方法
     *
     * @param joinPoint 切入点
     * @return 方法
     */
    public static Method findMethod(JoinPoint joinPoint) {
        Class<?> declaringType = joinPoint.getSignature().getDeclaringType();
        String name = joinPoint.getSignature().getName();
        Class[] params = ((CodeSignature) joinPoint.getSignature()).getParameterTypes();
        Method method = null;

        try {
            method = declaringType.getMethod(name, params);
        } catch (NoSuchMethodException ignored) {
        }

        if (method == null) {
            try {
                method = declaringType.getDeclaredMethod(name, params);
            } catch (NoSuchMethodException ignored) {
            }
        }

        return method;
    }
}
