package com.taocares.commons.util;

import javax.validation.constraints.NotNull;

import static com.taocares.commons.text.Mask.*;

/**
 * 数据脱敏工具
 *
 * @author Ankang
 * @date 2018/11/23
 */
public class MaskUtils {

    /**
     * 根据指定的格式进行数据加工，原始数据中超出Mask的部分会原封不动追加到结尾，<br/>
     * Mask支持的字符参见{@link com.taocares.commons.text.Mask}<br/>
     * 建议使用{@link com.taocares.commons.text.Mask.Builder}生成<br/>
     * 注意：Mask中可以使用单引号指定额外的格式化字符，<br/>例如：
     * 原始：1234567890，Mask：AAA'-'AAA'-'AA，结果：123-456-7890<br/>
     * 原始：test.me@taocares.com，Mask：AAA<@，结果：tes****@taocares.com<br/>
     *
     * @param source 原始数据
     * @param mask   格式定义
     * @return 格式化的结果
     */
    public static String mask(@NotNull String source, String mask) {
        return mask(source, mask, MASK_KEY, false);
    }

    /**
     * 根据指定的格式进行数据加工，可以指定超出Mask的部分是否截断处理<br/>
     * Mask支持的字符参见{@link com.taocares.commons.text.Mask}<br/>
     * Mask可以使用{@link com.taocares.commons.text.Mask.Builder}生成<br/>
     * 注意：Mask中可以使用单引号指定额外的格式化字符，<br/>例如：
     * 原始：1234567890，Mask：AAA'-'AAA'-'AA，truncate：true，结果：123-456-78<br/>
     * 原始：test.me@taocares.com，Mask：AAA<@，结果：tes****@taocares.com<br/>
     *
     * @param source   原始数据
     * @param mask     格式定义
     * @param maskChar 用于掩盖内容的字符
     * @param truncate 是否根据Mask长度进行截断（如果Mask中有截止符，此参数不生效）
     * @return 格式化的结果
     */
    public static String mask(@NotNull String source, String mask, char maskChar, boolean truncate) {
        StringBuilder masked = new StringBuilder();
        int index = 0;
        boolean literal = false;
        for (int i = 0; i < mask.length() && index < source.length(); i++) {
            char c = mask.charAt(i);
            if (c == LITERAL_KEY) {
                literal = !literal;
                continue;
            }
            if (literal) {
                masked.append(c);
            } else if (c == BEFORE_KEY) {
                // 如果使用截止符，不能截断内容
                truncate = false;
                char cc = mask.charAt(i + 1);
                while (index < source.length()
                        && source.charAt(index) != cc) {
                    masked.append(maskChar);
                    index++;
                }
            } else if (c == MASK_KEY) {
                masked.append(maskChar);
                index++;
            } else if (c == UPPER_KEY) {
                masked.append(Character.toUpperCase(source.charAt(index)));
                index++;
            } else if (c == LOWER_KEY) {
                masked.append(Character.toLowerCase(source.charAt(index)));
                index++;
            } else {
                masked.append(source.charAt(index));
                index++;
            }
        }
        if (!truncate) {
            for (int i = index; i < source.length(); i++) {
                masked.append(source.charAt(i));
            }
        }
        return masked.toString();
    }
}
