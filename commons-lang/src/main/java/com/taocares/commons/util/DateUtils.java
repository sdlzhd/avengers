package com.taocares.commons.util;

import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * 日期转换工具
 *
 * @author Ankang
 * @date 2018/9/21
 */
public class DateUtils {

    /**
     * 将时间对象转换为特定格式
     *
     * @param source  时间对象
     * @param pattern 格式
     * @return 格式化的字符串
     */
    public static String format(@NotNull Date source, @NotNull String pattern) {
        DateFormat format = new SimpleDateFormat(pattern);
        return format.format(source);
    }

    /**
     * 格式化LocalDate
     *
     * @param source  LocalDate
     * @param pattern 格式
     * @return 格式化的字符串
     */
    public static String format(@NotNull LocalDate source, @NotNull String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return source.format(formatter);
    }

    /**
     * 格式化LocalTime
     *
     * @param source  LocalTime
     * @param pattern 格式
     * @return 格式化的字符串
     */
    public static String format(@NotNull LocalTime source, @NotNull String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return source.format(formatter);
    }

    /**
     * 格式化LocalDateTime
     *
     * @param source  LocalDateTime
     * @param pattern 格式
     * @return 格式化的字符串
     */
    public static String format(@NotNull LocalDateTime source, @NotNull String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return source.format(formatter);
    }

    /**
     * 根据特定格式解析时间
     *
     * @param source  时间字符串
     * @param pattern 格式
     * @return 时间对象
     * @throws DateTimeException 时间解析异常
     */
    public static Date parseDate(@NotNull String source, @NotNull String pattern) {
        DateFormat format = new SimpleDateFormat(pattern);
        try {
            return format.parse(source);
        } catch (ParseException e) {
            throw new DateTimeException("时间解析异常", e);
        }
    }

    /**
     * 字符串解析为LocalDate
     *
     * @param source  时间字符串
     * @param pattern 格式
     * @return LocalDate
     * @throws DateTimeException 时间解析异常
     */
    public static LocalDate parseLocalDate(@NotNull String source, @NotNull String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(source, formatter);
    }

    /**
     * 字符串解析为LocalTime
     *
     * @param source  时间字符串
     * @param pattern 格式
     * @return LocalTime
     * @throws DateTimeException 时间解析异常
     */
    public static LocalTime parseLocalTime(@NotNull String source, @NotNull String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalTime.parse(source, formatter);
    }

    /**
     * 字符串解析为LocalDateTime
     *
     * @param source  时间字符串
     * @param pattern 格式
     * @return LocalDate
     * @throws DateTimeException 时间解析异常
     */
    public static LocalDateTime parseLocalDateTime(@NotNull String source, @NotNull String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(source, formatter);
    }

    /* 以上是时间格式化工具 */
    /* 以下是时间操作的工具 */

    /**
     * LocalDate转Instant
     *
     * @param source LocalDate
     * @return Instant
     */
    public static Instant toInstant(@NotNull LocalDate source) {
        return source.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }

    /**
     * LocalTime转Instant
     *
     * @param source LocalTime
     * @return Instant
     */
    public static Instant toInstant(@NotNull LocalTime source) {
        return source.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant();
    }

    /**
     * LocalDateTime转Instant
     *
     * @param source LocalDateTime
     * @return Instant
     */
    public static Instant toInstant(@NotNull LocalDateTime source) {
        return source.atZone(ZoneId.systemDefault()).toInstant();
    }

    /**
     * Date转LocalDate
     *
     * @param source Date
     * @return LocalDate
     * @deprecated 建议使用 {@link #toLocalDateTime(Date)}配合{@link LocalDateTime#toLocalDate()}完成转换
     */
    @Deprecated
    public static LocalDate toLocalDate(@NotNull Date source) {
        return toLocalDateTime(source).toLocalDate();
    }

    /**
     * Date转LocalTime
     *
     * @param source Date
     * @return LocalTime
     * @deprecated 建议使用 {@link #toLocalDateTime(Date)}配合{@link LocalDateTime#toLocalTime()}完成转换
     */
    @Deprecated
    public static LocalTime toLocalTime(@NotNull Date source) {
        return toLocalDateTime(source).toLocalTime();
    }

    /**
     * Date转LocalDateTime
     *
     * @param source Date
     * @return LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(@NotNull Date source) {
        return LocalDateTime.ofInstant(source.toInstant(), ZoneId.systemDefault());
    }

    /**
     * LocalDate转Date
     *
     * @param source LocalDate
     * @return Date
     * @deprecated 建议使用 {@link #toInstant(LocalDate)}配合{@link Date#from(Instant)}完成转换
     */
    @Deprecated
    public static Date toDate(@NotNull LocalDate source) {
        return Date.from(toInstant(source));
    }

    /**
     * LocalTime转Date
     *
     * @param source LocalTime
     * @return Date
     * @deprecated 建议使用 {@link #toInstant(LocalTime)}配合{@link Date#from(Instant)}完成转换
     */
    @Deprecated
    public static Date toDate(@NotNull LocalTime source) {
        return Date.from(toInstant(source));
    }

    /**
     * LocalDateTime转Date
     *
     * @param source LocalDateTime
     * @return Date
     * @deprecated 建议使用 {@link #toInstant(LocalDateTime)}配合{@link Date#from(Instant)}完成转换
     */
    @Deprecated
    public static Date toDate(@NotNull LocalDateTime source) {
        return Date.from(toInstant(source));
    }

    /**
     * 从时间戳获取LocalDate
     *
     * @param millis 时间戳
     * @return LocalDate
     * @deprecated 建议使用 {@link #toLocalDateTime(long)}配合{@link LocalDateTime#toLocalDate()}完成转换
     */
    @Deprecated
    public static LocalDate toLocalDate(long millis) {
        return toLocalDateTime(millis).toLocalDate();
    }

    /**
     * 从时间戳获取LocalTime
     *
     * @param millis 时间戳
     * @return LocalTime
     * @deprecated 建议使用 {@link #toLocalDateTime(long)}配合{@link LocalDateTime#toLocalTime()} ()}完成转换
     */
    @Deprecated
    public static LocalTime toLocalTime(long millis) {
        return toLocalDateTime(millis).toLocalTime();
    }

    /**
     * 从时间戳获取LocalDateTime
     *
     * @param millis 时间戳
     * @return LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault());
    }


    /* 以下是时间取整（上界、下界）的工具 */

    /**
     * 获取特定时间的结束，示例：<br/>
     * <ul>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#MONTHS}，输出2019-02-28 23:59:59</li>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#DAYS}，输出2019-02-12 23:59:59</li>
     * </ul>
     *
     * @param source 时间
     * @param unit   时间单位
     * @return 时间的开始
     */
    public static Date endOf(Date source, ChronoUnit unit) {
        return Date.from(endOf(source.toInstant(), unit).toInstant());
    }

    /**
     * 获取特定时间的结束，示例：<br/>
     * <ul>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#MONTHS}，输出2019-02-28 23:59:59</li>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#DAYS}，输出2019-02-12 23:59:59</li>
     * </ul>
     *
     * @param source 时间
     * @param unit   时间单位
     * @return 时间的结束
     */
    public static LocalDateTime endOf(LocalDateTime source, ChronoUnit unit) {
        return endOf(source.atZone(ZoneId.systemDefault()).toInstant(), unit).toLocalDateTime();
    }

    /**
     * 获取特定时间的结束，示例：<br/>
     * <ul>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#MONTHS}，输出2019-02-28 23:59:59</li>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#DAYS}，输出2019-02-12 23:59:59</li>
     * </ul>
     *
     * @param instant 时间点
     * @param unit    取整单位
     * @return 时间的结束
     */
    public static ZonedDateTime endOf(Instant instant, ChronoUnit unit) {
        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.systemDefault());
        switch (unit) {
            case YEARS:
                return ZonedDateTime.of(
                        LocalDate.of(zonedDateTime.getYear(), 12, 31),
                        LocalTime.MAX,
                        ZoneId.systemDefault());
            case MONTHS:
                return ZonedDateTime.of(
                        zonedDateTime.toLocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1),
                        LocalTime.MAX,
                        ZoneId.systemDefault());
            case DAYS:
            case HOURS:
            case MINUTES:
            case SECONDS:
                return zonedDateTime.truncatedTo(unit).plus(unit.getDuration()).minusNanos(1);
            default:
                throw new IllegalArgumentException("ChronoUnit: " + unit + " not supported!");
        }
    }

    /**
     * 获取特定时间的开始，示例：<br/>
     * <ul>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#MONTHS}，输出2019-02-01 00:00:00</li>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#DAYS}，输出2019-02-12 00:00:00</li>
     * </ul>
     *
     * @param source 时间
     * @param unit   时间单位
     * @return 时间的开始
     */
    public static Date startOf(Date source, ChronoUnit unit) {
        return Date.from(startOf(source.toInstant(), unit).toInstant());
    }

    /**
     * 获取特定时间的开始，示例：<br/>
     * <ul>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#MONTHS}，输出2019-02-01 00:00:00</li>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#DAYS}，输出2019-02-12 00:00:00</li>
     * </ul>
     *
     * @param source 时间
     * @param unit   时间单位
     * @return 时间的开始
     */
    public static LocalDateTime startOf(LocalDateTime source, ChronoUnit unit) {
        return startOf(source.atZone(ZoneId.systemDefault()).toInstant(), unit).toLocalDateTime();
    }

    /**
     * 获取特定时间的开始，示例：<br/>
     * <ul>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#MONTHS}，输出2019-02-01 00:00:00</li>
     * <li>输入2019-02-12 13:50:30, unit={@link ChronoUnit#DAYS}，输出2019-02-12 00:00:00</li>
     * </ul>
     *
     * @param instant 时间点
     * @param unit    取整单位
     * @return 时间的开始
     */
    public static ZonedDateTime startOf(Instant instant, ChronoUnit unit) {
        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.systemDefault());
        switch (unit) {
            case YEARS:
                return ZonedDateTime.of(
                        LocalDate.of(zonedDateTime.getYear(), 1, 1),
                        LocalTime.MIN,
                        ZoneId.systemDefault());
            case MONTHS:
                return ZonedDateTime.of(
                        LocalDate.of(zonedDateTime.getYear(), zonedDateTime.getMonthValue(), 1),
                        LocalTime.MIN,
                        ZoneId.systemDefault());
            case DAYS:
            case HOURS:
            case MINUTES:
            case SECONDS:
                return zonedDateTime.truncatedTo(unit);
            default:
                throw new IllegalArgumentException("ChronoUnit: " + unit + " not supported!");
        }
    }
}
