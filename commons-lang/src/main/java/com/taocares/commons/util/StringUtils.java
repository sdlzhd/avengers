package com.taocares.commons.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 字符串工具类
 *
 * @author Ankang
 * @date 2018/9/14
 */
public class StringUtils {

    /**
     * 判断字符串内容是否为空（或者全部是空白字符）
     *
     * @param str 字符串
     * @return true-空 false-非空
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * 判断字符串内容是否非空
     *
     * @param str 字符串
     * @return true-非空 false-空
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 将异常堆栈信息转换为字符串
     *
     * @param e 异常
     * @return 异常堆栈字符串
     */
    public static String formatStackTrace(Throwable e) {
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw)) {
            // 将出错的栈信息输出到printWriter中
            e.printStackTrace(pw);
            return sw.toString();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    /**
     * 获取两个子串之间的字符串
     *
     * @param str   源字符串
     * @param open  左子串
     * @param close 右子串
     * @return 左右子串之间的字符串
     */
    public static String substringBetween(final String str, final String open, final String close) {
        if (str == null || open == null || close == null) {
            return null;
        }
        final int start = str.indexOf(open);
        if (start != -1) {
            final int end = str.indexOf(close, start + open.length());
            if (end != -1) {
                return str.substring(start + open.length(), end);
            }
        }
        return null;
    }
}
