package com.taocares.commons.util;

import com.taocares.commons.lang.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 反射工具类
 *
 * @author Ankang
 * @date 2018/9/14
 */
public class ReflectionUtils {

    private static final Logger logger = LoggerFactory.getLogger(ReflectionUtils.class);

    /**
     * 获取字段的值
     * @param field 字段，不能为空
     * @param object 所属对象
     * @return 字段的值，可能为空
     */
    @Nullable
    public static Object getFieldValue(@NotNull Field field, Object object) {
        try {
            field.setAccessible(true);
            return field.get(object);
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    /**
     * 设置字段的值
     * @param field 字段，不能为空
     * @param object 所属对象
     * @param value 设置的值
     */
    public static void setFieldValue(@NotNull Field field, Object object, Object value) {
        try {
            field.setAccessible(true);
            field.set(object, value);
        } catch (IllegalAccessException e) {
            // 无法访问直接忽略
        }
    }

    /**
     * 从本类或者父类中获取特定字段
     * @param paramClass 类
     * @param fieldName 字段名
     * @return 字段
     * @throws NoSuchFieldException 未找到字段
     */
    public static Field getField(@NotNull Class<?> paramClass, String fieldName) throws NoSuchFieldException {
        // 记录错误需要
        Class<?> cls = paramClass;
        do {
            try {
                return cls.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                // 忽略，继续获取父类字段
            }
            cls = cls.getSuperclass();
        } while (cls != null);
        throw new NoSuchFieldException("Can't find field: " + fieldName + " in class: " + paramClass);
    }

    /**
     * 获取类的所有字段（包括继承的字段）
     * @param paramClass 类
     * @return 所有字段
     */
    public static List<Field> getAllFields(@NotNull Class<?> paramClass) {
        Class<?> cls = paramClass;
        List<Field> fields = new ArrayList<>();
        do {
            fields.addAll(Arrays.asList(cls.getDeclaredFields()));
            cls = cls.getSuperclass();
        } while (cls != null);
        return fields;
    }

    /**
     * 根据指定的类获取新的实例，遇到实例化错误，可能返回NULL
     * @param cls 类
     * @param <T> 类的类型
     * @return 类的实例
     */
    @Nullable
    public static <T> T newInstance(@NotNull Class<T> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("实例化对象失败", e);
            return null;
        }
    }
}
