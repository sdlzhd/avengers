package com.taocares.commons.util;

import java.util.Random;

/**
 * 代码来源于apache-common项目
 * http://commons.apache.org/
 */
public class RandomStringUtils {

    private static final Random RANDOM = new Random();

    public RandomStringUtils() {
        super();
    }

    /**
     * <p>从整个字符集中选择字符，创建指定长度的随机字符串</p>
     *
     * <p>大多数情况下，产生的是乱码</p>
     *
     * @param count 随机字符串的长度
     * @return 随机字符串
     */
    public static String random(final int count) {
        return random(count, false, false);
    }

    /**
     * <p>从ASCII编码为32到126的编码中，创建指定长度的随机字符串</p>
     *
     * @param count 随机字符串的长度
     * @return 随机字符串
     */
    public static String randomAscii(final int count) {
        return random(count, 32, 127, false, false);
    }


    public static String randomAscii(final int minLengthInclusive, final int maxLengthExclusive) {
        return randomAscii(nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * <p>创建指定长度的随机字符串</p>
     *
     * <p>字符将包括（a-z，A-Z）</p>
     *
     * @param count 随机字符串的长度
     * @return 随机字符串
     */
    public static String randomAlphabetic(final int count) {
        return random(count, true, false);
    }


    public static String randomAlphabetic(final int minLengthInclusive, final int maxLengthExclusive) {
        return randomAlphabetic(nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * <p>创建指定长度的随机字符串</p>
     *
     * <p>字符包括（a-z，A-Z）和0-9</p>
     *
     * @param count 随机字符串的长度
     * @return 随机字符串
     */
    public static String randomAlphanumeric(final int count) {
        return random(count, true, true);
    }


    public static String randomAlphanumeric(final int minLengthInclusive, final int maxLengthExclusive) {
        return randomAlphanumeric(nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * <p>创建指定长度的随机字符串</p>
     *
     * <p>包含所有可见的ASCII字符（即除空格和控制字符以外的任何字符）</p>
     *
     * @param count 随机字符串的长度
     * @return 随机字符串
     */
    public static String randomGraph(final int count) {
        return random(count, 33, 126, false, false);
    }


    public static String randomGraph(final int minLengthInclusive, final int maxLengthExclusive) {
        return randomGraph(nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * 从数字0-9中，创建指定长度的随机字符串
     *
     * @param count
     * @return
     */
    public static String randomNumeric(final int count) {
        return random(count, false, true);
    }

    public static String randomNumeric(final int minLengthInclusive, final int maxLengthExclusive) {
        return randomNumeric(nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * <p>创建指定长度的随机字符串</p>
     *
     * <p>包括所有可见的ASCII字符和空格</p>
     *
     * @param count 随机字符串的长度
     * @return 随机字符串
     */
    public static String randomPrint(final int count) {
        return random(count, 32, 126, false, false);
    }

    public static String randomPrint(final int minLengthInclusive, final int maxLengthExclusive) {
        return randomPrint(nextInt(minLengthInclusive, maxLengthExclusive));
    }


    public static String random(final int count, final boolean letters, final boolean numbers) {
        return random(count, 0, 0, letters, numbers);
    }

    public static String random(final int count, final int start, final int end, final boolean letters, final boolean numbers) {
        return random(count, start, end, letters, numbers, null, RANDOM);
    }


    public static String random(final int count, final int start, final int end, final boolean letters, final boolean numbers, final char... chars) {
        return random(count, start, end, letters, numbers, chars, RANDOM);
    }

    public static String random(int count, int start, int end, final boolean letters, final boolean numbers,
                                final char[] chars, final Random random) {
        if (count == 0) {
            return "";
        } else if (count < 0) {
            throw new IllegalArgumentException("Requested random string length " + count + " is less than 0.");
        }
        if (chars != null && chars.length == 0) {
            throw new IllegalArgumentException("The chars array must not be empty");
        }

        if (start == 0 && end == 0) {
            if (chars != null) {
                end = chars.length;
            } else {
                if (!letters && !numbers) {
                    end = Character.MAX_CODE_POINT;
                } else {
                    end = 'z' + 1;
                    start = ' ';
                }
            }
        } else {
            if (end <= start) {
                throw new IllegalArgumentException("Parameter end (" + end + ") must be greater than start (" + start + ")");
            }
        }

        final int zero_digit_ascii = 48;
        final int first_letter_ascii = 65;

        if (chars == null && (numbers && end <= zero_digit_ascii
                || letters && end <= first_letter_ascii)) {
            throw new IllegalArgumentException("Parameter end (" + end + ") must be greater then (" + zero_digit_ascii + ") for generating digits " +
                    "or greater then (" + first_letter_ascii + ") for generating letters.");
        }

        final StringBuilder builder = new StringBuilder(count);
        final int gap = end - start;

        while (count-- != 0) {
            int codePoint;
            if (chars == null) {
                codePoint = random.nextInt(gap) + start;

                switch (Character.getType(codePoint)) {
                    case Character.UNASSIGNED:
                    case Character.PRIVATE_USE:
                    case Character.SURROGATE:
                        count++;
                        continue;
                }

            } else {
                codePoint = chars[random.nextInt(gap) + start];
            }

            final int numberOfChars = Character.charCount(codePoint);
            if (count == 0 && numberOfChars > 1) {
                count++;
                continue;
            }

            if (letters && Character.isLetter(codePoint)
                    || numbers && Character.isDigit(codePoint)
                    || !letters && !numbers) {
                builder.appendCodePoint(codePoint);

                if (numberOfChars == 2) {
                    count--;
                }

            } else {
                count++;
            }
        }
        return builder.toString();
    }

    /**
     * <p>从指定的字符集中，创建指定长度的随机字符串<p/>
     *
     * @param count 随机字符串的长度
     * @param chars 指定的字符集
     * @return 随机字符串
     */
    public static String random(final int count, final String chars) {
        if (chars == null) {
            return random(count, 0, 0, false, false, null, RANDOM);
        }
        return random(count, chars.toCharArray());
    }

    public static String random(final int count, final char... chars) {
        if (chars == null) {
            return random(count, 0, 0, false, false, null, RANDOM);
        }
        return random(count, 0, chars.length, false, false, chars, RANDOM);
    }

    private static int nextInt(final int startInclusive, final int endExclusive) {
        if (startInclusive > endExclusive) {
            throw new IllegalArgumentException("Start value must be smaller or equal to end value.");
        }
        if (startInclusive < 0) {
            throw new IllegalArgumentException("Both range values must be non-negative.");
        }
        if (startInclusive == endExclusive) {
            return startInclusive;
        }
        return startInclusive + RANDOM.nextInt(endExclusive - startInclusive);
    }
}
