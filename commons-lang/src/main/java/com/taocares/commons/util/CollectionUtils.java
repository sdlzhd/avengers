package com.taocares.commons.util;

import com.taocares.commons.lang.Node;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * 集合操作工具
 *
 * @author Ankang
 * @date 2018/9/20
 */
public class CollectionUtils {

    /**
     * 将元素放入分组的列表中
     * @param listMap 分组的列表
     * @param key 键
     * @param value 值
     * @param <K> 键的类型
     * @param <V> 值的类型
     */
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public static <K, V> void putIntoListMap(@NotNull Map<K, List<V>> listMap, K key, V value) {
        List<V> list = listMap.get(key);
        if (list != null) {
            list.add(value);
        } else {
            synchronized (listMap) {
                list = new ArrayList<>();
                list.add(value);
                listMap.put(key, list);
            }
        }
    }

    /**
     * 判断集合是否为空
     * @param collection 待判断的集合
     * @return true-集合为空，false-集合非空
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * 判断Map是否为空
     * @param map 待判断的Map
     * @return true-为空，false-非空
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static <T extends Node> List<T> buildTree(List<T> nodes) {
        if (null == nodes) {
            return new ArrayList<>();
        }

        List<T> root = new ArrayList<>();
        HashMap<Object, Node> nodesMap = new HashMap<>();
        for (Node node : nodes){
            node.setChildren(null);
            nodesMap.put(node.getId(), node);
        }

        for (T node : nodes){
            Node parentNode = nodesMap.get(node.getParent());
            if (parentNode != null){
                parentNode.addChildren(node);
            } else {
                root.add(node);
            }
        }

        return root;
    }
}
