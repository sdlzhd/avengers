package com.taocares.commons.util;

/**
 * 秒表工具，用于计算代码执行时间
 *
 * @author ANKANG
 */
public class StopWatch {

    private State state;
    private Long startTimeMillis;
    private Long totalTimeElapsed;

    private StopWatch() {
        reset();
    }

    /**
     * 获取秒表实例
     *
     * @return 秒表实例
     */
    public static StopWatch create() {
        return new StopWatch();
    }

    /** 启动秒表，可以从暂停/就绪状态启动，停止后如需继续使用秒表，需调用{@link StopWatch#reset()}重置状态 */
    public void start() {
        switch (state) {
            case STOPPED:
                throw new IllegalStateException("stopwatch is already stopped!");
            case RUNNING:
                throw new IllegalStateException("stopwatch is already started!");
            case READY:
            case PAUSED:
                startTimeMillis = System.currentTimeMillis();
                state = State.RUNNING;
            default:
        }
    }

    /**
     * 停止秒表，停止后如需继续使用秒表，需调用{@link StopWatch#reset()}重置状态
     *
     * @return 累计运行时间（单位：毫秒）
     */
    public long stop() {
        switch (state) {
            case READY:
                throw new IllegalStateException("stopwatch is not started!");
            case STOPPED:
                throw new IllegalStateException("stopwatch is already stopped!");
            case RUNNING:
                totalTimeElapsed += System.currentTimeMillis() - startTimeMillis;
            case PAUSED:
            default:
                state = State.STOPPED;
                return totalTimeElapsed;
        }
    }

    /**
     * 暂停秒表，可以多次暂停重复获取累计时间
     *
     * @return 累计运行时间（单位：毫秒）
     */
    public long pause() {
        switch (state) {
            case READY:
                throw new IllegalStateException("stopwatch is not started!");
            case STOPPED:
                throw new IllegalStateException("stopwatch is already stopped!");
            case RUNNING:
                totalTimeElapsed += System.currentTimeMillis() - startTimeMillis;
            case PAUSED:
            default:
                state = State.PAUSED;
                return totalTimeElapsed;
        }
    }

    /** 重置秒表状态，秒表初始化或停止后需要调用此方法 */
    public void reset() {
        state = State.READY;
        totalTimeElapsed = 0L;
    }

    enum State {
        /** 就绪 */
        READY,
        /** 正在运行 */
        RUNNING,
        /** 已停止 */
        STOPPED,
        /** 已暂停 */
        PAUSED;
    }
}
