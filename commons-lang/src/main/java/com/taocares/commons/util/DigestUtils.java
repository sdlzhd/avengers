package com.taocares.commons.util;

import com.twmacinta.util.MD5;
import com.twmacinta.util.MD5InputStream;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * 消息摘要工具
 *
 * @author Li Zhendong
 * @date 2018/12/20
 */
public class DigestUtils {

    /**
     * 计算并返回输入流的MD5
     *
     * @param stream 输入流
     * @return MD5值
     */
    public static String md5(InputStream stream) throws IOException {
        long buf_size = stream.available();
        if (buf_size < 512) buf_size = 512;
        if (buf_size > 65536) buf_size = 65536;
        byte[] buf = new byte[(int) buf_size];
        MD5InputStream in = new MD5InputStream(stream);
        while (in.read(buf) != -1) ;
        in.close();
        return MD5.asHex(in.hash());
    }

    /**
     * 计算并返回指定文件的MD5
     *
     * @param file 要计算的MD5的文件
     * @return MD5值
     */
    public static String md5(File file) throws IOException {
        return MD5.asHex(MD5.getHash(file));
    }

    /**
     * 计算并返回字符串的MD5
     *
     * @param str 要计算MD5的字符串
     * @return MD5值
     */
    public static String md5(String str) {
        MD5 md5 = new MD5();
        try {
            // 指定utf-8编码
            md5.Update(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return md5.asHex();
    }
}
