package com.taocares.commons.text;

/**
 * Mask，支持的字符如下：<br/>
 * <ol>
 * <li>'：额外字符标识（必须成对出现）</li>
 * <li>*：需要掩盖的字符</li>
 * <li><：和任意字符结合使用，表示在此字符之前所有字符均要掩盖</li>
 * <li>A：任意字符</li>
 * <li>U：需要转大写的字符</li>
 * <li>L：需要转小写的字符</li>
 * </ol>
 *
 * @author Ankang
 * @date 2018/11/28
 */
public class Mask {
    // Potential values in mask.
    /**
     * 额外字符标识
     */
    public static final char LITERAL_KEY = '\'';
    /**
     * 转换为大写
     */
    public static final char UPPER_KEY = 'U';
    /**
     * 转换为小写
     */
    public static final char LOWER_KEY = 'L';
    /**
     * 任意字符
     */
    public static final char ANY_KEY = 'A';
    /**
     * 需要脱敏的字符
     */
    public static final char MASK_KEY = '*';
    /**
     * 截止符
     */
    public static final char BEFORE_KEY = '<';

    /**
     * 身份证号Mask（最后4位掩盖）
     */
    public static final String ID_CARD_MASK = "AAAAAAAAAAAAAA****";
    /**
     * 手机号Mask（中间4位掩盖）
     */
    public static final String MOBILE_MASK = "AAA****";
    /**
     * 邮箱Mask（保留前三位字符，@之前均掩盖）
     */
    public static final String EMAIL_MASK = "AAA<@";

    /**
     * Mask构建器
     */
    public static class Builder {
        private StringBuilder sb;

        public Builder() {
            sb = new StringBuilder();
        }

        /**
         * 设置任意字符
         *
         * @param count 数量
         */
        public Builder any(int count) {
            for (int i = 0; i < count; i++) {
                sb.append(ANY_KEY);
            }
            return this;
        }

        /**
         * 设置需要掩盖的字符
         *
         * @param count 数量
         */
        public Builder mask(int count) {
            for (int i = 0; i < count; i++) {
                sb.append(MASK_KEY);
            }
            return this;
        }

        /**
         * 设置截止字符（截止字符及以后的字符不受蒙版处理）<br/>
         * 不要和{@link #mask(int)}方法混用
         *
         * @param c 截止字符
         */
        public Builder maskBefore(char c) {
            sb.append(BEFORE_KEY).append(c);
            return this;
        }

        /**
         * 设置需要转大写的字符
         *
         * @param count 数量
         */
        public Builder upper(int count) {
            for (int i = 0; i < count; i++) {
                sb.append(UPPER_KEY);
            }
            return this;
        }

        /**
         * 设置需要转小写的字符
         *
         * @param count 数量
         */
        public Builder lower(int count) {
            for (int i = 0; i < count; i++) {
                sb.append(LOWER_KEY);
            }
            return this;
        }

        /**
         * 设置（原始数据之外的）模板字符
         *
         * @param cs 模板字符串
         */
        public Builder literal(CharSequence cs) {
            sb.append(LITERAL_KEY).append(cs).append(LITERAL_KEY);
            return this;
        }

        /**
         * 设置（原始数据之外的）模板字符
         *
         * @param c 模板字符
         */
        public Builder literal(char c) {
            sb.append(LITERAL_KEY).append(c).append(LITERAL_KEY);
            return this;
        }

        /**
         * 生成Mask
         */
        public String build() {
            return sb.toString();
        }
    }
}
