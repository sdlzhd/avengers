package com.taocares.commons.util;

import com.taocares.commons.lang.Node;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;

/**
 * @description:
 * @author: yunpeng liu
 * @create: 2019-01-18 14:08
 **/
public class TreeUtilsTest {
    @Test
    public void testBuildTree() {
        List<Node> originalList = Arrays.asList(
                new DummyNode(1L, 0L, null, "1l", "1v"),
                new DummyNode(2L, 0L, null, "2l", "2v"),
                new DummyNode(3L, 1L, null, "3l", "3v"),
                new DummyNode(4L, 1L, null, "4l", "4v"),
                new DummyNode(5L, 1L, null, "5l", "5v"),
                new DummyNode(6L, 2L, null, "6l", "6v"),
                new DummyNode(7L, 2L, null, "7l", "7v"),
                new DummyNode(8L, 3L, null, "8l", "8v"),
                new DummyNode(9L, 3L, null, "9l", "9v"),
                new DummyNode(10L, 8L, null, "10l", "10v"),
                new DummyNode(11L, 10L, null, "11l", "11v")
        );

        List<Node> result = CollectionUtils.buildTree(originalList);
        Assert.assertEquals(2, result.size());
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private class DummyNode implements Node<Long> {
        private Long id;
        private Long parent;
        private List<Node> children;

        private String label;
        private String value;
    }
}
