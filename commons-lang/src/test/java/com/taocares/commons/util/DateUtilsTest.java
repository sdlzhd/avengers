package com.taocares.commons.util;

import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.testng.Assert.assertEquals;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/11/1
 */
public class DateUtilsTest {

    @Test
    public void testToDate() {
        Date expected = new Date();
        long millis = expected.getTime();
        assertEquals(expected, DateUtils.toDate(DateUtils.toLocalDateTime(millis)));
    }

    @Test
    public void testToInstant() {
        Date date = new Date();
        LocalDateTime localDateTime = DateUtils.toLocalDateTime(date);
        assertEquals(date.toInstant(), DateUtils.toInstant(localDateTime));
    }

    @Test
    public void testToMillis() {
        Date date = new Date();
        assertEquals(date.getTime(), DateUtils.toInstant(DateUtils.toLocalDateTime(date)).toEpochMilli());
    }

    @Test
    public void testStartOfDate() {
        Date date = new Date();
        System.err.println("date: " + date);
        Date startOfYear = DateUtils.startOf(date, ChronoUnit.YEARS);
        System.err.println("startOfYear: " + startOfYear);
        assertEquals(startOfYear.getYear(), date.getYear());
        assertEquals(startOfYear.getMonth(), 0);
        assertEquals(startOfYear.getDate(), 1);
        assertEquals(startOfYear.getHours(), 0);
        assertEquals(startOfYear.getMinutes(), 0);
        assertEquals(startOfYear.getSeconds(), 0);
        Date startOfMonth = DateUtils.startOf(date, ChronoUnit.MONTHS);
        System.err.println("startOfMonth: " + startOfMonth);
        assertEquals(startOfMonth.getYear(), date.getYear());
        assertEquals(startOfMonth.getMonth(), date.getMonth());
        assertEquals(startOfMonth.getDate(), 1);
        assertEquals(startOfMonth.getHours(), 0);
        assertEquals(startOfMonth.getMinutes(), 0);
        assertEquals(startOfMonth.getSeconds(), 0);
        Date startOfDay = DateUtils.startOf(date, ChronoUnit.DAYS);
        System.err.println("startOfDay: " + startOfDay);
        assertEquals(startOfDay.getDate(), date.getDate());
        assertEquals(startOfDay.getHours(), 0);
        assertEquals(startOfDay.getMinutes(), 0);
        assertEquals(startOfDay.getSeconds(), 0);
        Date startOfHour = DateUtils.startOf(date, ChronoUnit.HOURS);
        System.err.println("startOfHour: " + startOfHour);
        assertEquals(startOfHour.getDate(), date.getDate());
        assertEquals(startOfHour.getHours(), date.getHours());
        assertEquals(startOfHour.getMinutes(), 0);
        assertEquals(startOfHour.getSeconds(), 0);
    }

    @Test
    public void testEndOfDate() {
        Date date = new Date();
        System.err.println("date: " + date);
        Date endOfYear = DateUtils.endOf(date, ChronoUnit.YEARS);
        System.err.println("endOfYear: " + endOfYear);
        assertEquals(endOfYear.getYear(), date.getYear());
        assertEquals(endOfYear.getMonth(), 11);
        assertEquals(endOfYear.getDate(), 31);
        assertEquals(endOfYear.getHours(), 23);
        assertEquals(endOfYear.getMinutes(), 59);
        assertEquals(endOfYear.getSeconds(), 59);
        Date endOfMonth = DateUtils.endOf(date, ChronoUnit.MONTHS);
        System.err.println("endOfMonth: " + endOfMonth);
        assertEquals(endOfMonth.getYear(), date.getYear());
        assertEquals(endOfMonth.getMonth(), date.getMonth());
        assertEquals(endOfMonth.getHours(), 23);
        assertEquals(endOfMonth.getMinutes(), 59);
        assertEquals(endOfMonth.getSeconds(), 59);
        Date endOfDay = DateUtils.endOf(date, ChronoUnit.DAYS);
        System.err.println("endOfDay: " + endOfDay);
        assertEquals(endOfDay.getDate(), date.getDate());
        assertEquals(endOfDay.getHours(), 23);
        assertEquals(endOfDay.getMinutes(), 59);
        assertEquals(endOfDay.getSeconds(), 59);
        Date endOfHour = DateUtils.endOf(date, ChronoUnit.HOURS);
        System.err.println("endOfHour: " + endOfHour);
        assertEquals(endOfHour.getDate(), date.getDate());
        assertEquals(endOfHour.getHours(), date.getHours());
        assertEquals(endOfHour.getMinutes(), 59);
        assertEquals(endOfHour.getSeconds(), 59);
    }

    @Test
    public void testStartOfLocalDateTime() {
        LocalDateTime date = LocalDateTime.now();
        System.err.println("date: " + date);
        LocalDateTime startOfYear = DateUtils.startOf(date, ChronoUnit.YEARS);
        System.err.println("startOfYear: " + startOfYear);
        LocalDateTime expectedStartOfYear = LocalDateTime.of(
                LocalDate.of(date.getYear(), 1, 1),
                LocalTime.MIN
        );
        assertEquals(startOfYear, expectedStartOfYear);
        LocalDateTime startOfMonth = DateUtils.startOf(date, ChronoUnit.MONTHS);
        System.err.println("startOfMonth: " + startOfMonth);
        LocalDateTime expectedStartOfMonth = LocalDateTime.of(
                LocalDate.of(date.getYear(), date.getMonthValue(), 1),
                LocalTime.MIN
        );
        assertEquals(startOfMonth, expectedStartOfMonth);
        LocalDateTime startOfDay = DateUtils.startOf(date, ChronoUnit.DAYS);
        System.err.println("startOfDay: " + startOfDay);
        LocalDateTime expectedStartOfDay = LocalDateTime.of(
                LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()),
                LocalTime.MIN
        );
        assertEquals(startOfDay, expectedStartOfDay);
        LocalDateTime startOfHour = DateUtils.startOf(date, ChronoUnit.HOURS);
        System.err.println("startOfHour: " + startOfHour);
        LocalDateTime expectedStartOfHour = LocalDateTime.of(
                LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()),
                LocalTime.of(date.getHour(), 0, 0, 0)
        );
        assertEquals(startOfHour, expectedStartOfHour);
    }

    @Test
    public void testEndOfLocalDateTime() {
        LocalDateTime date = LocalDateTime.now();
        System.err.println("date: " + date);
        LocalDateTime endOfYear = DateUtils.endOf(date, ChronoUnit.YEARS);
        System.err.println("endOfYear: " + endOfYear);
        LocalDateTime expectedEndOfYear = LocalDateTime.of(
                LocalDate.of(date.getYear(), 12, 31),
                LocalTime.MAX
        );
        assertEquals(endOfYear, expectedEndOfYear);
        LocalDateTime endOfMonth = DateUtils.endOf(date, ChronoUnit.MONTHS);
        System.err.println("endOfMonth: " + endOfMonth);
        LocalDateTime expectedEndOfMonth = LocalDateTime.of(
                date.toLocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1),
                LocalTime.MAX
        );
        assertEquals(endOfMonth, expectedEndOfMonth);
        LocalDateTime endOfDay = DateUtils.endOf(date, ChronoUnit.DAYS);
        System.err.println("endOfDay: " + endOfDay);
        LocalDateTime expectedEndOfDay = LocalDateTime.of(
                LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()),
                LocalTime.MAX
        );
        assertEquals(endOfDay, expectedEndOfDay);
        LocalDateTime endOfHour = DateUtils.endOf(date, ChronoUnit.HOURS);
        System.err.println("endOfHour: " + endOfHour);
        LocalDateTime expectedEndOfHour = LocalDateTime.of(
                LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()),
                LocalTime.of(date.getHour(), 59, 59, 999999999)
        );
        assertEquals(endOfHour, expectedEndOfHour);
    }
}