package com.taocares.commons.util;

import org.testng.annotations.Test;

import java.lang.reflect.Field;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/11/1
 */
public class ReflectionUtilsTest {

    private long id;

    @Test
    public void testClassType() throws NoSuchFieldException {
        Field field = ReflectionUtils.getField(ReflectionUtilsTest.class, "id");
        assertEquals(long.class, field.getType());
        assertNotEquals(Long.class, field.getType());
    }
}
