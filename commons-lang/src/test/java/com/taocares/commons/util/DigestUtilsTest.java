package com.taocares.commons.util;

import org.testng.annotations.Test;

import java.io.*;
import java.net.URISyntaxException;

import static org.testng.Assert.*;

public class DigestUtilsTest {

    @Test
    public void testMd5ForString() throws UnsupportedEncodingException {
        assertEquals("dbefd3ada018615b35588a01e216ae6e", DigestUtils.md5("你好，世界"));
    }

    @Test
    public void testMd5ForFile() throws IOException, URISyntaxException {
        assertEquals("0a0496c95957848317c311b088d2b62b", DigestUtils.md5(new File(getClass().getResource("/modena.css").toURI())));
    }

    @Test
    public void testMd5ForInputStream() throws IOException, URISyntaxException {
        File file = new File(getClass().getResource("/modena.css").toURI());
        InputStream inputStream = new FileInputStream(file);
        assertEquals("0a0496c95957848317c311b088d2b62b", DigestUtils.md5(inputStream));
    }
}