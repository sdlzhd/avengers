package com.taocares.commons.util;

import com.taocares.commons.text.Mask;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/11/28
 */
public class MaskUtilsTest {

    @Test
    public void testBuilder() {
        String mask = new Mask.Builder()
                .literal("PB").any(5).mask(3).build();
        assertEquals("'PB'AAAAA***", mask);
    }

    @Test
    public void testMask() {
        String mask = new Mask.Builder()
                .upper(2).literal("-").mask(2).literal('-').build();
        String masked = MaskUtils.mask("abcdefg", mask);
        assertEquals("AB-**-efg", masked);
        mask = new Mask.Builder()
                .literal('(').any(4).literal(')').mask(3).build();
        masked = MaskUtils.mask("053212345678", mask);
        assertEquals("(0532)***45678", masked);
        mask = new Mask.Builder()
                .any(3).mask(6).build();
        masked = MaskUtils.mask("15912345678", mask, '#', false);
        assertEquals("159######78", masked);
        mask = new Mask.Builder()
                .any(3).literal("***").mask(6).build();
        masked = MaskUtils.mask("15912345678", mask);
        assertEquals("159*********78", masked);
        mask = new Mask.Builder()
                .any(4).maskBefore('@').build();
        masked = MaskUtils.mask("test.me@qdcares.cn", mask);
        assertEquals("test***@qdcares.cn", masked);
        mask = new Mask.Builder()
                .any(4).maskBefore('@').build();
        masked = MaskUtils.mask("test.qdcares.cn", mask);
        assertEquals("test***********", masked);
    }

}