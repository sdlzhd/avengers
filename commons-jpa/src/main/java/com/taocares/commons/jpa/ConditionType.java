package com.taocares.commons.jpa;

/**
 * 条件类型
 *
 * @author Ankang
 * @date 2018/9/14
 */
public enum ConditionType {
    /**
     * 相等
     */
    EQUAL(ConditionGroup.DEFAULT),
    /**
     * 不相等
     */
    NOT_EQUAL(ConditionGroup.DEFAULT),
    /**
     * 匹配开始
     */
    STARTS_WITH(ConditionGroup.STRING),
    /**
     * 匹配结束
     */
    ENDS_WITH(ConditionGroup.STRING),
    /**
     * 全模糊匹配
     */
    CONTAINS(ConditionGroup.STRING),
    /**
     * 大于
     */
    GREATER_THAN(ConditionGroup.COMPARISON),
    /**
     * 小于等于
     */
    NOT_GREATER_THAN(ConditionGroup.COMPARISON),
    /**
     * 小于
     */
    LESS_THAN(ConditionGroup.COMPARISON),
    /**
     * 大于等于
     */
    NOT_LESS_THAN(ConditionGroup.COMPARISON),
    /**
     * 包含于
     */
    IN(ConditionGroup.DEFAULT);

    private final ConditionGroup group;

    ConditionType(ConditionGroup group) {
        this.group = group;
    }

    public ConditionGroup getGroup() {
        return group;
    }

    public enum ConditionGroup {
        DEFAULT, STRING, COMPARISON
    }
}
