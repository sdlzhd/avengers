package com.taocares.commons.jpa;

import com.google.common.base.CaseFormat;
import com.taocares.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * 自定义查询接口的基本实现
 *
 * @author Ankang
 * @date 2018/11/5
 */
@Repository
public class RepositoryEnhancerImpl implements RepositoryEnhancer {

    private final EntityManager entityManager;

    @Autowired
    public RepositoryEnhancerImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Query createQuery(String queryString) {
        return entityManager.createQuery(queryString);
    }

    @Override
    public <T> TypedQuery<T> createQuery(String queryString, Class<T> resultClass) {
        return entityManager.createQuery(queryString, resultClass);
    }

    @Override
    public Query createNativeQuery(String queryString) {
        return entityManager.createNativeQuery(queryString);
    }

    @Override
    public Query createNativeQuery(String queryString, Class resultClass) {
        return entityManager.createNativeQuery(queryString, resultClass);
    }

    @Override
    public List findByJpql(String queryString) {
        return createQuery(queryString).getResultList();
    }

    @Override
    public List findByJpql(String queryString, Pageable pageable) {
        int startPosition = pageable.getPageNumber() * pageable.getPageSize();
        return createQuery(queryString)
                .setFirstResult(startPosition)
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    @Override
    public List findByJpql(String queryString, Map<String, Object> parameters) {
        Query query = createQuery(queryString);
        setQueryParameter(query, parameters);
        return query.getResultList();
    }

    @Override
    public List findByJpql(String queryString, Map<String, Object> parameters, Pageable pageable) {
        Query query = createQuery(queryString);
        setQueryParameter(query, parameters);
        int startPosition = pageable.getPageNumber() * pageable.getPageSize();
        return query.setFirstResult(startPosition)
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    @Override
    public List findByJpql(String queryString, Object[] parameters) {
        Query query = createQuery(queryString);
        setQueryParameter(query, parameters);
        return query.getResultList();
    }

    @Override
    public List findByJpql(String queryString, Object[] parameters, Pageable pageable) {
        Query query = createQuery(queryString);
        setQueryParameter(query, parameters);
        int startPosition = pageable.getPageNumber() * pageable.getPageSize();
        return query.setFirstResult(startPosition)
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    @Override
    public <T> List<T> findByJpql(String queryString, Class<T> resultClass) {
        return createQuery(queryString, resultClass).getResultList();
    }

    @Override
    public <T> List<T> findByJpql(String queryString, Class<T> resultClass, Pageable pageable) {
        int startPosition = pageable.getPageNumber() * pageable.getPageSize();
        return createQuery(queryString, resultClass)
                .setFirstResult(startPosition)
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    @Override
    public <T> List<T> findByJpql(String queryString, Map<String, Object> parameters, Class<T> resultClass) {
        TypedQuery<T> query = createQuery(queryString, resultClass);
        setQueryParameter(query, parameters);
        return query.getResultList();
    }

    @Override
    public <T> List<T> findByJpql(String queryString, Map<String, Object> parameters, Class<T> resultClass, Pageable pageable) {
        TypedQuery<T> query = createQuery(queryString, resultClass);
        setQueryParameter(query, parameters);
        int startPosition = pageable.getPageNumber() * pageable.getPageSize();
        return query.setFirstResult(startPosition)
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    @Override
    public <T> List<T> findByJpql(String queryString, Object[] parameters, Class<T> resultClass) {
        TypedQuery<T> query = createQuery(queryString, resultClass);
        setQueryParameter(query, parameters);
        return query.getResultList();
    }

    @Override
    public <T> List<T> findByJpql(String queryString, Object[] parameters, Class<T> resultClass, Pageable pageable) {
        TypedQuery<T> query = createQuery(queryString, resultClass);
        setQueryParameter(query, parameters);
        int startPosition = pageable.getPageNumber() * pageable.getPageSize();
        return query.setFirstResult(startPosition)
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    @Override
    public String getOrderByFragment(Class<?> entityClass, Sort sort, Map<Class, String> aliasMap) {
        StringBuilder orderBy = new StringBuilder(" order by");
        boolean first = true;
        for (Sort.Order order : sort) {
            Class propertyOwnerClass = getPropertyOwnerClass(entityClass, order.getProperty());
            String alias = aliasMap.get(propertyOwnerClass);
            if (alias == null) {
                throw new IllegalArgumentException("Could not find alias for class: " + propertyOwnerClass);
            }
            String columnName = getColumnName(entityClass, order.getProperty());
            if (first) {
                first = false;
            } else {
                orderBy.append(',');
            }
            // " alias.columnName ASC"
            orderBy.append(' ').append(alias).append('.').append(columnName)
                    .append(' ').append(order.getDirection());
        }
        return orderBy.toString();
    }

    @Override
    public String getColumnName(Class<?> entityClass, String propertyName) {
        EntityType entityType = entityManager.getMetamodel().entity(entityClass);
        for (String property : propertyName.split("\\.")) {
            SingularAttribute attribute = entityType.getSingularAttribute(property);
            Type type = attribute.getType();
            switch (type.getPersistenceType()) {
                case ENTITY:
                    entityType = (EntityType) type;
                    break;
                case BASIC:
                    Field field = (Field) attribute.getJavaMember();
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        if (StringUtils.isNotEmpty(column.name())) {
                            return column.name();
                        }
                    }
                    return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, property);
                default:
                    throw new RuntimeException("persistent type [" + type.getPersistenceType() + "] of property [" + propertyName + "] not supported");
            }
        }
        throw new RuntimeException("property [" + propertyName + "] is not basic in class: " + entityClass);
    }

    private Class getPropertyOwnerClass(Class<?> entityClass, String propertyName) {
        EntityType entityType = entityManager.getMetamodel().entity(entityClass);
        for (String property : propertyName.split("\\.")) {
            SingularAttribute attribute = entityType.getSingularAttribute(property);
            Type type = attribute.getType();
            switch (type.getPersistenceType()) {
                case ENTITY:
                    entityType = (EntityType) type;
                    break;
                case BASIC:
                    return entityType.getJavaType();
                default:
                    throw new RuntimeException("persistent type [" + type.getPersistenceType() + "] of property [" + propertyName + "] not supported");
            }
        }
        throw new RuntimeException("property [" + propertyName + "] is not basic in class: " + entityClass);
    }

    private void setQueryParameter(Query query, Map<String, Object> parameters) {
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }

    private void setQueryParameter(Query query, Object[] parameters) {
        for (int i = 0; i < parameters.length; i++) {
            query.setParameter(i, parameters[i]);
        }
    }
}
