package com.taocares.commons.jpa;

import java.time.temporal.ChronoUnit;

/**
 * 日期处理方式（取特定时间粒度的开始/结束）
 *
 * @author Ankang
 * @date 2019/2/13
 */
public enum DatePolicy {
    // 年月日时分秒的开始
    START_OF_YEAR(Type.START, ChronoUnit.YEARS),
    START_OF_MONTH(Type.START, ChronoUnit.MONTHS),
    START_OF_DAY(Type.START, ChronoUnit.DAYS),
    START_OF_HOUR(Type.START, ChronoUnit.HOURS),
    START_OF_MINUTE(Type.START, ChronoUnit.MINUTES),
    START_OF_SECOND(Type.START, ChronoUnit.SECONDS),
    // 年月日时分秒的结束
    END_OF_YEAR(Type.END, ChronoUnit.YEARS),
    END_OF_MONTH(Type.END, ChronoUnit.MONTHS),
    END_OF_DAY(Type.END, ChronoUnit.DAYS),
    END_OF_HOUR(Type.END, ChronoUnit.HOURS),
    END_OF_MINUTE(Type.END, ChronoUnit.MINUTES),
    END_OF_SECOND(Type.END, ChronoUnit.SECONDS),
    /**
     * 无处理
     */
    NONE(Type.START, ChronoUnit.NANOS);

    private final Type type;
    private final ChronoUnit unit;

    DatePolicy(Type type, ChronoUnit unit) {
        this.type = type;
        this.unit = unit;
    }

    public Type getType() {
        return type;
    }

    public ChronoUnit getUnit() {
        return unit;
    }

    /**
     * 处理方式（开始、结束）
     */
    public enum Type {
        START, END
    }
}
