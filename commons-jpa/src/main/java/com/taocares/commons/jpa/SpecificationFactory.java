package com.taocares.commons.jpa;

import com.taocares.commons.lang.Nullable;
import com.taocares.commons.lang.UnsupportedTypeException;
import com.taocares.commons.util.CollectionUtils;
import com.taocares.commons.util.DateUtils;
import com.taocares.commons.util.ReflectionUtils;
import com.taocares.commons.util.StringUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 查询条件自动构建工具
 *
 * @author Ankang
 * @date 2018/9/14
 */
public class SpecificationFactory {

    private static final Logger logger = LoggerFactory.getLogger(SpecificationFactory.class);

    private static final String INVALID_CONDITION_TYPE = "输入条件类型不正确！";
    private static final String UNSUPPORTED_TYPE = "不支持的字段类型！";

    private static final String CHAR_CONCAT = "+";
    private static final String CHAR_MULTI = "*";
    private static final String CHAR_LIKE = "%";
    private static final String GROUP_PREFIX = "group-";

    private static ThreadLocal<Boolean> isJointThreadLocal = ThreadLocal.withInitial(() -> false);

    /**
     * 获取查询条件（使用{@link org.springframework.data.jpa.repository.JpaSpecificationExecutor}执行查询）
     *
     * @param queryObject Query Object
     * @param <T>         Entity的类型
     * @param <QO>        Query Object的类型
     * @return 查询条件定义
     */
    public static <T, QO> Specification<T> getSpecification(final QO queryObject) {
        return (Specification<T>) (root, query, criteriaBuilder) -> {
            Map<String, List<Predicate>> groupedPredicates = new HashMap<>();
            List<ConditionWrapper> conditionWrappers = new ArrayList<>();
            Class<?> clz = queryObject.getClass();
            boolean isQueryObject = clz.isAnnotationPresent(QueryObject.class);
            // 支持QO的属性继承
            for (Field field : ReflectionUtils.getAllFields(clz)) {
                String fieldName = field.getName();
                Object queryValue = ReflectionUtils.getFieldValue(field, queryObject);
                QueryCondition condition = AnnotationUtils.getAnnotation(field, QueryCondition.class);
                if (condition != null) {
                    conditionWrappers.add(new ConditionWrapper(fieldName, queryValue, condition));
                } else if (isQueryObject) {
                    conditionWrappers.add(new ConditionWrapper(fieldName, queryValue, SimpleCondition.INSTANCE));
                }
            }
            // 暂时不使用排序
//            conditionWrappers.sort(Comparator.naturalOrder());
            for (ConditionWrapper wrapper : conditionWrappers) {
                processQueryCondition(
                        wrapper.condition, wrapper.fieldName, wrapper.queryValue,
                        groupedPredicates, root, criteriaBuilder);
            }
            // 避免联合查询出现重复结果
            if (isJointThreadLocal.get()) {
                query.distinct(true); // 已知问题：如果结果中有CLOB等字段会失败
            }
            Predicate predicate = processResult(groupedPredicates, criteriaBuilder);
            isJointThreadLocal.set(false);
            return predicate;
        };
    }

    /**
     * 处理QueryCondition注解
     *
     * @param condition         查询条件注解
     * @param fieldName         QO中的字段名
     * @param queryValue        QO中的值
     * @param groupedPredicates 分组的查询条件
     * @param root              源对象
     * @param criteriaBuilder   查询构建器
     */
    private static void processQueryCondition(QueryCondition condition, String fieldName, Object queryValue, Map<String, List<Predicate>> groupedPredicates, Root<?> root, CriteriaBuilder criteriaBuilder) {
        String group = StringUtils.isEmpty(condition.group()) ?
                fieldName : GROUP_PREFIX + condition.group();
        if (condition.field().length != 0) {
            for (String targetField : condition.field()) {
                // “*”只能出现在起始位置
                if (targetField.indexOf(CHAR_MULTI) > 0) {
                    throw new IllegalArgumentException(CHAR_MULTI + "只能出现在目标字段起始位置！");
                }
                // 是一对多/多对多，并且条件不为空或者不忽略时，应该联合查询
                boolean shouldJoin = targetField.startsWith(CHAR_MULTI)
                        && (!isEmptyValue(queryValue) || condition.policy() == NullPolicy.INCLUDE);
                Expression<?> path = getExpression(root, targetField, shouldJoin, criteriaBuilder);
                Predicate predicate = getPredicate(path, queryValue, condition, criteriaBuilder);
                if (predicate != null) {
                    CollectionUtils.putIntoListMap(groupedPredicates, group, predicate);
                }
            }
        } else {
            Predicate predicate = getPredicate(root.get(group), queryValue, condition, criteriaBuilder);
            if (predicate != null) {
                CollectionUtils.putIntoListMap(groupedPredicates, group, predicate);
            }
        }
    }

    /**
     * 将分组的条件拼装为最终条件
     *
     * @param groupedPredicates 分组条件
     * @param criteriaBuilder   查询构建器
     * @return 最终条件
     */
    private static Predicate processResult(Map<String, List<Predicate>> groupedPredicates, CriteriaBuilder criteriaBuilder) {
        List<Predicate> processedPredicates = new ArrayList<>();
        for (List<Predicate> predicates : groupedPredicates.values()) {
            processedPredicates.add(criteriaBuilder.or(predicates.toArray(new Predicate[0])));
        }
        return criteriaBuilder.and(processedPredicates.toArray(new Predicate[0]));
    }

    /**
     * 条件获取
     *
     * @param expression      属性路径
     * @param value           用于比较的值
     * @param condition       查询条件定义
     * @param criteriaBuilder 查询构建器
     * @return 条件
     * @throws UnsupportedTypeException 如果调用者在不能比较的字段上使用了比较类型的条件，或者传入了不支持的条件
     */
    @Nullable
    @SuppressWarnings("unchecked")
    private static Predicate getPredicate(Expression<?> expression, Object value, QueryCondition condition, CriteriaBuilder criteriaBuilder) {
        if (expression == null) {
            return null;
        }
        if (value instanceof String) {
            if (condition.trim()) {
                value = ((String) value).trim();
            }
            if (condition.ignoreCase()) {
                value = ((String) value).toLowerCase();
                expression = criteriaBuilder.lower((Expression<String>) expression);
            }
        }
        value = processDateValueIfNecessary(value, condition.datePolicy());
        if (isEmptyValue(value)) {
            if (condition.policy() == NullPolicy.INCLUDE) {
                return criteriaBuilder.isNull(expression);
            }
        } else {
            switch (condition.type().getGroup()) {
                case DEFAULT:
                    return getDefaultPredicate(expression, value, condition.type(), criteriaBuilder);
                case STRING:
                    if (value instanceof String) {
                        return getStringPredicate((Expression<String>) expression, (String) value, condition.type(), criteriaBuilder);
                    } else {
                        throw new UnsupportedTypeException(UNSUPPORTED_TYPE + value.getClass());
                    }
                case COMPARISON:
                    if (value instanceof Number) {
                        return getNumberPredicate((Expression<? extends Number>) expression, (Number) value, condition.type(), criteriaBuilder);
                    } else if (value instanceof Comparable) {
                        return getComparablePredicate((Expression<? extends Comparable<Object>>) expression, (Comparable) value, condition.type(), criteriaBuilder);
                    } else {
                        throw new UnsupportedTypeException(UNSUPPORTED_TYPE + value.getClass());
                    }
                default:
            }
        }
        return null;
    }

    /**
     * 根据日期策略处理日期并返回
     *
     * @param value      原始的值
     * @param datePolicy 日期策略
     * @return 处理后的值
     */
    private static Object processDateValueIfNecessary(Object value, DatePolicy datePolicy) {
        if (datePolicy != DatePolicy.NONE) {
            switch (datePolicy.getType()) {
                case START:
                    if (value instanceof Date) {
                        value = DateUtils.startOf((Date) value, datePolicy.getUnit());
                    } else if (value instanceof LocalDateTime) {
                        value = DateUtils.startOf((LocalDateTime) value, datePolicy.getUnit());
                    }
                    break;
                case END:
                    if (value instanceof Date) {
                        value = DateUtils.endOf((Date) value, datePolicy.getUnit());
                    } else if (value instanceof LocalDateTime) {
                        value = DateUtils.endOf((LocalDateTime) value, datePolicy.getUnit());
                    }
                    break;
            }
        }
        return value;
    }

    /**
     * 基本判断条件获取
     *
     * @param expression      属性路径
     * @param value           用于比较的值
     * @param conditionType   比较条件
     * @param criteriaBuilder 查询构建器
     * @return 条件
     */
    private static Predicate getDefaultPredicate(Expression<?> expression, Object value, ConditionType conditionType, CriteriaBuilder criteriaBuilder) {
        switch (conditionType) {
            case EQUAL:
                return criteriaBuilder.equal(expression, value);
            case NOT_EQUAL:
                return criteriaBuilder.notEqual(expression, value);
            case IN:
                if (value instanceof Collection) {
                    return expression.in((Collection) value);
                } else if (value.getClass().isArray()) {
                    return expression.in((Object[]) value);
                } else {
                    return expression.in(value);
                }
            default:
                throw new UnsupportedTypeException(INVALID_CONDITION_TYPE + conditionType);
        }
    }

    /**
     * 模糊匹配条件获取
     *
     * @param expression      属性路径
     * @param value           用于比较的值
     * @param conditionType   比较条件
     * @param criteriaBuilder 查询构建器
     * @return 条件
     */
    private static Predicate getStringPredicate(Expression<String> expression, String value, ConditionType conditionType, CriteriaBuilder criteriaBuilder) {
        switch (conditionType) {
            case CONTAINS:
                return criteriaBuilder.like(expression, CHAR_LIKE + value + CHAR_LIKE);
            case STARTS_WITH:
                return criteriaBuilder.like(expression, value + CHAR_LIKE);
            case ENDS_WITH:
                return criteriaBuilder.like(expression, CHAR_LIKE + value);
            default:
                throw new UnsupportedTypeException(INVALID_CONDITION_TYPE + conditionType);
        }
    }

    /**
     * 数值比较的条件获取
     *
     * @param expression      属性路径
     * @param value           用于比较的值
     * @param conditionType   比较条件
     * @param criteriaBuilder 查询构建器
     * @return 条件
     */
    private static Predicate getNumberPredicate(Expression<? extends Number> expression, Number value, ConditionType conditionType, CriteriaBuilder criteriaBuilder) {
        switch (conditionType) {
            case GREATER_THAN:
                return criteriaBuilder.gt(expression, value);
            case NOT_GREATER_THAN:
                return criteriaBuilder.le(expression, value);
            case LESS_THAN:
                return criteriaBuilder.lt(expression, value);
            case NOT_LESS_THAN:
                return criteriaBuilder.ge(expression, value);
            default:
                throw new UnsupportedTypeException(INVALID_CONDITION_TYPE + conditionType);
        }
    }

    /**
     * 对象比较的条件获取
     *
     * @param expression      属性路径
     * @param value           用于比较的值
     * @param conditionType   比较条件
     * @param criteriaBuilder 查询构建器
     * @return 条件
     */
    private static Predicate getComparablePredicate(Expression<? extends Comparable<Object>> expression, Comparable value, ConditionType conditionType, CriteriaBuilder criteriaBuilder) {
        switch (conditionType) {
            case GREATER_THAN:
                return criteriaBuilder.greaterThan(expression, value);
            case NOT_GREATER_THAN:
                return criteriaBuilder.lessThanOrEqualTo(expression, value);
            case LESS_THAN:
                return criteriaBuilder.lessThan(expression, value);
            case NOT_LESS_THAN:
                return criteriaBuilder.greaterThanOrEqualTo(expression, value);
            default:
                throw new UnsupportedTypeException(INVALID_CONDITION_TYPE + conditionType);
        }
    }

    /**
     * 获取查询路径，如果路径不正确会返回null
     *
     * @param root            对象
     * @param compositeField  字段名
     * @param shouldJoin      是否应该联合查询
     * @param criteriaBuilder 查询构建器
     * @return 查询路径
     */
    private static Expression<?> getExpression(Root<?> root, String compositeField, boolean shouldJoin, CriteriaBuilder criteriaBuilder) {
        if (compositeField.contains(CHAR_CONCAT)) {
            Expression<String> result = null;
            Expression<String> temp;
            for (String field : compositeField.split("\\+")) {
                temp = getPath(root, field, shouldJoin);
                if (temp == null) {
                    return null;
                }
                result = result == null ? temp : criteriaBuilder.concat(result, temp);
            }
            return result;
        } else {
            return getPath(root, compositeField, shouldJoin);
        }
    }

    /**
     * 获取复杂字段表达式的路径，如果路径不正确会返回null<br/>
     * 非类型安全的方法，使用时需要注意
     *
     * @param root           对象
     * @param compositeField 字段名
     * @param shouldJoin     是否应该联合查询
     * @return 字段的路径
     */
    @SuppressWarnings("unchecked")
    private static <T> Path<T> getPath(Root<?> root, String compositeField, boolean shouldJoin) {
        Path<?> path = root;
        try {
            for (String field : compositeField.split("\\.")) {
                field = field.trim();
                if (field.startsWith(CHAR_MULTI)) {
                    if (shouldJoin) {
                        path = ((Root<?>) path).join(field.substring(1));
                        isJointThreadLocal.set(true);
                    } else {
                        path = null;
                        break;
                    }
                } else {
                    path = path.get(field);
                }
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            path = null;
            // 记录路径不正确的错误
            logger.error("找不到对应的字段", e);
        }
        return (Path<T>) path;
    }

    /**
     * 判断查询条件值是否为空
     *
     * @param value 查询条件的值
     * @return true-空 false-非空
     */
    private static boolean isEmptyValue(Object value) {
        return value == null || "".equals(value);
    }

    /**
     * 查询条件封装
     */
    @AllArgsConstructor
    private static class ConditionWrapper implements Comparable<ConditionWrapper> {
        private String fieldName;
        private Object queryValue;
        private QueryCondition condition;

        /**
         * 排序规则：普通条件在前，Join条件在后，多个Join条件按照字母顺序排列<br/>
         * 普通条件不排序
         */
        @Override
        public int compareTo(ConditionWrapper that) {
            boolean thisIsJoin = false;
            boolean thatIsJoin = false;
            String thisJoinField = null;
            String thatJoinField = null;
            for (String field : this.condition.field()) {
                if (field.startsWith(CHAR_MULTI)) {
                    thisIsJoin = true;
                    thisJoinField = field;
                    break;
                }
            }
            for (String field : that.condition.field()) {
                if (field.startsWith(CHAR_MULTI)) {
                    thatIsJoin = true;
                    thatJoinField = field;
                    break;
                }
            }
            if (thisIsJoin ^ thatIsJoin) { // 不同
                return thisIsJoin ? 1 : -1;
            } else { // 相同
                if (thisJoinField == null) {
                    return 0;
                } else {
                    return thisJoinField.compareTo(thatJoinField);
                }
            }
        }
    }

    /**
     * 基本查询条件，注意里面的默认值需要和{@link QueryCondition}注解中保持一致
     */
    private static class SimpleCondition implements QueryCondition {

        public static final SimpleCondition INSTANCE = new SimpleCondition();

        private String[] field = new String[0];

        @Override
        public String[] targetField() {
            return field;
        }

        @Override
        public String[] field() {
            return field;
        }

        @Override
        public ConditionType type() {
            return ConditionType.EQUAL;
        }

        @Override
        public NullPolicy policy() {
            return NullPolicy.IGNORE;
        }

        @Override
        public String group() {
            return "";
        }

        @Override
        public DatePolicy datePolicy() {
            return DatePolicy.NONE;
        }

        @Override
        public boolean trim() {
            return true;
        }

        @Override
        public boolean ignoreCase() {
            return true;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return QueryCondition.class;
        }
    }
}
