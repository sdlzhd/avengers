package com.taocares.commons.jpa;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 查询条件注解，需要根据实际情况使用：
 * <ol>
 * <li>可以指定目标字段、条件类型和空值处理策略</li>
 * <li>默认匹配方式为严格相等，默认目标字段名即注解所在字段名</li>
 * </ol>
 *
 * @author Ankang
 * @date 2018/9/14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface QueryCondition {

    /**
     * 目标字段值<br/>
     * <ul>
     * <li>支持嵌套属性，如： "flightStage" / "flight.stage"</li>
     * <li>支持使用“+”表示多个属性连接，如："icao+flightNo"</li>
     * <li>如果目标字段为集合，在属性之前加“*”，如：“*routes.name"</li>
     * </ul>
     * 条件作用在多个字段时，关系为“或”，即多个字段有一个满足条件即可
     * @deprecated 推荐使用更简洁的 {@link #field()}
     */
    @AliasFor("field")
    @Deprecated
    String[] targetField() default {};

    /**
     * 目标字段值<br/>
     * <ul>
     * <li>支持嵌套属性，如： "flightStage" / "flight.stage"</li>
     * <li>支持使用“+”表示多个属性连接，如："icao+flightNo"</li>
     * <li>如果目标字段为集合，在属性之前加“*”，如：“*routes.name"</li>
     * </ul>
     * 条件作用在多个字段时，关系为“或”，即多个字段有一个满足条件即可
     */
    @AliasFor("targetField")
    String[] field() default {};

    /**
     * 比较条件类型
     */
    ConditionType type() default ConditionType.EQUAL;

    /**
     * 目标字段为NULL时的处理方式
     */
    NullPolicy policy() default NullPolicy.IGNORE;

    /**
     * 条件分组，同一分组下的条件关系为“或”，即满足一个即可<br/>
     */
    String group() default "";

    /**
     * 取指定时间单位的开始或结束，支持对{@link java.util.Date}或者{@link java.time.LocalDateTime}进行处理，支持年、月、日、时、分、秒<br/>
     * 例如：
     * <ul>
     * <li>datePolicy=DatePolicy.START_OF_DAY，条件中的时间会取当天的开始，即<br/>
     * 输入：2019-02-12 15:10:10，输出2019-02-12 00:00:00</li>
     * <li>datePolicy=DatePolicy.END_OF_MONTH，条件中的时间会取当月的结束，即<br/>
     * 输入：2019-02-12 15:10:10，输出2019-02-28 23:59:59
     * </li>
     * </ul>
     */
    DatePolicy datePolicy() default DatePolicy.NONE;

    /**
     * 字符串是否自动trim
     */
    boolean trim() default true;

    /**
     * 字符串是否忽略大小写
     */
    boolean ignoreCase() default true;
}
