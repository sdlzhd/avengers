package com.taocares.commons.jpa;

import java.lang.annotation.*;

/**
 * 查询对象注解，加此注解的类型表示其中所有字段均为查询条件，默认为相等
 *
 * @author Ankang
 * @date 2018/9/14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface QueryObject {
}
