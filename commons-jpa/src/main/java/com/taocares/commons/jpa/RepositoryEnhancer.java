package com.taocares.commons.jpa;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;

/**
 * 自定义查询接口
 *
 * @author Ankang
 * @date 2018/11/5
 */
public interface RepositoryEnhancer {

    /**
     * 获取查询条件
     *
     * @param queryString JPQL字符串
     * @return 查询条件实例
     */
    Query createQuery(String queryString);

    /**
     * 获取查询条件，并将结果映射到目标类中
     *
     * @param queryString JPQL字符串
     * @param resultClass 目标类
     * @param <T>         结果的类型
     * @return 查询条件实例
     */
    <T> TypedQuery<T> createQuery(String queryString, Class<T> resultClass);

    /**
     * 获取原生SQL查询条件
     *
     * @param queryString SQL字符串
     * @return SQL查询条件实例
     */
    Query createNativeQuery(String queryString);

    /**
     * 获取原生SQL查询条件，并将结果映射到目标类中
     *
     * @param queryString SQL字符串
     * @param resultClass 目标类
     * @return SQL查询条件实例
     */
    Query createNativeQuery(String queryString, Class resultClass);

    List findByJpql(String queryString);

    List findByJpql(String queryString, Pageable pageable);

    List findByJpql(String queryString, Map<String, Object> parameters);

    List findByJpql(String queryString, Map<String, Object> parameters, Pageable pageable);

    List findByJpql(String queryString, Object[] parameters);

    List findByJpql(String queryString, Object[] parameters, Pageable pageable);

    <T> List<T> findByJpql(String queryString, Class<T> resultClass);

    <T> List<T> findByJpql(String queryString, Class<T> resultClass, Pageable pageable);

    <T> List<T> findByJpql(String queryString, Map<String, Object> parameters, Class<T> resultClass);

    <T> List<T> findByJpql(String queryString, Map<String, Object> parameters, Class<T> resultClass, Pageable pageable);

    <T> List<T> findByJpql(String queryString, Object[] parameters, Class<T> resultClass);

    <T> List<T> findByJpql(String queryString, Object[] parameters, Class<T> resultClass, Pageable pageable);

    /**
     * 根据传入的Order对象获取SQL order by 子句<br/>
     * 示例输出：" order by t.foo ASC, t.bar DESC"
     *
     * @param entityClass 查询的实体类
     * @param sort        排序对象
     * @param aliasMap    实体类和别名的映射
     * @return order by 子句
     */
    String getOrderByFragment(Class<?> entityClass, Sort sort, Map<Class, String> aliasMap);

    /**
     * 获取实体特定属性对应的数据库列名
     *
     * @param entityClass  属性所在的实体类
     * @param propertyName 属性名（可以是嵌套的属性路径）
     * @return 数据库列名
     */
    String getColumnName(Class<?> entityClass, String propertyName);
}
