package com.taocares.commons.jpa;

/**
 * 字段为空的处理方式
 *
 * @author Ankang
 * @date 2018/9/17
 */
public enum NullPolicy {
    /**
     * 包含
     */
    INCLUDE,
    /**
     * 忽略
     */
    IGNORE
}
