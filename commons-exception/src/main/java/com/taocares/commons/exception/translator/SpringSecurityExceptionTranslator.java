package com.taocares.commons.exception.translator;

import com.taocares.commons.exception.ExceptionTranslator;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * Spring Security异常转换
 *
 * @author Ankang
 * @date 2018/11/16
 */
@Component
public class SpringSecurityExceptionTranslator implements ExceptionTranslator<AuthenticationException, RuntimeException> {
    @Override
    public RuntimeException translate(AuthenticationException e) {
        return e;
    }
}
