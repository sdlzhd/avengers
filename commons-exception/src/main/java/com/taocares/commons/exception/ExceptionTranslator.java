package com.taocares.commons.exception;

/**
 * 异常转换器接口
 *
 * @param <S> 原始的异常类型
 * @param <T> 目标的异常类型
 * @author Ankang
 * @date 2018/10/30
 */
public interface ExceptionTranslator<S extends Throwable, T extends Throwable> {

    T translate(S e);
}
