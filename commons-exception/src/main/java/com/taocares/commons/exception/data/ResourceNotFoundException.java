package com.taocares.commons.exception.data;

/**
 * 找不到指定的数据
 */
public class ResourceNotFoundException extends RuntimeException {
    private Object id;

    /**
     * @param entityName 实体名
     * @param id         主键
     */
    public ResourceNotFoundException(String entityName, Object id) {
        super("找不到ID为：" + id + "的" + entityName);
        this.id = id;
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(Throwable cause) {
        super(cause);
    }

    public Object getId() {
        return id;
    }
}
