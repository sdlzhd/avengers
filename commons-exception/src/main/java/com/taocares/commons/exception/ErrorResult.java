package com.taocares.commons.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 异常统一封装类
 *
 * @author Ankang
 * @date 2018/11/16
 */
@Data
@Builder
public class ErrorResult<T> {
    /**
     * 异常代码
     */
    @Builder.Default
    private int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
    /**
     * 异常信息
     */
    @Builder.Default
    private String message = "未知的服务器错误";
    /**
     * 异常可能携带的额外数据
     */
    private T data;
}
