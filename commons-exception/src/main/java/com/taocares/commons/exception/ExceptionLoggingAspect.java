package com.taocares.commons.exception;

import com.taocares.commons.lang.annotation.LogException;
import com.taocares.commons.util.AopUtils;
import com.taocares.commons.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 异常记录切面（在异常转换之前记录）
 *
 * @author Ankang
 * @date 2018/10/29
 */
@Slf4j
@Component
@Aspect
@Order(200)
public class ExceptionLoggingAspect {

    @Pointcut("within(com.taocares..*)")
    public void allPackages() {}

    @Pointcut("within(com.taocares..controller..*)")
    public void controllerPackages() {}

    @Pointcut("@annotation(com.taocares.commons.lang.annotation.LogException) || @within(com.taocares.commons.lang.annotation.LogException)")
    public void logException() {}

    @AfterThrowing(value = "logException() || controllerPackages()", throwing = "e")
    public void logException(JoinPoint joinPoint, Exception e) {
        log.error(StringUtils.formatStackTrace(e));
        Method method = AopUtils.findMethod(joinPoint);
        LogException annotation = AnnotationUtils.findAnnotation(method, LogException.class);
        if (annotation != null) {

        }
    }
}
