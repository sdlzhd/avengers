package com.taocares.commons.exception;

/**
 * 错误码
 *
 * @author Ankang
 * @date 2018/11/22
 */
public class ErrorCode {


    public static final int DATA_ERROR = 200;
    public static final int BIZ_ERROR = 300;

    /**
     * 认证授权相关的错误<br/>
     * <ol>
     *     <li>400：本地认证异常</li>
     *     <li>410：本地授权异常（待完善）</li>
     *     <li>420：OAuth认证异常</li>
     * </ol>
     *
     */
    public static final int AUTH_ERROR = 400;

    /**
     * 未认证，需要登录
     */
    public static final int AUTHENTICATION_REQUIRED = 40001;
    /**
     * 用户不存在
     */
    public static final int USER_NOT_EXITS = 40002;
    /**
     * 凭据（密码、验证码等）错误
     */
    public static final int BAD_CREDENTIALS = 40003;
    /**
     * 账户已禁用
     */
    public static final int ACCOUNT_DISABLED = 40011;
    /**
     * 账户已锁定
     */
    public static final int ACCOUNT_LOCKED = 40012;
    /**
     * 账户已过期
     */
    public static final int ACCOUNT_EXPIRED = 40013;
    /**
     * 凭据（密码、验证码等）已过期
     */
    public static final int CREDENTIALS_EXPIRED = 40014;

    /**
     * Oauth认证成功，但是需要进行用户绑定
     */
    public static final int OAUTH_BINDING_REQUIRED = 42001;

    public static final int OAUTH_INVALID_REQUEST = 42002;
    public static final int OAUTH_INVALID_CLIENT = 42003;
    public static final int OAUTH_INVALID_GRANT = 42004;
    public static final int OAUTH_UNAUTHORIZED_CLIENT = 42005;
    public static final int OAUTH_UNSUPPORTED_GRANT_TYPE = 42006;
    public static final int OAUTH_INVALID_SCOPE = 42007;
    public static final int OAUTH_INSUFFICIENT_SCOPE = 42008;
    public static final int OAUTH_INVALID_TOKEN = 42009;
    public static final int OAUTH_REDIRECT_URI_MISMATCH = 42010;
    public static final int OAUTH_UNSUPPORTED_RESPONSE_TYPE = 42011;
    public static final int OAUTH_ACCESS_DENIED = 42012;
    
    public static final int UNKNOWN_ERROR = 500;
}
