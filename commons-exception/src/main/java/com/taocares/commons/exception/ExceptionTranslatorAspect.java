package com.taocares.commons.exception;

import com.taocares.commons.exception.data.ResourceNotFoundException;
import com.taocares.commons.exception.translator.DataAccessExceptionTranslator;
import com.taocares.commons.exception.translator.SpringSecurityExceptionTranslator;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

/**
 * 异常转换切面
 *
 * @author Ankang
 * @date 2018/10/29
 */
@Slf4j
@Component
@Aspect
@Order(100)
public class ExceptionTranslatorAspect {

    @Autowired
    private DataAccessExceptionTranslator dataAccessExceptionTranslator;

    @Autowired
    private SpringSecurityExceptionTranslator springSecurityExceptionTranslator;

    @Pointcut("within(com.taocares..controller..*)")
    public void controllerPackages() {}

    @AfterThrowing(value = "controllerPackages()", throwing = "e")
    public void convertControllerException(Exception e) {
        // 不记录日志，交给ExceptionLoggingAspect
//        log.error(StringUtils.formatStackTrace(e));
        if (e instanceof EntityNotFoundException) {
            // Unable to find com.taocares.providerdemo.entity.Student with id 16
            throw new ResourceNotFoundException(e);
        } else if (e instanceof DataAccessException) {
            // TODO handle data exception
            throw dataAccessExceptionTranslator.translate((DataAccessException) e);
        } else if (e instanceof AuthenticationException) {
            throw springSecurityExceptionTranslator.translate((AuthenticationException) e);
        }
    }
}
