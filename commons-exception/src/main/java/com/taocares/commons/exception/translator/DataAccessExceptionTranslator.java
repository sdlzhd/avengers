package com.taocares.commons.exception.translator;

import com.taocares.commons.exception.ExceptionTranslator;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

/**
 * 数据访问异常转换器
 *
 * @author Ankang
 * @date 2018/10/30
 */
@Component
public class DataAccessExceptionTranslator implements ExceptionTranslator<DataAccessException, RuntimeException> {

    @Override
    public RuntimeException translate(DataAccessException e) {
        return e;
    }
}
