package com.taocares.commons.exception;

import com.taocares.commons.exception.data.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentConversionNotSupportedException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public ErrorResult handleResourceNotFoundException(ResourceNotFoundException e) {
        return ErrorResult.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .message(e.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AuthenticationException.class)
    public ErrorResult handleAuthenticationException(AuthenticationException e) {
        // spring security异常不做处理
        throw e;
//        return ErrorResult.builder()
//                .code(HttpStatus.UNAUTHORIZED.value())
//                .message(getMessage(e, false))
//                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorResult handleConstraintViolationException(ConstraintViolationException e) {
        StringBuilder sb = new StringBuilder("数据校验失败！\n");
        for (ConstraintViolation<?> constraintViolation : e.getConstraintViolations()) {
            sb.append(constraintViolation.getMessage());
            sb.append("\n");
        }
        return ErrorResult.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(sb.toString())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ErrorResult handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return ErrorResult.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message("无法读取HTTP消息！详细信息：\n" + e.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ErrorResult handleBindException(BindException e) {
        StringBuilder sb = new StringBuilder("参数绑定失败！\n");
        if (e.hasGlobalErrors()) {
            for (ObjectError objectError : e.getGlobalErrors()) {
                sb.append("对象：").append(objectError.getObjectName());
                sb.append("\n");
            }
        }
        if (e.hasFieldErrors()) {
            for (FieldError fieldError : e.getFieldErrors()) {
                sb.append("属性：").append(fieldError.getField())
                        .append("；值：").append(fieldError.getRejectedValue());
                sb.append("\n");
            }
        }
        return ErrorResult.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(sb.toString())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ServletRequestBindingException.class,
            MethodArgumentNotValidException.class,
            MethodArgumentTypeMismatchException.class,
            MethodArgumentConversionNotSupportedException.class})
    public ErrorResult handleServletRequestBindingException(Exception e) {
        return ErrorResult.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message("请求参数错误！详细信息：\n" + e.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BizException.class)
    public ErrorResult handleBizException(BizException e) {
        return ErrorResult.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(e.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public ErrorResult handleRuntimeException(RuntimeException e) {
        // 不处理spring security相关的异常
        if (e.getClass().getCanonicalName().startsWith("org.springframework.security")) {
            throw e;
        }
        return ErrorResult.builder()
                .message(getMessage(e, true))
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResult handleException(Exception e) {
        return ErrorResult.builder()
                .message(getMessage(e, true))
                .build();
    }

    /**
     * 临时使用
     *
     * @return 错误信息
     */
    private String getMessage(Exception e, boolean force) {
        return e.getMessage();
        /*if (force) {
            return e.getMessage() + " [以下为临时信息，生产会移除] 请联系安康处理此异常：" + e.getClass();
        } else {
            return e.getMessage() + " [以下为临时信息，生产会移除] 如果此异常处理（错误代码/错误信息）不符合预期，请联系安康处理：" + e.getClass();
        }*/
    }
}
