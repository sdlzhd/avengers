package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.data.Item;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/25
 */
public class ItemsToStringConverter extends AbstractToStringConverter {

    @Override
    protected String itemToString(Object item) {
        return ((Item) item).getName();
    }

    @Override
    protected String getDelimiter() {
        return "; ";
    }
}
