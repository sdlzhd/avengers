package com.taocares.commons.beans;

import com.taocares.commons.beans.data.EmbeddedData;
import com.taocares.commons.beans.data.Item;
import com.taocares.commons.beans.data.TestDto;
import com.taocares.commons.beans.data.TestEntity;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.Assert.*;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
public class BeanUtilsTest {

    @org.testng.annotations.Test
    public void testCopyProperties() {
        EmbeddedData data = new EmbeddedData();
        data.setContent("embedded-data");
        TestEntity testEntity = new TestEntity();
        testEntity.setData(data);
        testEntity.setId(1L);
        testEntity.setName("test");
        testEntity.setAge(8L);
        testEntity.setFlag(false);
        testEntity.setItems(Arrays.asList(new Item(1L, "a"), new Item(2L, "b"), new Item(3L, "c")));
        TestDto testDto = new TestDto();
//        org.springframework.beans.BeanUtils.copyProperties(testEntity, testDto);
        BeanUtils.copyProperties(testEntity, testDto);
        testDto.getData().getDate().setTime(0);
        System.err.println(testEntity);
        System.err.println(testDto);
        assertEquals(testDto.getId(), testEntity.getId());
        assertEquals(testDto.getEntityName(), testEntity.getName());
        assertEquals(testDto.getAge(), testEntity.getAge().intValue());
        assertEquals(testDto.getCreateTime(), testEntity.getCreateTime());
        // 反向拷贝
        TestEntity copiedEntity = BeanUtils.copyProperties(testDto, TestEntity.class);
        System.err.println(copiedEntity);
        // 日期经过格式化/解析 无法恢复原值
//        assertEquals(testEntity, copiedEntity);
        assertEquals(testEntity.getId(), copiedEntity.getId());
        assertEquals(testEntity.getName(), copiedEntity.getName());
        assertEquals(testEntity.getAge(), copiedEntity.getAge());
        assertEquals(testEntity.getData(), copiedEntity.getData());
        assertEquals(testDto.getData().getDate(), copiedEntity.getData().getDate());
    }

    @Test
    public void testListCopy() {
        TestEntity entity = new TestEntity();
        entity.setName("ceshi");
        entity.setId(1L);
        entity.setAge(10L);
        entity.setData(new EmbeddedData());
        List<TestEntity> list = new ArrayList<>();
        list.add(entity);
        List<TestDto> result1 = BeanUtils.copyProperties(list, TestDto.class);
        assertEquals(1, result1.size());

        Set<TestEntity> set = new HashSet<>();
        set.add(entity);
        Set<TestDto> result2 = BeanUtils.copyProperties(set, TestDto.class);
        assertEquals(1, result2.size());
    }

    @Test
    public void testMapCopy() {
        TestEntity entity = new TestEntity();
        entity.setName("ceshi");
        entity.setId(8L);
        entity.setAge(10L);
        entity.setData(new EmbeddedData());
        Map map = new HashMap();
        BeanUtils.copyProperties(entity, map, false);
        assertFalse(map.containsKey("flag"));
        BeanUtils.copyProperties(entity, map, true);
        assertTrue(map.containsKey("flag"));
        System.err.println(map);
        TestEntity entity1 = BeanUtils.copyProperties(map, TestEntity.class);
        assertEquals(entity, entity1);
    }
}