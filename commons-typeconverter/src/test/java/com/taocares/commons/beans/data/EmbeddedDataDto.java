package com.taocares.commons.beans.data;

import com.taocares.commons.beans.annotation.Mapping;
import com.taocares.commons.beans.converter.StringPrefixConverter;
import com.taocares.commons.beans.converter.StringPrefixRemoveConverter;
import lombok.Data;

import java.util.Date;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
@Data
public class EmbeddedDataDto {
    @Mapping(converter = StringPrefixConverter.class, inverseConverter = StringPrefixRemoveConverter.class)
    private String content;
    private Date date;
}
