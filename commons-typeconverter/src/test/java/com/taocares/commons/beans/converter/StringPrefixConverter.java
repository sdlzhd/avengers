package com.taocares.commons.beans.converter;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/30
 */
public class StringPrefixConverter extends AbstractToStringConverter {
    @Override
    protected String itemToString(Object item) {
        return "prefix-" + item;
    }
}
