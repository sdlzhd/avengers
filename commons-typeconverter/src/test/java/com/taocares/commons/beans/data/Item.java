package com.taocares.commons.beans.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    private Long id;
    private String name;
}
