package com.taocares.commons.beans.data;

import com.taocares.commons.util.DateUtils;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
@Data
public class Base {

    private Date createTime = new Date();

    private LocalDateTime createTimeJava8 = DateUtils.toLocalDateTime(createTime);
}
