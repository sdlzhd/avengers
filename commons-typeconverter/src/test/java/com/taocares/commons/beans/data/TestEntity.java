package com.taocares.commons.beans.data;

import lombok.Data;

import java.util.List;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
@Data
public class TestEntity extends Base {

    private Long id;
    private String name;
    private Long age;
    private EmbeddedData data;

    private Boolean flag;

    private List<Item> items;

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", data=" + data +
                ", flag=" + flag +
                ", items=" + items +
                '}' + super.toString();
    }
}
