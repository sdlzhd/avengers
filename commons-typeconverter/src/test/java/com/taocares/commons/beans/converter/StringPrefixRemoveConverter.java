package com.taocares.commons.beans.converter;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/30
 */
public class StringPrefixRemoveConverter extends AbstractToStringConverter {
    @Override
    protected String itemToString(Object item) {
        String itemString = (String) item;
        if (itemString.indexOf("prefix-") == 0) {
            itemString = itemString.substring(7);
        }
        return itemString;
    }
}
