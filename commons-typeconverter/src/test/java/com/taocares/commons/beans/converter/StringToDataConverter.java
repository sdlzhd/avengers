package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.AbstractTypeConverter;
import com.taocares.commons.beans.data.EmbeddedData;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/24
 */
public class StringToDataConverter extends AbstractTypeConverter {

    @Override
    protected Object doConvert(Object origin) {
        EmbeddedData data = new EmbeddedData();
        String dataString = ((String) origin);
        dataString = dataString.substring(dataString.indexOf('=') + 1, dataString.indexOf(')'));
        data.setContent(dataString);
        return data;
    }
}
