package com.taocares.commons.beans.data;

import com.taocares.commons.beans.annotation.Mapping;
import com.taocares.commons.beans.annotation.Nested;
import com.taocares.commons.beans.converter.*;
import com.taocares.commons.beans.converter.internal.ObjectToStringConverter;
import lombok.Data;

import java.util.Date;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
@Data
public class TestDto {

    private Long id;
    @Mapping(field = "name")
    private String entityName;
    private int age;
//    @Mapping(ignore = true)
    @Mapping(field = "data", converter = {ObjectToStringConverter.class, StringPrefixConverter.class}, inverseConverter = StringToDataConverter.class, oneway = true)
    private String dataString;

    @Nested(thisClass = EmbeddedDataDto.class, thatClass = EmbeddedData.class)
    private EmbeddedDataDto data;
    @Mapping(field = "data.content")
    private String dataContent;

//    @Mapping(field = "createTime", converter = DefaultDateConverter.class, inverseConverter = DefaultDateConverter.class)
    @Mapping(field = "createTime", datePattern = "yyyy-MM-dd")
    private String date;

    @Mapping(field = "createTimeJava8", datePattern = "yyyy-MM-dd")
    private Date createTime;

    @Mapping(field="flag", converter = YesOrNoConverter.class, oneway = true)
    private String flagCn;

    @Mapping(converter = DictionaryConverter.class, inverseConverter = DictionaryConverter.class)
    private String flag;

    @Mapping(field = "items", converter = ItemsToStringConverter.class, oneway = true)
    private String itemNames;

    @Override
    public String toString() {
        return "TestDto{" +
                "id=" + id +
                ", entityName='" + entityName + '\'' +
                ", age=" + age +
                ", dataString='" + dataString + '\'' +
                ", data=" + data +
                ", dataContent='" + dataContent + '\'' +
                ", date='" + date + '\'' +
                ", createTime='" + createTime + '\'' +
                ", flagCn='" + flagCn + '\'' +
                ", flag='" + flag + '\'' +
                ", itemNames='" + itemNames + '\'' +
                '}';
    }
}
