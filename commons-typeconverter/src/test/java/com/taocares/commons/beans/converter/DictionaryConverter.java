package com.taocares.commons.beans.converter;

import java.util.HashMap;
import java.util.Map;

public class DictionaryConverter extends AbstractDictionaryConverter {

    @Override
    protected Map<Object, Object> getDictionary() {
        Map<Object, Object> dictionary = new HashMap<>();
        dictionary.put(false, "否");
        dictionary.put(true, "是");
        return dictionary;
    }
}
