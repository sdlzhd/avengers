package com.taocares.commons.beans.data;

import lombok.Data;

import java.util.Date;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
@Data
public class EmbeddedData {
    private String content;
    private Date date = new Date();
}
