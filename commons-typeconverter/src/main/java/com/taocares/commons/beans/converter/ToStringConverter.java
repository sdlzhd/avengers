package com.taocares.commons.beans.converter;

/**
 * 集合转换为字符串（元素调用toString之后，用逗号分隔）
 *
 * @author Ankang
 * @date 2018/9/24
 */
public class ToStringConverter extends AbstractToStringConverter {

    @Override
    protected String itemToString(Object item) {
        return item.toString();
    }
}
