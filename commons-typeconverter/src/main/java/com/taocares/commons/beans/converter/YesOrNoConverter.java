package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * 数据库中的1/0，实体中的true/false转换为：是/否
 *
 * @author Ankang
 * @date 2018/9/25
 */
public class YesOrNoConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        if (origin instanceof Boolean) {
            return ((boolean) origin) ? "是" : "否";
        } else if (origin instanceof Number) {
            return ((Number) origin).intValue() != 0 ?
                    "是" : "否";
        }
        return origin;
    }
}
