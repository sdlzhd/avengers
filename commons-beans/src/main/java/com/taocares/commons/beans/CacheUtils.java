package com.taocares.commons.beans;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.taocares.commons.beans.caffeine.CaffeineCacheManager;
import com.taocares.commons.beans.caffeine.CaffeineConfig;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @description: 缓存工具类，用于创建缓存、获取缓存数据以及添加缓存数据
 * @author: yunpeng liu
 * @create: 2018-12-20 15:28
 **/
public class CacheUtils {
    /**
     * 将结果进行缓存
     *
     * @param struct 缓存配置
     * @param key 缓存的key
     * @param value 缓存的value
     */
    static void cachePut(CaffeineConfig struct, Object key, Object value) {
        //根据cacheName获取cache。
        // 若存在则直接获取，若不存在则根据struct新添加一个cache
        Cache cache = CaffeineCacheManager.getInstance().getCache(struct.getCacheName());
        if (null == cache) {
            cache = Caffeine.newBuilder()
                    .maximumSize(struct.getMaximumSize())
                    .expireAfterWrite(struct.getExpireAfterWriteMinutes(), TimeUnit.MINUTES)
                    .expireAfterAccess(struct.getExpireAfterAccessMinutes(), TimeUnit.MINUTES)
                    .build();

            CaffeineCacheManager.getInstance().addCache(struct.getCacheName(), cache);
        }

        //创建新元素并置入缓存
        cache.put(key, value);
    }

    /**
     * 获取缓存结果
     *
     * @param cacheName 缓存名
     * @param key 缓存key
     * @return 缓存value
     */
    static Object cacheGet(String cacheName, Object key){
        try {
            Cache cache = CaffeineCacheManager.getInstance().getCache(cacheName);
            return Objects.requireNonNull(cache).getIfPresent(key);
        } catch (Exception e) {
            return null;
        }

    }
}
