package com.taocares.commons.beans.resolver;

import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.proxy.HibernateProxy;

import java.util.Collection;
import java.util.Optional;

/**
 * Hibernate代理对象处理类
 *
 * @author Ankang
 * @date 2018/10/11
 */
public class HibernateProxyResolver {

    /**
     * 判断传入的对象是否为代理对象
     *
     * @param object 参数
     * @return 是否为代理对象
     */
    public static boolean isProxy(Object object) {
        return object instanceof HibernateProxy
                || object instanceof PersistentCollection;
    }

    /**
     * 获取对象的真实类型（被代理的对象类型或者集合中元素的类型）
     *
     * @param object 原始对象/集合
     * @return 真实对象的类型
     */
    public static Class getProxiedClass(Object object) {
        if (object == null) return Void.class;

        if (object instanceof HibernateProxy) {
            return ((HibernateProxy) object).getHibernateLazyInitializer().getPersistentClass();
        } else if (object instanceof Collection) {
            Optional<?> optional = ((Collection) object).stream().findFirst();
            if (optional.isPresent()) {
                return optional.get().getClass();
            }
        }
        // 空集合直接返回集合类型即可
        return object.getClass();
    }

    /**
     * 从代理对象中获取真实对象（如果不是代理对象，直接返回）
     *
     * @param proxyObject 代理对象
     * @return 真实对象
     */
    public static Object getRealObject(Object proxyObject) {
        if (proxyObject instanceof HibernateProxy) {
            return ((HibernateProxy) proxyObject).getHibernateLazyInitializer().getImplementation();
        }
        return proxyObject;
    }
}
