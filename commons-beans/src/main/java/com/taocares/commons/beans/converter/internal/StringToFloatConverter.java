package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * String转Float
 *
 * @author Ankang
 * @date 2018/9/26
 */
public class StringToFloatConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return Float.valueOf((String) origin);
    }
}
