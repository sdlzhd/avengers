package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * String转Boolean
 *
 * @author Ankang
 * @date 2018/9/26
 */
public class StringToBooleanConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return Boolean.valueOf((String) origin);
    }
}
