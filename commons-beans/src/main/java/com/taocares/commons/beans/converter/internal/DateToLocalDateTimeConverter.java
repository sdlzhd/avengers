package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;
import com.taocares.commons.util.DateUtils;

import java.util.Date;

/**
 * Date转LocalDateTime
 *
 * @author Ankang
 * @date 2018/9/27
 */
public class DateToLocalDateTimeConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return DateUtils.toLocalDateTime((Date) origin);
    }
}
