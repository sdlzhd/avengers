package com.taocares.commons.beans;

import com.taocares.commons.util.ReflectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Spring上下文工具，
 * <b>注意：应用需要扫描此类才能完成上下文的注入</b>
 *
 * @author Ankang
 * @date 2018/9/25
 */
@Component
public class SpringContextUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    /**
     * 缓存已经实例化的类
     */
    private static ConcurrentMap<Class<?>, Object> instances = new ConcurrentHashMap<>();

    /**
     * 根据指定类型获取实例，优先扫描Spring上下文，<br/>
     * 如果无法加载上下文或者无法找到类的定义，会通过反射获取实例
     *
     * @param cls 类
     * @param <T> 类的类型
     * @return 类的实例
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<?> cls) {
        Object instance = null;
        // 尝试通过Spring加载
        if (applicationContext != null) {
            try {
                instance = applicationContext.getBean(cls);
            } catch (BeansException e) {
                // 不做处理
            }
        }
        // 尝试从缓存中获取
        if (instance == null) {
            instance = instances.get(cls);
        }
        // 通过反射生成实例
        if (instance == null) {
            instance = ReflectionUtils.newInstance(cls);
            instances.putIfAbsent(cls, instance);
        }
        return (T) instance;
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }
}
