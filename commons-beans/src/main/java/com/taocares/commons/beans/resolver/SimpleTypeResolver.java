package com.taocares.commons.beans.resolver;

import com.taocares.commons.beans.SpringContextUtils;
import com.taocares.commons.beans.TypeConverter;
import com.taocares.commons.beans.converter.DoNothingConverter;
import com.taocares.commons.beans.converter.internal.*;
import com.taocares.commons.lang.TypeConvertException;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

/**
 * 简单类型自动转换工具
 *
 * @author Ankang
 * @date 2018/9/30
 */
public class SimpleTypeResolver {

    /**
     * 对常见类型进行自动的类型转换，目前支持的自动转换：
     * <ol>
     * <li>Java8 Date和Date互相转换</li>
     * <li>字符串转换为数值/布尔值</li>
     * <li>数值转换为数值/布尔值</li>
     * <li>任意对象转换为字符串</li>
     * </ol>
     *
     * @param value        原始的值
     * @param expectedType 期望的类型
     * @return 转换之后的值
     * @throws TypeConvertException 类型转换异常
     */
    public static Object convert(@NotNull Object value, @NotNull Class<?> expectedType) {
        Class<?> valueType = value.getClass();
        // 类型相同直接返回
        if (valueType == expectedType) {
            return value;
        }
        Class<? extends TypeConverter> converterClass = DoNothingConverter.class;
        if (expectedType == LocalDate.class && Date.class.isAssignableFrom(valueType)) {
            converterClass = DateToLocalDateConverter.class;
        } else if (expectedType == LocalTime.class && Date.class.isAssignableFrom(valueType)) {
            converterClass = DateToLocalTimeConverter.class;
        } else if (expectedType == LocalDateTime.class && Date.class.isAssignableFrom(valueType)) {
            converterClass = DateToLocalDateTimeConverter.class;
        } else if (expectedType == Date.class) {
            if (valueType == LocalDate.class) {
                converterClass = LocalDateToDateConverter.class;
            } else if (valueType == LocalTime.class) {
                converterClass = LocalTimeToDateConverter.class;
            } else if (valueType == LocalDateTime.class) {
                converterClass = LocalDateTimeToDateConverter.class;
            }
        } else if (expectedType == Boolean.class || expectedType == boolean.class) {
            if (valueType == String.class) {
                converterClass = StringToBooleanConverter.class;
            } else if (Number.class.isAssignableFrom(valueType)) {
                converterClass = NumberToBooleanConverter.class;
            }
        } else if (expectedType == Integer.class || expectedType == int.class) {
            if (valueType == String.class) {
                converterClass = StringToIntegerConverter.class;
            } else if (Number.class.isAssignableFrom(valueType)) {
                converterClass = NumberToIntegerConverter.class;
            }
        } else if (expectedType == Long.class || expectedType == long.class) {
            if (valueType == String.class) {
                converterClass = StringToLongConverter.class;
            } else if (Number.class.isAssignableFrom(valueType)) {
                converterClass = NumberToLongConverter.class;
            }
        } else if (expectedType == Float.class || expectedType == float.class) {
            if (valueType == String.class) {
                converterClass = StringToFloatConverter.class;
            } else if (Number.class.isAssignableFrom(valueType)) {
                converterClass = NumberToFloatConverter.class;
            }
        } else if (expectedType == Double.class || expectedType == double.class) {
            if (valueType == String.class) {
                converterClass = StringToDoubleConverter.class;
            } else if (Number.class.isAssignableFrom(valueType)) {
                converterClass = NumberToDoubleConverter.class;
            }
        } else if (expectedType == String.class) {
            converterClass = ObjectToStringConverter.class;
        }
        TypeConverter converter = SpringContextUtils.getBean(converterClass);
        return converter.convert(value);
    }
}
