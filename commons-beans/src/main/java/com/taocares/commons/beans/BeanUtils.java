package com.taocares.commons.beans;

import com.taocares.commons.beans.annotation.Mapping;
import com.taocares.commons.beans.annotation.Nested;
import com.taocares.commons.beans.exception.BeanPropertyCopyException;
import com.taocares.commons.beans.resolver.DateResolver;
import com.taocares.commons.beans.resolver.HibernateProxyResolver;
import com.taocares.commons.beans.resolver.SimpleTypeResolver;
import com.taocares.commons.lang.Nullable;
import com.taocares.commons.lang.TypeConvertException;
import com.taocares.commons.util.CollectionUtils;
import com.taocares.commons.util.MaskUtils;
import com.taocares.commons.util.ReflectionUtils;
import com.taocares.commons.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.InvalidPropertyException;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.DateTimeException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 对象操作工具类
 *
 * @author Ankang
 * @date 2018/9/19
 */
public class BeanUtils {

    private static final Logger logger = LoggerFactory.getLogger(BeanUtils.class);

    /**
     * 标记嵌套的实体处理，避免循环依赖
     */
    private static ThreadLocal<Set<Class>> nestedClasses = ThreadLocal.withInitial(LinkedHashSet::new);

    /**
     * 从源对象列表拷贝生成目标类的列表
     *
     * @param sourceList  源对象列表
     * @param targetClass 目标类
     * @param <T>         目标类的类型
     * @return 目标类的列表
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    public static <T> List<T> copyProperties(@NotNull List sourceList, @NotNull Class<T> targetClass) {
        List<T> result = new ArrayList<>();
        for (Object source : sourceList) {
            result.add(copyProperties(source, targetClass));
        }
        return result;
    }

    /**
     * 从源对象集合拷贝生成目标类的集合
     *
     * @param sourceSet   源对象集合
     * @param targetClass 目标类
     * @param <T>         目标类的类型
     * @return 目标类的集合
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    public static <T> Set<T> copyProperties(@NotNull Set sourceSet, @NotNull Class<T> targetClass) {
        Set<T> result = new HashSet<>();
        for (Object source : sourceSet) {
            result.add(copyProperties(source, targetClass));
        }
        return result;
    }

    /**
     * 从源对象拷贝生成目标类的实例，循环嵌套的对象会被忽略
     *
     * @param source      源对象
     * @param targetClass 目标类
     * @param <T>         目标类的类型
     * @return 目标类的实例
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    public static <T> T copyProperties(@NotNull Object source, @NotNull Class<T> targetClass) {
        T target = ReflectionUtils.newInstance(targetClass);
        if (target != null) {
            copyProperties(source, target);
        }
        return target;
    }

    /**
     * 在两个对象间复制属性，循环嵌套的对象会被忽略<br/>
     * 支持Map和Bean的互相拷贝（仅基本拷贝，不支持类型转换和映射关系）
     *
     * @param source 源对象
     * @param target 目标对象
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    public static void copyProperties(@NotNull Object source, @NotNull Object target) {
        copyProperties(source, target, true);
    }

    /**
     * 在两个对象间复制属性，循环嵌套的对象会被忽略，可以指定是否拷贝null到目标对象<br/>
     * 支持Map和Bean的互相拷贝（仅基本拷贝，不支持类型转换和映射关系）
     *
     * @param source  源对象
     * @param target  目标对象
     * @param mapNull 是否拷贝null
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    @SuppressWarnings("unchecked")
    public static void copyProperties(@NotNull final Object source, @NotNull final Object target, final boolean mapNull) {
        if (source instanceof Map) {
            copyPropertiesFromMap((Map) source, target);
        } else if (target instanceof Map) {
            copyPropertiesToMap(source, (Map) target, mapNull);
        } else {
            copyBeanProperties(source, target, mapNull);
        }
    }

    /**
     * Map拷贝到Bean，不支持复杂映射及类型转换
     *
     * @param source 源Map
     * @param target 目标Bean
     */
    private static void copyPropertiesFromMap(Map<String, Object> source, Object target) {
        BeanWrapper targetWrapper = new BeanWrapperImpl(target);
        for (Map.Entry<String, Object> entry : source.entrySet()) {
            if (targetWrapper.isWritableProperty(entry.getKey())) {
                targetWrapper.setPropertyValue(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * Bean拷贝到Map，不支持复杂映射及类型转换
     *
     * @param source  源Bean
     * @param target  目标Map
     * @param mapNull 是否拷贝null
     */
    private static void copyPropertiesToMap(Object source, Map<String, Object> target, boolean mapNull) {
        BeanWrapper sourceWrapper = new BeanWrapperImpl(source);
        for (String propertyName : BeanPropertyUtils.getAllPropNames(source)) {
            Object propertyValue = sourceWrapper.getPropertyValue(propertyName);
            if (mapNull || propertyValue != null) {
                target.put(propertyName, propertyValue);
            }
        }
    }

    /**
     * Bean之间的属性拷贝
     *
     * @param source  源Bean
     * @param target  目标Bean
     * @param mapNull 是否拷贝null
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    private static void copyBeanProperties(final Object source, final Object target, boolean mapNull) {
        List<MappingConfig> configList = MappingConfigManager.getMappingConfig(source.getClass(), target.getClass());
        BeanWrapper sourceWrapper = new BeanWrapperImpl(source);
        BeanWrapper targetWrapper = new BeanWrapperImpl(target);
        nestedClasses.get().add(source.getClass());
        for (String fieldName : BeanPropertyUtils.getAllPropNames(target)) {
            MappingConfig config = getConfigForField(fieldName, configList);
            List<MappingConfig> deepConfigList = null;
            if (config == null) {
                deepConfigList = getDeepConfigForField(fieldName, configList);
            }
            if (CollectionUtils.isEmpty(deepConfigList)) {
                doCopyProperty(sourceWrapper, targetWrapper, fieldName, config, mapNull);
            } else {
                boolean notEmpty = false;
                Class<?> fieldType = targetWrapper.getPropertyDescriptor(fieldName).getPropertyType();
                Object fieldObject = ReflectionUtils.newInstance(fieldType);
                BeanWrapper fieldWrapper = new BeanWrapperImpl(fieldObject);
                for (MappingConfig deepConfig : deepConfigList) {
                    // 获取第一个"."之后的属性值
                    String innerFieldName = deepConfig.targetFieldName
                            .substring(deepConfig.targetFieldName.indexOf(".") + 1);
                    if (fieldWrapper.isReadableProperty(innerFieldName)
                            && fieldWrapper.isWritableProperty(innerFieldName)) {
                        notEmpty = doCopyProperty(sourceWrapper, fieldWrapper, innerFieldName, deepConfig, mapNull) || notEmpty;
                    }
                }
                if (notEmpty) {
                    targetWrapper.setPropertyValue(fieldName, fieldObject);
                } else if (mapNull) {
                    targetWrapper.setPropertyValue(fieldName, null);
                }
            }

        }
        nestedClasses.get().remove(source.getClass());
    }

    /**
     * 对特定字段执行数据拷贝
     *
     * @param sourceWrapper   源对象
     * @param targetWrapper   目标对象
     * @param targetFieldName 目标字段名
     * @param config          映射配置
     * @param mapNull         是否映射空值
     * @return 值是否拷贝成功（空值视为失败）
     * @throws BeanPropertyCopyException 属性拷贝发生异常
     */
    private static boolean doCopyProperty(final BeanWrapper sourceWrapper, final BeanWrapper targetWrapper, String targetFieldName, MappingConfig config, boolean mapNull) {
        boolean isMapNull = mapNull;
        String sourceFieldName = targetFieldName;
        Class<?> nestedSourceClass = null;
        Class<?> nestedTargetClass = null;
        String datePattern = null;
        String mask = null;
        String getter = null;
        String setter = null;
        List<TypeConverter> converters = new ArrayList<>();
        if (config != null) {
            // 跳过忽略的属性
            if (config.ignore) {
                return false;
            }
            isMapNull = config.mapNull && mapNull;
            sourceFieldName = config.sourceFieldName;
            nestedSourceClass = config.nestedSourceClass;
            nestedTargetClass = config.nestedTargetClass;
            datePattern = config.datePattern;
            mask = config.mask;
            getter = config.getter;
            setter = config.setter;
            if (config.converterClass != null) {
                for (Class<? extends TypeConverter> cls : config.converterClass) {
                    converters.add(SpringContextUtils.getBean(cls));
                }
            }
        }
        try {
            Object value;
            if (StringUtils.isEmpty(getter)) {
                value = getSourceValue(sourceWrapper, sourceFieldName);
            } else {
                value = invokeMethodByName(sourceWrapper, getter, null, null);
            }

            for (TypeConverter converter : converters) {
                value = converter.convert(value);
            }
            if (value != null || isMapNull) {
                copyValueIgnoringNestedFields(targetWrapper, targetFieldName, value, nestedSourceClass, nestedTargetClass, datePattern, mask, setter);
            }
            return value != null;
        } catch (InvalidPropertyException e) {
            // 直接忽略，无需处理
            return false;
        }
    }

    /**
     * 从源对象中取出对应字段的值
     *
     * @param sourceWrapper 源对象
     * @param fieldName     字段名
     * @return 值对象
     * @throws InvalidPropertyException 属性不存在
     */
    private static Object getSourceValue(final BeanWrapper sourceWrapper, String fieldName) throws InvalidPropertyException {
        // 如果映射字段为“this”，直接返回对象
        if ("this".equals(fieldName)) {
            return sourceWrapper.getWrappedInstance();
        } else {
            Object sourceValue = sourceWrapper.getPropertyValue(fieldName);
            Class sourceClass = HibernateProxyResolver.getProxiedClass(sourceValue);
            // 嵌套的实体直接返回null
            return isNested(sourceClass) ? null : sourceValue;
        }
    }

    /**
     * 根据拷贝路径判断是否是循环嵌套<br/>树形数据（自身引用自身）不算循环嵌套
     *
     * @param cls 待判断的类型
     * @return true-是 false-不是
     */
    private static boolean isNested(Class cls) {
        Set<Class> classes = nestedClasses.get();
        if (classes.contains(cls)) {
            Class lastClass = null;
            for (Class aClass : classes) {
                lastClass = aClass;
            }
            return lastClass != cls;
        }
        return false;
    }

    /**
     * 忽略值对象中的嵌套字段对目标赋值
     *
     * @param targetWrapper     目标对象
     * @param targetFieldName   目标字段名
     * @param value             值对象
     * @param nestedSourceClass 嵌套的源对象类型
     * @param nestedTargetClass 嵌套的目标对象类型
     * @param datePattern       日期格式
     * @param mask              字符模板
     * @throws DateTimeException    时间解析异常
     * @throws TypeConvertException 类型转换异常
     */
    private static void copyValueIgnoringNestedFields(BeanWrapper targetWrapper, String targetFieldName, Object value, Class<?> nestedSourceClass, Class<?> nestedTargetClass, String datePattern, String mask, String setter) {
        if (value != null) {
            // 嵌套对象的拷贝设置nested为true，不再处理Hibernate关联对象
            if (nestedSourceClass != null) {
                nestedClasses.get().add(nestedSourceClass);
                if (value instanceof List) {
                    value = copyProperties((List) value, nestedTargetClass);
                } else if (value instanceof Set) {
                    value = copyProperties((Set) value, nestedTargetClass);
                } else {
                    value = copyProperties(value, nestedTargetClass);
                }
                nestedClasses.get().remove(nestedSourceClass);
            }
            Class<?> targetFieldType = targetWrapper.getPropertyType(targetFieldName);
            value = DateResolver.convert(value, datePattern, targetFieldType);
            value = SimpleTypeResolver.convert(value, targetFieldType);
            if (StringUtils.isNotEmpty(mask) && value instanceof String) {
                value = MaskUtils.mask((String) value, mask);
            }
        }
        if (StringUtils.isNotEmpty(setter) && value != null) {
            invokeMethodByName(targetWrapper, setter, value.getClass(), value);
        } else {
            targetWrapper.setPropertyValue(targetFieldName, value);
        }
    }

    /**
     * 调用指定的方法，并获取返回值
     *
     * @param beanWrapper 目标类
     * @param method      方法名
     * @return 被调用方法的返回值
     */
    private static Object invokeMethodByName(BeanWrapper beanWrapper, String method, Class argClass, Object value) {
        try {
            if (argClass != null) {
                beanWrapper.getWrappedClass().getMethod(method, argClass).invoke(beanWrapper.getWrappedInstance(), value);
            } else {
                return beanWrapper.getWrappedClass().getMethod(method).invoke(beanWrapper.getWrappedInstance());
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            // 指定方法不存在时，忽略
        }
        return null;
    }

    /**
     * 获取适用于目标字段的映射配置
     *
     * @param fieldName      字段名
     * @param mappingConfigs 映射配置列表
     * @return 适用的配置，没有则返回NULL
     */
    @Nullable
    private static MappingConfig getConfigForField(String fieldName, List<MappingConfig> mappingConfigs) {
        for (MappingConfig config : mappingConfigs) {
            if (Objects.equals(config.targetFieldName, fieldName)) {
                // 如果映射的不是同名字段，并且ignore=true，继续检索配置
                if (Objects.equals(config.targetFieldName, config.sourceFieldName)
                        || !config.ignore) {
                    return config;
                }
            }
        }
        return null;
    }

    /**
     * 获取适用于目标字段的更多映射配置（源字段为目标字段的内部属性）
     *
     * @param fieldName      字段名
     * @param mappingConfigs 映射配置列表
     * @return 适用的配置
     */
    private static List<MappingConfig> getDeepConfigForField(String fieldName, List<MappingConfig> mappingConfigs) {
        List<MappingConfig> result = new ArrayList<>();
        for (MappingConfig config : mappingConfigs) {
            if (config.targetFieldName.indexOf(fieldName + ".") == 0) {
                result.add(config);
            }
        }
        return result;
    }

    /**
     * 映射配置管理器，封装配置的解析和获取等细节
     */
    private static class MappingConfigManager {
        static ConcurrentMap<MappingConfig.Key, List<MappingConfig>> mappings = new ConcurrentHashMap<>();

        /**
         * 根据源类型和目标类型获取配置
         *
         * @param sourceClass 源类
         * @param targetClass 目标类
         * @return 适用的配置列表
         */
        public static List<MappingConfig> getMappingConfig(Class<?> sourceClass, Class<?> targetClass) {
            MappingConfig.Key key = MappingConfig.Key.of(sourceClass, targetClass);
            if (!mappings.containsKey(key)) {
                buildMappingConfig(sourceClass, targetClass);
                buildMappingConfig(targetClass, sourceClass);
            }
            return mappings.getOrDefault(key, new ArrayList<>());
        }

        /**
         * 解析并缓存映射配置
         *
         * @param sourceClass 源类
         * @param targetClass 目标类
         */
        private static void buildMappingConfig(Class<?> sourceClass, Class<?> targetClass) {
            for (Field field : ReflectionUtils.getAllFields(sourceClass)) {
                if (field.isAnnotationPresent(Mapping.class)
                        || field.isAnnotationPresent(Nested.class)) {
                    MappingConfig config = createMappingConfig(sourceClass, targetClass, field.getName());
                    Mapping mapping = field.getAnnotation(Mapping.class);
                    if (mapping != null) {
                        if (StringUtils.isNotEmpty(mapping.field())) {
                            config.targetFieldName = mapping.field();
                        }
                        // 未配在本侧的单向绑定也需要忽略
                        config.ignore = mapping.ignore() || mapping.oneway();
                        config.mapNull = mapping.mapNull();
                        config.datePattern = mapping.datePattern();
                        config.mask = mapping.mask();
                        config.getter = mapping.getter();
                        config.setter = mapping.setter();
                        config.converterClass = mapping.inverseConverter();
                    }
                    Nested nested = field.getAnnotation(Nested.class);
                    if (nested != null) {
                        config.nestedSourceClass = nested.thisClass();
                        config.nestedTargetClass = nested.thatClass();
                    }
                    CollectionUtils.putIntoListMap(mappings, config.key(), config);
                }
            }
            for (Field field : ReflectionUtils.getAllFields(targetClass)) {
                if (field.isAnnotationPresent(Mapping.class)
                        || field.isAnnotationPresent(Nested.class)) {
                    MappingConfig config = createMappingConfig(sourceClass, targetClass, field.getName());
                    Mapping mapping = field.getAnnotation(Mapping.class);
                    if (mapping != null) {
                        if (StringUtils.isNotEmpty(mapping.field())) {
                            config.sourceFieldName = mapping.field();
                        }
                        config.ignore = mapping.ignore();
                        config.mapNull = mapping.mapNull();
                        config.datePattern = mapping.datePattern();
                        config.mask = mapping.mask();
                        config.getter = mapping.getter();
                        config.setter = mapping.setter();
                        config.converterClass = mapping.converter();
                    }
                    Nested nested = field.getAnnotation(Nested.class);
                    if (nested != null) {
                        config.nestedSourceClass = nested.thatClass();
                        config.nestedTargetClass = nested.thisClass();
                    }
                    CollectionUtils.putIntoListMap(mappings, config.key(), config);
                }
            }
        }
    }

    private static MappingConfig createMappingConfig(Class sourceClass, Class targetClass, String fieldName) {
        MappingConfig config = new MappingConfig();
        config.sourceClass = sourceClass;
        config.targetClass = targetClass;
        config.sourceFieldName = fieldName;
        config.targetFieldName = fieldName;
        return config;
    }

    /**
     * 映射配置对象
     */
    private static class MappingConfig {
        Class<?> sourceClass;
        Class<?> targetClass;
        String sourceFieldName;
        String targetFieldName;
        boolean ignore = false;
        boolean mapNull = true;
        Class<? extends TypeConverter>[] converterClass;
        /**
         * 嵌套的源对象类型
         */
        Class<?> nestedSourceClass;
        /**
         * 嵌套的目标对象类型
         */
        Class<?> nestedTargetClass;
        /**
         * 日期格式
         */
        String datePattern;
        /**
         * 字符蒙版
         */
        String mask;

        String getter;

        String setter;

        /**
         * 获取配置的Key
         *
         * @return 配置的Key
         */
        public Key key() {
            return Key.of(sourceClass, targetClass);
        }

        private static class Key {
            Class<?> sourceClass;
            Class<?> targetClass;

            Key(Class<?> sourceClass, Class<?> targetClass) {
                this.sourceClass = sourceClass;
                this.targetClass = targetClass;
            }

            /**
             * 根据源类型和目标类型构造配置的Key
             *
             * @param sourceClass 源类
             * @param targetClass 目标类
             * @return 配置的Key
             */
            public static Key of(Class<?> sourceClass, Class<?> targetClass) {
                return new Key(sourceClass, targetClass);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Key key = (Key) o;
                return Objects.equals(sourceClass, key.sourceClass) &&
                        Objects.equals(targetClass, key.targetClass);
            }

            @Override
            public int hashCode() {
                return Objects.hash(sourceClass, targetClass);
            }
        }

        @Override
        public String toString() {
            return "MappingConfig{" +
                    "sourceClass=" + sourceClass +
                    ", targetClass=" + targetClass +
                    ", sourceFieldName='" + sourceFieldName + '\'' +
                    ", targetFieldName='" + targetFieldName + '\'' +
                    ", ignore=" + ignore +
                    ", mapNull=" + mapNull +
                    ", converterClass=" + Arrays.toString(converterClass) +
                    ", nestedSourceClass=" + nestedSourceClass +
                    ", nestedTargetClass=" + nestedTargetClass +
                    ", datePattern='" + datePattern + '\'' +
                    ", mask='" + mask + '\'' +
                    ", getter='" + getter + '\'' +
                    ", setter='" + setter + '\'' +
                    '}';
        }
    }
}
