package com.taocares.commons.beans.exception;

/**
 * 属性拷贝时发生的异常
 *
 * @author Ankang
 * @date 2018/10/24
 */
public class BeanPropertyCopyException extends RuntimeException {
    private final String propertyName;
    private final Object sourceObject;

    /**
     * 属性拷贝异常
     *
     * @param propertyName 属性名
     * @param sourceObject 源对象
     * @param cause        原因
     */
    public BeanPropertyCopyException(String propertyName, Object sourceObject, Throwable cause) {
        super("属性：" + propertyName + "拷贝时发生异常", cause);
        this.propertyName = propertyName;
        this.sourceObject = sourceObject;
    }

    /**
     * 获取问题字段名
     *
     * @return
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * 获取源对象
     *
     * @return
     */
    public Object getSourceObject() {
        return sourceObject;
    }
}
