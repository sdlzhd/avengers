package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;
import com.taocares.commons.util.DateUtils;

import java.time.LocalDateTime;

/**
 * LocalDateTime转Date
 *
 * @author Ankang
 * @date 2018/9/27
 */
public class LocalDateTimeToDateConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return DateUtils.toDate((LocalDateTime) origin);
    }
}
