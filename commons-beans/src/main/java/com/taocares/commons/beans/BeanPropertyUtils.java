package com.taocares.commons.beans;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

/**
 * 实体分析工具，借助BeanWrapper实现
 */
public class BeanPropertyUtils {

    /**
     * 获取值为null的属性
     *
     * @param source 源对象
     * @return 空属性列表
     */
    public static List<String> getNullPropNames(Object source) {
        List<String> result = new ArrayList<>();
        BeanWrapper wrapper = source instanceof BeanWrapper ?
                (BeanWrapper) source : new BeanWrapperImpl(source);
        for (PropertyDescriptor pd : wrapper.getPropertyDescriptors()) {
            if (wrapper.getPropertyValue(pd.getName()) == null) {
                result.add(pd.getName());
            }
        }
        return result;
    }

    /**
     * 获取对象的所有属性
     *
     * @param source 源对象
     * @return 属性列表
     */
    public static List<String> getAllPropNames(Object source) {
        List<String> result = new ArrayList<>();
        BeanWrapper wrapper = source instanceof BeanWrapper ?
                (BeanWrapper) source : new BeanWrapperImpl(source);
        for (PropertyDescriptor pd : wrapper.getPropertyDescriptors()) {
            if (pd.getWriteMethod() != null && pd.getReadMethod() != null) {
                result.add(pd.getName());
            }
        }
        return result;
    }

    public static Object getPropertyValue(Object source, String propertyName) {
        BeanWrapper wrapper = source instanceof BeanWrapper ?
                (BeanWrapper) source : new BeanWrapperImpl(source);
        return wrapper.getPropertyValue(propertyName);
    }
}
