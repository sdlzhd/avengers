package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * String转Integer
 *
 * @author Ankang
 * @date 2018/9/26
 */
public class StringToIntegerConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return Integer.valueOf((String) origin);
    }
}
