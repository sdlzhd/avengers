package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;
import com.taocares.commons.util.DateUtils;

import java.time.LocalDate;

/**
 * LocalDate转Date
 *
 * @author Ankang
 * @date 2018/9/27
 */
public class LocalDateToDateConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return DateUtils.toDate((LocalDate) origin);
    }
}
