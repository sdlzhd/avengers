package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;
import com.taocares.commons.util.DateUtils;

import java.util.Date;

/**
 * Date转LocalTime
 *
 * @author Ankang
 * @date 2018/9/27
 */
public class DateToLocalTimeConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return DateUtils.toLocalTime((Date) origin);
    }
}
