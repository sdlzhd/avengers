package com.taocares.commons.beans.annotation;

import com.taocares.commons.beans.TypeConverter;

import java.lang.annotation.*;

/**
 * 定义实体拷贝时的字段映射
 *
 * @author Ankang
 * @date 2018/9/19
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Mapping {
    /**
     * 映射字段的名字，支持嵌套属性：(如：flight.stage)<br/>
     * 支持使用“this”获取整个源对象
     */
    String field() default "";

    /**
     * 忽略该属性
     */
    boolean ignore() default false;

    /**
     * 单向绑定（只在其他对象拷贝到此对象时生效）
     */
    boolean oneway() default false;

    /**
     * 是否拷贝null
     */
    boolean mapNull() default true;

    /**
     * 日期格式
     */
    String datePattern() default "";

    /**
     * Mask模板
     */
    String mask() default "";

    /**
     * target对象的setter方法名，默认使用setXXX
     */
    String setter() default "";

    /**
     * source对象的getter方法名，默认使用getXXX
     */
    String getter() default "";

    /**
     * 类型转换器，其他对象属性拷贝到此对象时使用
     */
    Class<? extends TypeConverter>[] converter() default {};

    /**
     * 反向类型转换器，此对象属性拷贝到其他对象时使用
     */
    Class<? extends TypeConverter>[] inverseConverter() default {};
}
