package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * Number任意子类转Float
 *
 * @author Ankang
 * @date 2018/9/27
 */
public class NumberToFloatConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return ((Number) origin).floatValue();
    }
}
