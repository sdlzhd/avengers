package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.AbstractTypeConverter;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 抽象类型转换器：集合转List
 *
 * @author Ankang
 * @date 2018/9/25
 */
public abstract class AbstractListConverter extends AbstractTypeConverter {

    @Override
    protected Object doConvert(@NotNull Object origin) {
        List<Object> result = new ArrayList<>();
        for (Object item : (Collection<?>) origin) {
            if (item != null) {
                result.add(convertItem(item));
            }
        }
        return result;
    }

    /**
     * 单个源对象转换为单个目标对象
     *
     * @param item 源对象
     * @return 目标对象
     */
    protected abstract Object convertItem(Object item);
}
