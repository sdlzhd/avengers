package com.taocares.commons.beans;

import com.taocares.commons.lang.TypeConvertException;

/**
 * 对象转换
 *
 * @author Ankang
 * @date 2018/9/20
 */
public interface TypeConverter {

    /**
     * 执行对象类型转换
     *
     * @param origin 原始对象
     * @return 转换之后的对象
     * @throws TypeConvertException 类型转换异常
     */
    Object convert(Object origin) throws TypeConvertException;
}
