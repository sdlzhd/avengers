package com.taocares.commons.beans;

import com.taocares.commons.beans.caffeine.CaffeineConfig;
import com.taocares.commons.lang.TypeConvertException;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

/**
 * 类型转换器基类，提供统一的异常处理和空指针处理
 *
 * 若需自定义缓存配置可以重写configure方法
 *
 * @author Ankang
 * @date 2018/9/25
 */
public abstract class AbstractTypeConverter implements TypeConverter {
    private CaffeineConfig struct = new CaffeineConfig()
            .cacheName(getDefaultCacheName());

    @PostConstruct
    private void init() {
        configure(struct);
    }

    @Override
    public Object convert(Object origin) throws TypeConvertException {
        Object result;

        if (struct.isCacheable()) {
            //若子类未设置缓存名时，自动生成缓存名
            if (null == struct.getCacheName()) {
                struct.cacheName(getDefaultCacheName());
            }

            //尝试从缓存获取
            result = CacheUtils.cacheGet(struct.getCacheName(), origin);
            if (null != result) {
                return result;
            }
        }

        try {
            result = origin == null ? convertNull() : doConvert(origin);
        } catch (Exception e) {
            throw new TypeConvertException(e);
        }

        //判断子类是否选择使用缓存
        if (origin != null &&
                result != null &&
                struct.isCacheable()) {
            CacheUtils.cachePut(struct, origin, result);
        }

        return result;
    }

    /**
     * 单个源对象转换为单个目标对象，
     * 转换过程出现的异常无需处理
     *
     * @param origin 源对象
     * @return 目标对象
     */
    protected abstract Object doConvert(@NotNull Object origin);

    /**
     * 使用者可以根据需要设置空值对应的结果（默认返回null）
     *
     * @return null应该转换成的值
     */
    public Object convertNull() {
        return null;
    }

    /**
     * 将CacheConfigStruct设置默认值
     *
     * @return 缓存配置对象
     */
    protected void configure(CaffeineConfig struct) {

    }

    /**
     * 获取默认的缓存名。
     * 当子类未指定缓存名称时，生成默认的缓存名。
     * 默认缓存名为子类全名
     *
     * @return 默认缓存名
     */
    private String getDefaultCacheName() {
        return this.getClass().getName();
    }
}
