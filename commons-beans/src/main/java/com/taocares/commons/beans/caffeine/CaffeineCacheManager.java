package com.taocares.commons.beans.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.taocares.commons.beans.exception.CacheException;
import org.springframework.cache.CacheManager;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @description: caffeine缓存管理类，用来管理缓存
 * @author: yunpeng liu
 * @create: 2018-12-24 11:26
 **/
public class CaffeineCacheManager {
    private static volatile CaffeineCacheManager singleton;
    private final ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<>(32);

    private CaffeineCacheManager() { }

    /**
     * 获取所有缓存名称
     * @return 缓存名称集合
     */
    public Collection<String> getCacheNames() {
        return Collections.unmodifiableSet(this.cacheMap.keySet());
    }

    /**
     * 根据缓存名获取一个缓存
     * @param name 缓存名
     * @return caffeine缓存实例
     */
    @Nullable
    public Cache getCache(String name) {
        return this.cacheMap.get(name);
    }

    /**
     * 获取当前缓存管理器实例
     * @return 当前缓存管理器单例
     * @throws CacheException
     */
    public static CaffeineCacheManager getInstance() throws CacheException {
        if (null != singleton) {
            return singleton;
        } else {
            synchronized(CaffeineCacheManager.class) {
                if (singleton == null) {
                    singleton = new CaffeineCacheManager();
                }

                return singleton;
            }
        }
    }

    /**
     * 在缓存管理器中添加一个caffeine缓存
     * @param cacheName 缓存名称
     * @param cache 缓存对象
     * @throws CacheException
     */
    public synchronized void addCache(String cacheName, Cache cache) throws CacheException {
        if (cacheName != null && cacheName.length() != 0) {
            if (singleton.getCache(cacheName) != null) {
                throw new CacheException("Cache " + cacheName + " already exists");
            } else {
                cacheMap.put(cacheName, cache);
            }
        }
    }
}
