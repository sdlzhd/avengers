package com.taocares.commons.beans.annotation;

import java.lang.annotation.*;

/**
 * 标记此字段为嵌套的对象
 *
 * @author Ankang
 * @date 2018/10/8
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Nested {
    /**
     * 本字段的对象类型
     */
    Class<?> thisClass();
    /**
     * 映射字段的对象类型
     */
    Class<?> thatClass();
}
