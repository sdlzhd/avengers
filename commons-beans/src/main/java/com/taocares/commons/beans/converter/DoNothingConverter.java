package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.TypeConverter;
import com.taocares.commons.lang.TypeConvertException;

/**
 * 不做任何转换的转换器
 *
 * @author Ankang
 * @date 2018/9/20
 */
public class DoNothingConverter implements TypeConverter {
    @Override
    public Object convert(Object origin) throws TypeConvertException {
        return origin;
    }
}
