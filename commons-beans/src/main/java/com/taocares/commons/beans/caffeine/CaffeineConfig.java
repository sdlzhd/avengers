package com.taocares.commons.beans.caffeine;

/**
 * @description: 缓存配置参数，封装为一个类方便实用
 * @author: yunpeng liu
 * @create: 2018-12-20 11:03
 **/
public class CaffeineConfig {
    /**
     * 是否使用缓存
     */
    private boolean cacheable = false;
    /**
     * 缓存名
     */
    private String cacheName = null;
    /**
     * 当前缓存在对内存上所能保存的最大元素数量。若设置为0，则对象在载入缓存后会立即从缓存中移除。
     */
    private long maximumSize = 500;
    /**
     * 对象存活时间，指对象从创建到失效所需要的时间。（单位：分钟）
     * 当设定为-1时表示一直可以访问。
     */
    private long expireAfterWriteMinutes = 5;
    /**
     * 对象空闲时，指对象在多长时间没有被访问就会失效。（单位：分钟）
     * 当设定为-1时表示一直可以访问。
     */
    private long expireAfterAccessMinutes = 5;

    public boolean isCacheable() {
        return cacheable;
    }

    public CaffeineConfig cacheable(boolean cacheable) {
        this.cacheable = cacheable;
        return this;
    }

    public String getCacheName() {
        return cacheName;
    }

    public CaffeineConfig cacheName(String cacheName) {
        this.cacheName = cacheName;
        return this;
    }

    public long getMaximumSize() {
        return maximumSize;
    }

    public CaffeineConfig maximumSize(long maximumSize) {
        this.maximumSize = maximumSize;
        return this;
    }

    public long getExpireAfterWriteMinutes() {
        return expireAfterWriteMinutes;
    }

    public CaffeineConfig expireAfterWriteMinutes(long expireAfterWriteMinutes) {
        this.expireAfterWriteMinutes = expireAfterWriteMinutes;
        return this;
    }

    public long getExpireAfterAccessMinutes() {
        return expireAfterAccessMinutes;
    }

    public CaffeineConfig expireAfterAccessMinutes(long expireAfterAccessMinutes) {
        this.expireAfterAccessMinutes = expireAfterAccessMinutes;
        return this;
    }
}
