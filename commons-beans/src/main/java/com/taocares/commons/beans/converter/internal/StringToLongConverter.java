package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * String转Long
 *
 * @author Ankang
 * @date 2018/9/26
 */
public class StringToLongConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return Long.valueOf((String) origin);
    }
}
