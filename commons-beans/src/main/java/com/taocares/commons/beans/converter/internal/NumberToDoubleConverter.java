package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * Number任意子类转Double
 *
 * @author Ankang
 * @date 2018/9/27
 */
public class NumberToDoubleConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return ((Number) origin).doubleValue();
    }
}
