package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.AbstractTypeConverter;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * 抽象类型转换器：集合或者对象转字符串<br/>
 * 集合转字符串会对所有元素转换之后用逗号连接
 *
 * @author Ankang
 * @date 2018/9/25
 */
public abstract class AbstractToStringConverter extends AbstractTypeConverter {

    @Override
    protected Object doConvert(@NotNull Object origin) {
        if (origin instanceof Collection) {
            StringBuilder sb = new StringBuilder();
            for (Object item : (Collection<?>) origin) {
                if (item != null) {
                    sb.append(getDelimiter());
                    sb.append(itemToString(item));
                }
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(0);
            }
            return sb.toString();
        } else {
            return itemToString(origin);
        }
    }

    /**
     * 单个源对象转换为字符串
     * @param item 源对象
     * @return 目标对象
     */
    protected abstract String itemToString(Object item);

    /**
     * 如果是列表拼接的字符串，指定分隔符
     * @return 分隔符
     */
    protected String getDelimiter() {
        return ",";
    }
}
