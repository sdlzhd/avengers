package com.taocares.commons.beans.resolver;

import com.taocares.commons.util.DateUtils;
import com.taocares.commons.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

/**
 * 日期处理类
 *
 * @author Ankang
 * @date 2018/10/11
 */
public class DateResolver {

    public static final String ISO_LOCAL_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * 日期和字符串/时间戳互相转换
     *
     * @param value        原始的对象
     * @param pattern      日期格式
     * @param expectedType 期望的类型
     * @return 转换结果
     * @throws DateTimeException 日期转换出错
     */
    public static Object convert(@NotNull Object value, String pattern, @NotNull Class<?> expectedType) {
        pattern = StringUtils.isEmpty(pattern) ? ISO_LOCAL_DATE_TIME : pattern;
        // 和字符串的转换
        if (expectedType == String.class) {
            if (value instanceof Date) {
                return DateUtils.format((Date) value, pattern);
            } else if (value instanceof LocalDate) {
                return DateUtils.format((LocalDate) value, pattern);
            } else if (value instanceof LocalTime) {
                return DateUtils.format((LocalTime) value, pattern);
            } else if (value instanceof LocalDateTime) {
                return DateUtils.format((LocalDateTime) value, pattern);
            }
        } else if (value instanceof String) {
            if (expectedType == Date.class) {
                return DateUtils.parseDate((String) value, pattern);
            } else if (expectedType == LocalDate.class) {
                return DateUtils.parseLocalDate((String) value, pattern);
            } else if (expectedType == LocalTime.class) {
                return DateUtils.parseLocalTime((String) value, pattern);
            } else if (expectedType == LocalDateTime.class) {
                return DateUtils.parseLocalDateTime((String) value, pattern);
            }
        }
        // 和时间戳的转换
        if (expectedType == Long.class || expectedType == long.class) {
            if (value instanceof Date) {
                return ((Date) value).getTime();
            } else if (value instanceof LocalDate) {
                return DateUtils.toInstant((LocalDate) value).toEpochMilli();
            } else if (value instanceof LocalTime) {
                return DateUtils.toInstant((LocalTime) value).toEpochMilli();
            } else if (value instanceof LocalDateTime) {
                return DateUtils.toInstant((LocalDateTime) value).toEpochMilli();
            }
        } else if (value instanceof Long) {
            if (expectedType == Date.class) {
                return new Date((Long) value);
            } else if (expectedType == LocalDate.class) {
                return DateUtils.toLocalDateTime((Long) value).toLocalDate();
            } else if (expectedType == LocalTime.class) {
                return DateUtils.toLocalDateTime((Long) value).toLocalTime();
            } else if (expectedType == LocalDateTime.class) {
                return DateUtils.toLocalDateTime((Long) value);
            }
        }
        return value;
    }
}
