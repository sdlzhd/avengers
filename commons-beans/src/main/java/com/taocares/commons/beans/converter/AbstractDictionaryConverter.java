package com.taocares.commons.beans.converter;

import com.taocares.commons.beans.AbstractTypeConverter;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * 抽象类型转换器：字典内容转换
 *
 * @author Ankang
 * @date 2018/9/25
 */
public abstract class AbstractDictionaryConverter extends AbstractTypeConverter {

    private Map<Object, Object> dictionary;
    private Map<Object, Object> inverseDictionary = new HashMap<>();

    @Override
    protected Object doConvert(@NotNull Object origin) {
        loadDictionary();
        Object result = dictionary.get(origin);
        if (result == null) {
            result = inverseDictionary.get(origin);
        }
        return result;
    }

    private void loadDictionary() {
        if (dictionary == null) {
            dictionary = getDictionary();
            for (Map.Entry<Object, Object> entry : dictionary.entrySet()) {
                inverseDictionary.put(entry.getValue(), entry.getKey());
            }
        }
    }

    /**
     * 获取Map形式的字典数据
     * @return 字典数据
     */
    protected abstract Map<Object, Object> getDictionary();
}
