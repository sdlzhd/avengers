package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * String转Double
 *
 * @author Ankang
 * @date 2018/9/26
 */
public class StringToDoubleConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return Double.valueOf((String) origin);
    }
}
