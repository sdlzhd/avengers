package com.taocares.commons.beans.exception;

/**
 * @description: 缓存异常
 * @author: yunpeng liu
 * @create: 2018-12-25 11:49
 **/
public class CacheException extends RuntimeException {
    private int errorCode;

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public CacheException() {
        super();
    }

    public CacheException(String errorMessage) {
        super(errorMessage);
    }

    public CacheException(int errorCode, String errorMessage) {
        super(errorMessage);
        this.setErrorCode(errorCode);
    }

    public CacheException(int errorCode, String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.setErrorCode(errorCode);
    }
}
