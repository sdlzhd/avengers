package com.taocares.commons.beans.converter.internal;

import com.taocares.commons.beans.AbstractTypeConverter;

/**
 * 对象转换为字符串
 *
 * @author Ankang
 * @date 2018/9/20
 */
public class ObjectToStringConverter extends AbstractTypeConverter {
    @Override
    protected Object doConvert(Object origin) {
        return String.valueOf(origin);
    }
}
