package com.taocares.commons.beans.foobar;

import lombok.Data;

import java.util.Date;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/10/23
 */
@Data
public class Bar extends Base {
    private Long id;
    private String name;
    private Date date;
}
