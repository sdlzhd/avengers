package com.taocares.commons.beans.foobar;

import lombok.Data;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/10/10
 */
@Data
public class Foo extends Base {
    private String name;
    private String content;
    private String mobile;
    private Bar bar;
    private String address;

    public String where(){
        return "Qingdao";
    }
}
