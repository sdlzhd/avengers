package com.taocares.commons.beans.foobar;

import lombok.Data;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/10/31
 */
@Data
public class Base {

    private Long baseId;
    private String baseName;
}
