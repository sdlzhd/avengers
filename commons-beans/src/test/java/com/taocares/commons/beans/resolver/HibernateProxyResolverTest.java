package com.taocares.commons.beans.resolver;

import com.taocares.commons.beans.foobar.Foo;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/12/29
 */
public class HibernateProxyResolverTest {

    @Test
    public void testGetProxiedClass() {
        List<Foo> fooList = new ArrayList<>();
        Class c1 = HibernateProxyResolver.getProxiedClass(fooList);
        fooList.add(new Foo());
        Class c2 = HibernateProxyResolver.getProxiedClass(fooList);
        assertEquals(ArrayList.class, c1);
        assertEquals(Foo.class, c2);
    }
}