package com.taocares.commons.beans;

import com.taocares.commons.beans.foobar.Foo;
import com.taocares.commons.beans.foobar.FooDto;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.testng.annotations.Test;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/9/19
 */
public class BeanUtilsTest {

    @Test
    public void testPropertyDescriptor() {
        BeanWrapper beanWrapper = new BeanWrapperImpl(new Foo());
        for (PropertyDescriptor pd : beanWrapper.getPropertyDescriptors()) {
            System.err.println(pd.getReadMethod().getReturnType() + "/" + pd.getName());
        }

        Foo foo = new Foo();
        foo.setMobile("18955555555");
        System.err.println(BeanPropertyUtils.getNullPropNames(foo));
        System.err.println(BeanPropertyUtils.getAllPropNames(foo));

    }

    @Test
    public void testMapNull() {
        Foo foo = new Foo();
        foo.setName("foo");
        foo.setContent("content");
        FooDto fooDto = new FooDto();
        BeanUtils.copyProperties(fooDto, foo, false);
        assertNotNull(foo.getName());
        assertNotNull(foo.getContent());
        System.err.println(foo);
        BeanUtils.copyProperties(fooDto, foo, true);
        assertNotNull(foo.getName());
        assertNull(foo.getContent());
        System.err.println(foo);
    }

    @Test
    public void testDeepMapping() {
        FooDto fooDto = new FooDto();
        fooDto.setName("foo");
        fooDto.setContent("foo content");
        fooDto.setBarId(1L);
        fooDto.setBarName("bar");
        fooDto.setDateMillis(System.currentTimeMillis());
        Foo foo = BeanUtils.copyProperties(fooDto, Foo.class);
        System.err.println(foo);
        assertEquals("foo", foo.getName());
        assertNotNull(foo.getBar());
        assertNull(foo.getBar().getId());
        assertNotNull(foo.getBar().getDate());
        assertEquals("bar", foo.getBar().getName());
        System.err.println(BeanUtils.copyProperties(foo, FooDto.class));
    }

    @Test
    public void testConvertTimestamp() {
        FooDto fooDto = new FooDto();
        Date date = new Date();
        fooDto.setDateMillis(date.getTime());
        Foo foo = BeanUtils.copyProperties(fooDto, Foo.class);
        assertEquals(date, foo.getBar().getDate());
    }

    @Test
    public void testMask() {
        Foo foo = new Foo();
        foo.setMobile("18912345678");
        FooDto fooDto = BeanUtils.copyProperties(foo, FooDto.class);
        assertEquals("189****5678", fooDto.getMobile1());
        assertEquals("189 1234 5678", fooDto.getMobile2());
    }

    @Test
    public void testCopyBean() {
        Foo foo = new Foo();
        foo.setAddress("青岛");
        foo.setName("凯亚");
        FooDto fooDto = BeanUtils.copyProperties(foo, FooDto.class);
        assertEquals(fooDto.getName(), "cares");
        assertEquals(fooDto.getAddress(), "Qingdao");
    }
}