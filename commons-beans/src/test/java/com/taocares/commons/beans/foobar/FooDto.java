package com.taocares.commons.beans.foobar;

import com.taocares.commons.beans.annotation.Mapping;
import com.taocares.commons.text.Mask;
import lombok.Data;

/**
 * Description goes here
 *
 * @author Ankang
 * @date 2018/10/10
 */
@Data
public class FooDto {
    @Mapping(mapNull = false, setter = "naming")
    private String name;
    private String content;
    @Mapping(field = "mobile", mask = Mask.MOBILE_MASK)
    private String mobile1;
    @Mapping(field = "mobile", mask = "AAA' 'AAAA' '")
    private String mobile2;

    @Mapping(field = "bar.id", oneway = true)
    private Long barId;

    @Mapping(field = "bar.name")
    private String barName;

    @Mapping(field = "bar.date", oneway = true)
    private String date;

    @Mapping(field = "bar.date")
    private Long dateMillis;

    @Mapping(getter = "where")
    private String address;

    public void naming(String name){
        this.name = "cares";
    }
}
